<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::middleware('MultiLanguage')->group(function () {
    Route::get('/',[HomeController::class, 'index'])->name('home');
    Route::get('/change-language/{lang}',[HomeController::class, 'changeLang'])->name('changeLang');
});
Route::get('/honors',[HomeController::class, 'honors'])->name('honors');
Route::get('/category/{slug}/',[ProductController::class, 'showCategory'])->name('showCategory');
Route::get('/subsetCompany/{slug}',[HomeController::class, 'subsetCompany'])->name('showSubsetCompany');
Route::get('/products',[HomeController::class, 'products'])->name('products');
Route::get('/products/{slug}',[HomeController::class, 'showProduct'])->name('showProduct');
Route::get('/services/{slug}',[HomeController::class, 'showService'])->name('showService');
Route::get('/services',[HomeController::class, 'services'])->name('services');
Route::get('/catalog',[ProductController::class, 'show'])->name('catalog');
Route::get('/blog',[BlogController::class, 'blog'])->name('blog');
Route::get('/blog/search/',[BlogController::class, 'blogSearch'])->name('blog.search');
Route::get('/blog/category/{slug}',[BlogController::class, 'blogCategory'])->name('blog.category');
Route::get('/blog/post/{slug}',[BlogController::class, 'show'])->name('blog.post');
Route::get('/faq',[HomeController::class, 'faq'])->name('faq');
Route::get('/about',[HomeController::class, 'about'])->name('about');
Route::get('/contact',[HomeController::class, 'contact'])->name('contact.show');
Route::post('/sendMessage',[HomeController::class, 'sendMessage'])->name('contact.sendMessage');
Route::post('/sendGetAdvice',[HomeController::class, 'sendGetAdvice'])->name('contact.sendGetAdvice');
Route::post('/search',[HomeController::class, 'search'])->name('front_search');

Auth::routes();
Route::get('/password/forget/mobile', [App\Http\Controllers\Auth\ForgotPasswordController::class,'sendVerifyCode'])->name('auth.sendVerifyCode');
Route::post('/password/mobile/verify', [App\Http\Controllers\Auth\ForgotPasswordController::class,'showNewPassword'])->name('auth.showNewPassword');
Route::post('/password/new', [App\Http\Controllers\Auth\ForgotPasswordController::class,'newPassword'])->name('auth.newPassword');
Route::get('/login/loginWithVerifyCode', [App\Http\Controllers\AuthController::class,'loginWithVerifyCode'])->name('auth.loginWithVerifyCode');
Route::get('/login-register', [App\Http\Controllers\AuthController::class,'loginRegister'])->name('auth.loginRegister');
Route::post('/login-register', [App\Http\Controllers\AuthController::class,'doLoginRegister'])->name('auth.doLoginRegister');



Route::middleware('auth')->group(function (){
    Route::get('/profile',                         [App\Http\Controllers\UserController::class,    'showProfile'])->name('profile.showProfile');
    Route::get('/profile/personal-info',            [App\Http\Controllers\UserController::class,    'personalInfo'])->name('profile.personalInfo.show');
    Route::post('/profile/personal-info',           [App\Http\Controllers\UserController::class,    'update'])->name('profile.personalInfo.update');
    Route::post('/profile/verifyMobile',            [App\Http\Controllers\UserController::class,    'verifyMobile'])->name('profile.verifyMobile');
    Route::post('/profile/updateMobile',            [App\Http\Controllers\UserController::class,    'updateMobile'])->name('profile.updateMobile');
    Route::get('/user',                             [App\Http\Controllers\UserController::class,    'user'])->name('user.user');
});
