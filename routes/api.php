<?php

use Illuminate\Support\Facades\Route;

Route::post('admin/login', 'Admin\AuthController@login');
//Route::post('admin/register', 'Admin\AuthController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('admin')->middleware(['auth:api','roleAuth'])->group(function () {
    //    Categories Route
    Route::patch('categories/publish', 'Admin\CategoryController@publish');
    Route::resource('categories', 'Admin\CategoryController');

    //    Product Route
    Route::post('products/arrange', 'Admin\ProductController@arrange');
    Route::delete('products/destroyFeature/{product}/{id}', 'Admin\ProductController@destroyFeature');
    Route::delete('products/destroyImage/{id}', 'Admin\ProductController@destroyImage');
    Route::patch('products/publish', 'Admin\ProductController@publish');
    Route::resource('products', 'Admin\ProductController');

    //  Banner Route
    Route::patch('banners/publish', 'Admin\BannerController@publish');
    Route::resource('banners', 'Admin\BannerController');
    //  Client Route
    Route::patch('clients/publish', 'Admin\ClientController@publish');
    Route::resource('clients', 'Admin\ClientController');
    //  SiteConfig Route
    Route::post('configs/update', 'Admin\ConfigController@update');
    Route::resource('configs', 'Admin\ConfigController');
    //  Dashboard Route
    Route::resource('dashboard', 'Admin\DashboardController');
    //  FAQ Route
    Route::patch('faqs/publish', 'Admin\FaqController@publish');
    Route::resource('faqs', 'Admin\FaqController');
    //  FAQ Categories Route
    Route::patch('faqCategories/publish', 'Admin\FaqsCategoriesController@publish');
    Route::resource('faqCategories', 'Admin\FaqsCategoriesController');
    //  Honor Route
    Route::patch('honors/publish', 'Admin\HonorController@publish');
    Route::resource('honors', 'Admin\HonorController');
    //  Page Route
    Route::post('pages/arrange', 'Admin\PageController@arrange');
    Route::patch('pages/publish', 'Admin\PageController@publish');
    Route::resource('pages', 'Admin\PageController');
    //  Permission Route
    Route::resource('permissions', 'Admin\PermissionController');
    //  Project Route
    Route::patch('projects/publish', 'Admin\ProjectController@publish');
    Route::resource('projects', 'Admin\ProjectController');
    //  Report Route
    Route::post('reports/salesReport', 'Admin\ReportController@salesReport');
    //  Role Route
    Route::patch('roles/publish', 'Admin\RoleController@publish');
    Route::resource('roles', 'Admin\RoleController');
    //  Service Route
    Route::patch('services/publish', 'Admin\ServiceController@publish');
    Route::resource('services', 'Admin\ServiceController');
    //  Property value Route
    Route::patch('subsetCompanies/publish', 'Admin\SubsetCompanyController@publish');
    Route::resource('subsetCompanies', 'Admin\SubsetCompanyController');
    //    User Route
    Route::patch('users/publish', 'Admin\UserController@publish');
    Route::resource('users', 'Admin\UserController');

});

//    Blog Route
Route::prefix('admin/blog')->middleware(['auth:api','roleAuth'])->group(function () {

    //    Categories Route
    Route::patch('categories/publish', 'Admin\BlogCategoryController@publish');
    Route::resource('categories', 'Admin\BlogCategoryController');

    //    Posts Route
    Route::patch('posts/publish', 'Admin\BlogPostController@publish');
    Route::post('posts/uploadImage', 'Admin\BlogPostController@uploadImage');
    Route::resource('posts', 'Admin\BlogPostController');

    //    BlogPostComment Route
    Route::patch('postComments/confirm', 'Admin\BlogPostCommentController@confirm');
    Route::resource('postComments', 'Admin\BlogPostCommentController');
});

//    Upload Route
Route::prefix('admin/uploads')->middleware(['auth:api','roleAuth'])->group(function () {
    Route::post('catalogFile', 'Admin\UploadController@catalogFile');
    Route::post('subsetCompanyImage', 'Admin\UploadController@subsetCompanyImage');
    Route::post('clientImage', 'Admin\UploadController@clientImage');
    Route::post('serviceImage', 'Admin\UploadController@serviceImage');
    Route::post('projectImage', 'Admin\UploadController@projectImage');
    Route::post('honorImage', 'Admin\UploadController@honorImage');
    Route::post('productImage', 'Admin\UploadController@productImage');
    Route::post('generalImage', 'Admin\UploadController@generalImage');
    Route::post('alternativeLogo', 'Admin\UploadController@alternativeLogo');
    Route::post('aboutUsImage', 'Admin\UploadController@aboutUsImage');
    Route::post('blogPostImage', 'Admin\UploadController@blogPostImage');
    Route::post('slideImage', 'Admin\UploadController@slideImage');
    Route::post('uploadImage', 'Admin\UploadController@uploadImage');
    Route::post('bannerImage', 'Admin\UploadController@bannerImage');
    Route::get('slides', 'Admin\UploadController@slides');
    Route::delete('slides/{id}', 'Admin\UploadController@deleteSlides');
    Route::post('arrange', 'Admin\UploadController@arrange');
});
