<?php

return [
  "BannerCreatedSuccessful"   => "Banner Created Successful.",
  "BannerEditedSuccessful"    => "Banner Edited Successful.",
  "BannerDeletedSuccessful"   => "Banner Deleted Successful."
];
