<?php

return [
  "CategoryCreatedSuccessful"   => "Category Created Successful.",
  "CategoryEditedSuccessful"    => "Category Edited Successful.",
  "CategoryDeletedSuccessful"   => "Category Deleted Successful."
];