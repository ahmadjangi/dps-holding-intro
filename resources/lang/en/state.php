<?php

return [
  "StateCreatedSuccessful"   => "OrderStateId Created Successful.",
  "StateEditedSuccessful"    => "OrderStateId Edited Successful.",
  "StateDeletedSuccessful"   => "OrderStateId Deleted Successful."
];
