<?php

return [
  "FaqCreatedSuccessful"   => "FAQ Created Successful.",
  "FAQEditedSuccessful"    => "FAQ Edited Successful.",
  "FAQDeletedSuccessful"   => "FAQ Deleted Successful.",
"FaqCategoryCreatedSuccessful"   => "دسته بندی ایجاد شد.",
  "FAQCategoryEditedSuccessful"   => "دسته بندی ویرایش شد.",
  "FaqCategoryDeletedSuccessful"   => ":count دسته بندی حذف شد.",
  "ThisFaqCategoryHasTransaction"   => "این دسته بندی دارای سوال و جواب است.",
];
