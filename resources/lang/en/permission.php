<?php

return [
  "PermissionCreatedSuccessful"   => "Permission Created Successful.",
  "PermissionEditedSuccessful"    => "Permission Edited Successful.",
  "PermissionDeletedSuccessful"   => "Permission Deleted Successful."
];