<?php

return [
  "CountryCreatedSuccessful"   => "Country Created Successful.",
  "CountryEditedSuccessful"    => "Country Edited Successful.",
  "CountryDeletedSuccessful"   => "Country Deleted Successful.",
  "CountryCodeMin"   => "Country Code Min.",
  "CountryCodeMax"   => "Country Code Max.",
  "CountryCodeRequired"   => "Country Code Required.",
  "CountryCodeUnique"   => "Country Code Unique.",
];
