<?php

return [
  "BrandCreatedSuccessful"   => "Brand Created Successful.",
  "BrandEditedSuccessful"    => "Brand Edited Successful.",
  "BrandDeletedSuccessful"   => ":count Brand Deleted Successful."
];
