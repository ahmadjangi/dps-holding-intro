<?php

return [
  "CategoryCreatedSuccessful"   => "Category Created Successful.",
  "CategoryEditedSuccessful"    => "Category Edited Successful.",
  "CategoryDeletedSuccessful"   => "Category Deleted Successful.",
  "PostImageUploadedSuccessful"   => "Post Image Uploaded Successful.",
  "ThisPostHasTransaction"   => "This Post Has Transaction.",
  "CommentSendSuccessful"   => "Comment Send Successful.",
  "CommentSendError"   => "Comment Send Error.",
  "CommentLikeSendSuccessful"   => "Comment Like Send Successful.",
  "CommentLikeSendError"   => "Comment Like Send Error.",
  "PostCreatedSuccessful"   => "Post Created Successful.",
  "PostEditedSuccessful"   => "Post Edited Successful.",
  "PostDeletedSuccessful"   => "Post Deleted Successful.",
];
