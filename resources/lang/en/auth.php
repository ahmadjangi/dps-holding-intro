<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Unauthorized' => 'You Unauthorized',
    'YouShouldLoginAgain' => 'You Should Login Again',
    'PleaseEnterValidMobile' => 'You Unauthorized',
    'ThisEmailNotRegisterPleaseEnterValidMobileForRegister' => 'You Unauthorized',
    'EnteredCodeNoteCorrect' => 'You Unauthorized',
    'ThisMobileNotRegister' => 'You Unauthorized',
	'usernameOrPasswordNotCorrect' => 'نام کاربری یا رمز عبور اشتباه است.',
	'YourAccountSuspendedPleaseCallWithAdmin' => 'Your Account Suspended Please Call With Admin',
	'SendSMSError' => 'SendSMSError',


];
