<?php
return [
    "ThisDiscountIsUsedInCart" => "This Discount Is Used In Cart",
    "DiscountNotFound" => "Discount Not Found",
    "DiscountAppliedSuccessful" => "Discount Applied Successful",
    "TheUsableNumberIsOver" => "The Usable Number Is Over",
    "TheNumberUsedHasExpired" => "The Number Used Has Expired",
    "CartProductQuantityIsLowerForApplyDiscount" => "Cart Product Quantity Is Lower For Apply Discount",
    "CartAmountIsLowerForApplyDiscount" => "Cart Amount Is Lower For Apply Discount",
    "DiscountOutOfDate" => "Discount Out Of Date",
    "quantityMoreThanMaxQuantity" => "quantity More Than Max Quantity",
    "DiscountReturnedSuccessful" => "Discount Returned Successful",
    "YouNotUsedThisCoupon" => "You Not Used This Coupon",
    "ErrorInReturnCoupon" => "Error In Return Coupon",
];
