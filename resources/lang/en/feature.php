<?php

return [
    "FeatureCreatedSuccessful"   => "Feature Created Successful.",
    "FeatureEditedSuccessful"    => "Feature Edited Successful.",
    "FeatureDeletedSuccessful"   => "Feature Deleted Successful.",
    "FeatureGroupCreatedSuccessful"   => "FeatureGroup Created Successful.",
    "FeatureGroupEditedSuccessful"   => "FeatureGroup Edited Successful.",
    "FeatureGroupDeletedSuccessful"   => "Feature Group Deleted Successful.",
    "FeatureValueCreatedSuccessful"   => "Feature Value Created Successful",
    "FeatureValueEditedSuccessful"   => "Feature Value Edited Successful",
    "FeatureValueDeletedSuccessful"   => "Feature Value Deleted Successful",

];
