<?php

return [
  "BadgeCreatedSuccessful"   => "Badge Created Successful.",
  "BadgeEditedSuccessful"    => "Badge Edited Successful.",
  "BadgeDeletedSuccessful"   => "Badge Deleted Successful.",
  "ImageIsRequired"   => "Image Is Required."
];
