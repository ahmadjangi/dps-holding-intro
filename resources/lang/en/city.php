<?php

return [
  "CityCreatedSuccessful"   => "City Created Successful.",
  "CityEditedSuccessful"    => "City Edited Successful.",
  "CityDeletedSuccessful"   => "City Deleted Successful."
];
