<?php

return [
  "UserCreatedSuccessful"   => "User Created Successful.",
  "UserEditedSuccessful"    => "User Edited Successful.",
  "UserDeletedSuccessful"   => ":count User Deleted Successful.",
  "LoginFailed" => "نام کاربری یا رمز عبور اشتباه است.",
  "AddressEditedSuccessful"   => "User Deleted Successful.",
  "AddressEditedError"   => "User Deleted Successful.",
  "YourRequestReceivedSuccessful"   => "User Deleted Successful.",
  "YourRequestReceivedError"   => "User Deleted Successful.",
  "UserUpdatedSuccessful"   => "User Deleted Successful.",
  "YouDoNotHaveAccessToUpdateYourProfile"   => "User Deleted Successful.",
  "YourMobileChangedSuccessfully"   => "User Deleted Successful.",
  "MobileNumberNoteChanged"   => "User Deleted Successful.",
];
