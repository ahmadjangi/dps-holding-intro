<?php

return [
  "RoleCreatedSuccessful"   => "Role Created Successful.",
  "RoleEditedSuccessful"    => "Role Edited Successful.",
  "RoleDeletedSuccessful"   => "Role Deleted Successful."
];