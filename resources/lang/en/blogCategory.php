<?php

return [
  "BlogCategoryCreatedSuccessful"   => "BlogCategory Created Successful.",
  "BlogCategoryEditedSuccessful"    => "BlogCategory Edited Successful.",
  "BlogCategoryDeletedSuccessful"   => "BlogCategory Deleted Successful.",
  "ThisBlogCategoryHasTransaction"   => "This Blog Category Has Transaction",
  "RecentPosts"   => "Recent Posts",
  "Categories"   => "Categories",
];
