<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'رمز عبور باید 6 کاراکتر باشد',
    'reset' => 'رمزعبور شما ریست شد.',
    'sent' => 'ایمیل خود را برای ریست رمزعبور وارد نمایید.',
    'token' => 'توکن ریست رمزعبور نامعتبر است.',
    'user' => "کاربری با این ایمیل وجود ندارد.",

];
