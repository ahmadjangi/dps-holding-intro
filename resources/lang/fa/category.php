<?php
return [
  "CategoryCreatedSuccessful" => "دسته بندی ایجاد شد.",
  "CategoryEditedSuccessful"=> "دسته بندی ویرایش شد.",
  "CategoryDeletedSuccessful"=> "دسته بندی حذف شد."
];