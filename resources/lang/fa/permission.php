<?php

return [
  "PermissionCreatedSuccessful"   => "دسترسی جدید اضافه شد",
  "PermissionEditedSuccessful"    => "دسترسی ویرایش شد.",
  "PermissionDeletedSuccessful"   => "دسترسی حذف شد."
];