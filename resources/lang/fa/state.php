<?php

return [
  "StateCreatedSuccessful"   => "استان ایجاد شد.",
  "StateEditedSuccessful"    => "استان ویرایش شد.",
  "StateDeletedSuccessful"   => "استان حذف شد."
];
