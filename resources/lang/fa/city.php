<?php

return [
  "CityCreatedSuccessful"   => "شهر ایجاد شد.",
  "CityEditedSuccessful"    => "شهر ویرایش شد.",
  "CityDeletedSuccessful"   => "شهر حذف شد."
];
