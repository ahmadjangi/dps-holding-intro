<?php

return [
    "UserCreatedSuccessful"                   => "کاربر ایجاد شد.",
    "UserEditedSuccessful"                    => "کاربر ویرایش شد.",
    "UserDeletedSuccessful"                   => ":count کاربر حذف شد.",
    "LoginFailed"                             => "نام کاربری یا رمز عبور اشتباه است.",
    "AddressEditedSuccessful"                 => "آدرس کاربر ویرایش شد.",
    "AddressEditedError"                      => "ویرایش آدرس با خطا مواجه شد.",
    "YourRequestReceivedSuccessful"           => "درخواست شما دریافت شد، پس از بررسی با شما تماس خواهیم گرفت.",
    "YourRequestReceivedError"                => "ارسال درخواست با خظا مواجه شد.",
    "UserUpdatedSuccessful"                   => "اطلاعات شما ویرایش شد.",
    "YouDoNotHaveAccessToUpdateYourProfile"   => "شما دسترسی لازم برای ویرایش پروفایل ندارید.",
    "YourMobileChangedSuccessfully"           => "شماره موبایل شما تغییر کرد.",
    "MobileNumberNoteChanged"                 => "شماره موبایل تغییر نکرد.",
];
