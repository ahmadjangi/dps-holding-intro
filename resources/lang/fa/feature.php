<?php

return [
    "FeatureCreatedSuccessful"   => "ویژگی جدید اضافه شد",
    "FeatureEditedSuccessful"    => "ویژگی ویرایش شد.",
    "FeatureDeletedSuccessful"   => "ویژگی حذف شد.",
    "FeatureGroupCreatedSuccessful"   => "گروه ویژگی ها ایجاد شد.",
    "FeatureGroupEditedSuccessful"   => "گروه ویژگی ها ویرایش شد.",
    "FeatureGroupDeletedSuccessful"   => "گروه ویژگی ها حذف شد.",
    "FeatureValueCreatedSuccessful"   => "مقدار ویژگی ایجاد شد.",
    "FeatureValueEditedSuccessful"   => "مقدار ویژگی ویرایش شد.",
    "FeatureValueDeletedSuccessful"   => "مقدار ویژگی حذف شد.",

];
