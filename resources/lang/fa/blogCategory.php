<?php

return [
  "BlogCategoryCreatedSuccessful"   => "دسته بندی وبلاگ ایجاد شد.",
  "BlogCategoryEditedSuccessful"    => "دسته بندی وبلاگ ویرایش شد.",
  "BlogCategoryDeletedSuccessful"   => "دسته بندی وبلاگ حذف شد.",
  "ThisBlogCategoryHasTransaction"  => "این دسته بندی دارای پست است.",
  "Categories"  => "دسته بندی ها",
  "RecentPosts"  => "جدید ترین مطالب",
];
