<?php

return [
  "RoleCreatedSuccessful"   => "نقش جدید اضافه شد.",
  "RoleEditedSuccessful"    => "نقش ویرایش شد.",
  "RoleDeletedSuccessful"   => "نقش حذف شد."
];
