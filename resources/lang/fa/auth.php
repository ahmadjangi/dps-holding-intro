<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'نام کاربری یا رمزعبور اشتباه است',
    'throttle' => 'ورود ناموق بیش از حد مجاز، لطفا بعد از :seconds ثانیه وارد شوید',
    'Unauthorized' => 'شما دسترسی لارم به این صفحه را ندارید.',
    'YouShouldLoginAgain' => 'شما باید دوباره وارد شوید.',
    'PleaseEnterValidMobile' => 'موبایل را به شکل صحیح وارد نمایید.',
    'ThisEmailNotRegisterPleaseEnterValidMobileForRegister' => 'این ایمیل ثبت نام نشده است، برای ثبت نام موبایل صحیح وارد نمایید.',
    'EnteredCodeNoteCorrect' => 'کد تایید وارد شده صحیح نیست.',
    'ThisMobileNotRegister' => 'موبایل وارد شده ثبت نام نکرده است.',
    'usernameOrPasswordNotCorrect' => 'نام کاربری یا رمز عبور اشتباه است.',
    'YourAccountSuspendedPleaseCallWithAdmin' => 'اکانت کاربری شما مسدود شده است، با لطفا مدیر وب سایت تماس بگیرید.',
    'SendSMSError' => 'خطا در ارسال پیامک تایید.',



];
