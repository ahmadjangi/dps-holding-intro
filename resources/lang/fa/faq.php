<?php

return [
  "FaqCreatedSuccessful"   => "سوال ایجاد شد.",
  "FAQEditedSuccessful"    => "سوال ویرایش شد.",
  "FAQDeletedSuccessful"   => ":count سوال حذف شد.",
  "FaqCategoryCreatedSuccessful"   => "دسته بندی ایجاد شد.",
  "FAQCategoryEditedSuccessful"   => "دسته بندی ویرایش شد.",
  "FaqCategoryDeletedSuccessful"   => ":count دسته بندی حذف شد.",
  "ThisFaqCategoryHasTransaction"   => "این دسته بندی دارای سوال و جواب است.",
];
