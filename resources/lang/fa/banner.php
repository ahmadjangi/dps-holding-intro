<?php

return [
  "BannerCreatedSuccessful"   => "بنر ایجاد شد.",
  "BannerEditedSuccessful"    => "بنر ویرایش شد.",
  "BannerDeletedSuccessful"   => "بنر حذف شد."
];
