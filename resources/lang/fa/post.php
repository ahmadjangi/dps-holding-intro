<?php

return [
  "CategoryCreatedSuccessful"   => "پست ایجاد شد.",
  "CategoryEditedSuccessful"    => "پست ویرایش شد.",
  "CategoryDeletedSuccessful"   => "پست حذف شد.",
  "PostImageUploadedSuccessful"   => "تصویر پست آپلود شد.",
  "ThisPostHasTransaction"   => "این پست دارای کامنت است.",
  "CommentSendSuccessful"   => "دیدگاه شما ارسال شد، پس از تایید مدیر نمایش داده خواهد شد.",
  "CommentSendError"   => "ارسال دیدگاه با خظا مواجه شد.",
  "CommentLikeSendSuccessful"   => "ممنون ار لایک شما",
  "CommentLikeSendError"   => "ارسال لایک با خطا مواجه شد.",
    "PostCreatedSuccessful"   => "پست ایجاد شد.",
    "PostEditedSuccessful"   => "پست ویرایش شد.",
    "PostDeletedSuccessful"   => "پست حذف شد.",
];
