<?php

return [
    "CountryCreatedSuccessful"   => "کشور ایجاد شد.",
    "CountryEditedSuccessful"    => "کشور ویرایش شد.",
    "CountryDeletedSuccessful"   => "کشور حذف شد.",
    "CountryCodeMin"   => "کد کشور حداقل 3 حرف است.",
    "CountryCodeMax"   => "کد کشور جداکثر 255 حرف است.",
    "CountryCodeRequired"   => "کد کشور الزامی است.",
    "CountryCodeUnique"   => "کد کشور تکراری است.",
];
