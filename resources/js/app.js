import $ from 'jquery';
window.$ = window.jQuery = $;
import 'bootstrap';
import Modal from "bootstrap/js/src/modal";
window.Modal = Modal
import 'jquery-ui/ui/widgets/autocomplete';
import 'jquery-ui/ui/effects/effect-fade';
import 'jquery-validation';
import Swiper, {Navigation, Pagination, EffectFade, Autoplay} from 'swiper';
window.Swiper = Swiper;
Swiper.use([Navigation, Pagination,EffectFade,Autoplay]);
import 'swiper/css/bundle'
import Cookies from 'js-cookie'
window.Cookies = Cookies;
import wNumb from 'wnumb'
window.wNumb = wNumb
import noUiSlider from 'nouislider'
window.noUiSlider = noUiSlider;
import { Fancybox } from "@fancyapps/ui";
window.Fancybox = Fancybox
import Helper from './javascriptHelper'
window.Helper = Helper
import toastr from 'toastr'
window.toastr = toastr
toastr.options.rtl = true;
