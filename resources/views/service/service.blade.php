@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] .' | '. $service->langs[0]->title)
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')
    @include('component.header.pageHeader',['img'=>env('APP_URL').'/storage/serviceImage/'.$service->img_url,'title'=>__('general.OurServices'),'breadcrumbs'=>['services']])
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 order-2 order-lg-1">
                    @include('service.sidebar')
                </div>
                <div class="col-lg-8 order-1 order-lg-2 mb-1-9 mb-lg-0">
                    <div class="services-single-right">
                        <h4 class="font-weight-600 mb-5">{{$service->langs[0]->title}}</h4>
                        <div class="mb-2-3"><img
                                src="{{env('APP_URL').'/storage/serviceImage/thumbnail/950/'.$service->img_url}}"
                                class="border-radius-5 box-shadow-primary" alt="$service->langs[0]->title"></div>
                        <div class="mb-1-9 w-95">@php echo $service->langs[0]->description @endphp</div>
                        <div class="row">
                            <div class="col-12">
                                <div class="inner-title">
                                    <h6>{{__('general.KeyBenefits')}}</h6>
                                </div>
                                <div id="accordion" class="accordion-style">
                                    @foreach($service->faqs as $faq)
                                        <div class="card">
                                            <div class="card-header" id="heading{{$faq->id}}">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-bs-toggle="collapse"
                                                            data-bs-target="#collapse{{$faq->id}}" aria-expanded="true"
                                                            aria-controls="collapse{{$faq->id}}">
                                                        {{$faq->langs[0]->title}}
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="collapse{{$faq->id}}" class="collapse show" aria-labelledby="heading{{$faq->id}}"
                                                 data-bs-parent="#accordion">
                                                <div class="card-body">
                                                    {{$faq->langs[0]->description}}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection

@section('scripts')

@endsection


