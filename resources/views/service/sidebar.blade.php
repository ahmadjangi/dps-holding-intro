<div class="pe-lg-1-9 pe-xl-2-3">
    <div class="services-single-left-box">
        <h6 class="font-weight-700 small text-uppercase left-title mb-4">{{__('general.OurServices')}}</h6>
        <div class="services-single-menu bg-light-gray mb-1-9">
            <ul class="m-0 list-unstyled">
                @foreach($services as $item)
                    @if($service->id === $item->id)
                        <li class="active"><a href="{{route('showService',$service->slug)}}">{{$item->langs[0]->title}}</a></li>
                    @else
                        <li ><a href="{{route('showService',$item->slug)}}">{{$item->langs[0]->title}}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <div class="bg-img cover-background primary-overlay border-radius-5 mb-1-9" data-overlay-dark="8" data-background="img/bg/bg2.jpg">
        <div class="position-relative z-index-9 text-center px-1-9 py-1-9 py-sm-2-3 py-md-2-9">
            <i class="fas fa-headset display-15 text-white mb-3"></i>
            <h5 class="text-white font-weight-600 mb-1">{{__('general.HowCanWeHelp?')}}</h5>
            <p class="text-white font-weight-500">{{__('general.Let’sGetTnTouch!!')}}</p>
            <div class="bg-white separator-line-horrizontal-full opacity3 mb-4"></div>
            <ul class="text-center list-unstyled m-0">
                <li class="text-white mb-1"><i class="fa fa-phone text-white me-2"></i><a href="tel:{{$siteConfigs->tel1}}" class="text-white">{{$siteConfigs->tel1}}</a></li>
                <li class="text-white mb-1"><i class="fa fa-phone text-white me-2"></i><a href="tel:{{$siteConfigs->tel2}}" class="text-white">{{$siteConfigs->tel2}}</a></li>
                <li class="text-white"><i class="fa fa-envelope-open text-white me-2"></i><a href="mailto:{{$siteConfigs->email}}" class="text-white">{{$siteConfigs->email}}</a></li>
            </ul>
        </div>
    </div>

    @if($service->catalog_url)
        <h6 class="font-weight-700 small text-uppercase left-title mb-4">{{__('general.CatalogDownload')}}</h6>
        <ul class="downloads list-unstyled m-0">
            <li class="mb-2"><a href="{{$service->catalog_url}}" target="_blank"><i class="far fa-file-pdf display-22"></i><span class="label font-weight-600">{{__('general.IntroService')}}</span></a></li>
        </ul>
    @endif
</div>
