@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] . ' | '. __('general.OurServices'))
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')

    @include('component.header.pageHeader',['img'=>'img/bg/bg15.jpg','title'=>__('general.OurServices'),'breadcrumbs'=>['services']])
    @includeWhen($services,'component.services',['services'=>$services,'grayBackground'=>false])
    <section class="parallax" data-overlay-dark="8" data-background="img/bg/bg1.jpg">
        <div class="container">

            <div class="section-heading white">
                <h2>{{__('general.RequestACall')}}</h2>
            </div>

            <div class="row justify-content-center">

                <div class="col-lg-10">
                    <div class="contact-form-box">
                        <form class="" id="frmServices">
                            <div class="quform-elements">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="quform-element form-group">
                                            <div class="quform-input">
                                                <input id="name" class="form-control" type="text" name="name" placeholder="{{__('general.YourNameHere')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="quform-element form-group">
                                            <div class="quform-input">
                                                <input id="email" class="form-control" type="text" name="email" placeholder="{{__('general.YourEmailHere')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="quform-element form-group">
                                            <div class="quform-input">
                                                <input id="phone" class="form-control" type="text" name="phone" placeholder="{{__('general.YourPhoneNumber')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="quform-submit-inner">
                                            <button class="butn white-hover primary w-100 btn-send" type="submit"><span>{{__('general.SubmitComment')}}</span></button>
                                        </div>
                                        <div class="quform-loading-wrap text-start"><span class="quform-loading"></span></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="alert alert-success" role="alert" style="display: none">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="section-heading">
                <h2>{{__('general.GetInTouchNow')}}</h2>
            </div>
            <div class="row">
                <div class="col-lg-6 mb-1-9 mb-lg-0">
                </div>
                <div class="col-lg-6">
                    <div id="accordion" class="accordion-style3">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        How can i purchase this item ?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion">
                                <div class="card-body bg-white">
                                    Tempora incidunt ut labore et dolore exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        why unique and creative design ?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
                                <div class="card-body bg-white">
                                    Neque porro quisquam est quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        are you ready to buy this theme ?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-bs-parent="#accordion">
                                <div class="card-body bg-white no-padding-bottom">
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection
@section('scripts')
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            $("#frmServices").validate({
                rules: {
                    name: {
                        required: true
                    },
                    phone: {
                        number: true,
                        minlength: 11,
                        maxlength: 11
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                },
                messages: {
                    name: {
                        required: "نام خود را وارد نمایید.",
                    },
                    email: {
                        required: "ایمیل خود را وارد نمایید.",
                        email: 'ایمیل را بصورت صحیح وارد نمایید',
                    },
                    phone: {
                        number: "تلفن باید بصورت عدد باشد.",
                        maxlength: "تلفن را بصورت صحیح وارد نمایید",
                        minlength: "تلفن را بصورت صحیح وارد نمایید",
                    }
                },
                submitHandler: function (form) {

                    let name = $('#name').val();
                    let email = $('#email').val();
                    let phone = $('#phone').val();

                    $.ajax({
                        url: '{{route('contact.sendGetAdvice')}}',
                        method: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            name: name,
                            phone: phone,
                            email: email,
                        },
                        beforeSend: function () {
                            $('.btn-send').prop('disable', true);
                        },
                        success: function (res) {
                            $('.alert-success').html('پیام شما ارسال شد، در صورت نیاز با شما تماس می گیریم')
                            $('.alert-success').fadeIn().delay(4000).fadeOut();
                        },
                        error: function (err) {

                            if (err.status === 402) {
                                let errors = $.parseJSON(err.responseText);
                                $.each(errors['data'], function (key, val) {
                                    toastr.warning(val[0]);
                                });
                            } else if (err.status === 422) {
                                let errors = $.parseJSON(err.responseText);
                                $.each(errors['errors'], function (key, val) {
                                    toastr.warning(val[0]);
                                });
                            } else if (err.status === 422) {
                                location.preload()
                            } else if (err.status === 500) {
                                err = JSON.parse(err.responseText);
                                toastr.error(err.message);
                            }
                        },
                        complete: function () {
                            $('.btn-send').prop('disable', false);
                        }
                    });

                    return false;
                }
            });
        })
    </script>
@endsection

