@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()])
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')

    @includeWhen(true,'component.home.homeSlider')
    @includeWhen(true,'component.home.services')
    @includeWhen(true,'component.home.about',['topMargin'=>0,'image'=>$siteConfigs->aboutUsImage,'slogan'=>$siteConfigs->slogan[app()->getLocale()],'about'=>$siteConfigs->shortDescription[app()->getLocale()]])
    @includeWhen(true,'component.home.solution')
    {{--    @includeWhen(true,'component.home.client')--}}
    @includeWhen($specialProducts,'component.home.products',['products'=>$specialProducts])
    {{--    @includeWhen(true,'component.home.testimonial')--}}
    {{--    @includeWhen(true,'component.home.team')--}}
    @includeWhen(true,'component.home.blog')
    @includeWhen(true,'component.home.address',[
    'title'=>$siteConfigs->title[app()->getLocale()],
    'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
    'tel1'=>$siteConfigs->tel1,
    'tel2'=>$siteConfigs->tel2,
    'postalCode'=>$siteConfigs->postalCode,
    'address'=>$siteConfigs->address[app()->getLocale()],
    'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
    'email'=>$siteConfigs->email,
    ])
@endsection
@section('scripts')

@endsection


