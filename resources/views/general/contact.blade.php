@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] .' | '.__('general.ContactUs'))
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')
    <section class="page-title-section2 bg-img cover-background top-position1" data-overlay-dark="6"
             data-background="img/bg/bg15.jpg">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h1>{{__('general.ContactUs')}}</h1>
                </div>
                <div class="col-md-12">
                    <ul class="ps-0">
                        <li><a href="{{route('home')}}">{{__('general.Home')}}</a></li>
                        <li><a href="{{route('contact.show')}}">{{__('general.ContactUs')}}</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-1-9 mb-lg-0">
                    <div class="pe-lg-1-9">
                        <div class="row">
                            <div class="section-heading half {{app()->getLocale() === 'fa' ? 'right' : 'left'}}">
                                <h3 class="font-weight-600">{{__('general.AboutInformation')}}</h3>
                            </div>

                            <div class="d-flex border-bottom pb-4 mb-4">
                                <div class="flex-shrink-0">
                                    <i class="fas fa-phone display-20 text-primary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    <h4 class="mb-1 text-uppercase display-30">{{__('general.PhoneNumber')}}</h4>
                                    <p class="mb-0">{{$siteConfigs->tel1}}</p>
                                    <p class="mb-0">{{$siteConfigs->tel2}}</p>
                                </div>
                            </div>
                            <div class="d-flex border-bottom pb-4 mb-4">
                                <div class="flex-shrink-0">
                                    <i class="fas fa-map-marker-alt display-20 text-primary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    <h4 class="mb-1 text-uppercase display-30">{{__('general.Locations')}}</h4>
                                    @if($siteConfigs->address)
                                        <p class="mb-0">{{$siteConfigs->address[app()->getLocale()]}}</p>
                                    @endif
                                    <br />
                                    @if($siteConfigs->addressTwo)
                                        <p class="mb-0">{{$siteConfigs->addressTwo[app()->getLocale()]}}</p>
                                    @endif

                                </div>
                            </div>
                            <div class="d-flex border-bottom pb-4 mb-4">
                                <div class="flex-shrink-0">
                                    <i class="far fa-envelope display-20 text-primary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    <h4 class="mb-1 text-uppercase display-30">{{__('general.PostalCode')}}</h4>
                                    @if($siteConfigs->postalCode)
                                        <p>{{$siteConfigs->postalCode}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex mb-1-9">
                                <div class="flex-shrink-0">
                                    <i class="far fa-envelope display-20 text-primary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    <h4 class="mb-1 text-uppercase display-30">{{__('general.Email')}}</h4>
                                    @if($siteConfigs->email)
                                        <p class="mb-0">{{$siteConfigs->email}}</p>
                                    @endif
                                </div>
                            </div>

                            <ul class="social-icon-style7 ps-0">
                                <li><a href="#!"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#!"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#!"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#!"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#!"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>

                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <div class="contact-form-box">
                        <div class="alert alert-success" role="alert" style="display: none">
                        </div>
                        <div class="section-heading half {{app()->getLocale() === 'fa' ? 'right' : 'left'}}">
                            <h3 class="font-weight-600">@lang('general.ContactUs')</h3>
                        </div>
                        <p class="contact-us__form-desc">@lang('general.PleaseSeeFaq')</p>
                        <form id="frmContact" class="mb-3">
                            <div class="row">
                                <div class="col-12 col-lg-6 mb-3">
                                    <label for="name">@lang('general.Name')</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="@lang('general.Name')">
                                </div>

                                <div class="col-12 col-lg-6 mb-3">
                                    <label for="email">@lang('general.Email')</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="@lang('general.Email')">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-lg-6 mb-3">
                                    <label for="phone">@lang('general.Phone')</label>
                                    <input type="tel" class="form-control" id="phone" name="phone" placeholder="@lang('general.Phone')"
                                           maxlength="11">
                                </div>

                                <div class="col-12 col-lg-6 mb-3">
                                    <label for="subject">@lang('general.Subject')</label>
                                    <input type="text" class="form-control" id="subject" name="subject"
                                           placeholder="@lang('general.Subject')">
                                </div>
                            </div>

                            <label for="message">@lang('general.Message')</label>
                            <textarea class="form-control" cols="30" rows="4" id="message" name="message" required
                                      placeholder=""></textarea>
                            <div class="quform-submit-inner mt-3">
                                <button class="butn btn-send" type="submit"><span>@lang('general.Send')</span></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- MAP
    ================================================== -->
    <iframe class="contact-map" id="gmap_canvas"
            src="https://maps.google.com/maps?q=tabriz,شرکت فناوری دینامیک پایش&t=&z=14&ie=UTF8&iwloc=&output=embed" scrolling="no"
            marginheight="0" marginwidth="0"></iframe>
@endsection

@section('scripts')
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#frmContact").validate({
                rules: {
                    name: {
                        required: true
                    },
                    phone: {
                        number: true,
                        minlength: 11,
                        maxlength: 11
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    message: {
                        required: true
                    },
                },
                messages: {
                    name: {
                        required: "نام خود را وارد نمایید.",
                    },
                    email: {
                        required: "ایمیل خود را وارد نمایید.",
                        email: 'ایمیل را بصورت صحیح وارد نمایید',
                    },
                    phone: {
                        number: "تلفن باید بصورت عدد باشد.",
                        maxlength: "تلفن را بصورت صحیح وارد نمایید",
                        minlength: "تلفن را بصورت صحیح وارد نمایید",
                    },
                    message: {
                        required: "پیام خود را وارد نمایید.",
                    }
                },
                submitHandler: function (form) {

                    let name = $('#name').val();
                    let email = $('#email').val();
                    let phone = $('#phone').val();
                    let subject = $('#subject').val();
                    let message = $('#message').val();

                    $.ajax({
                        url: '{{route('contact.sendMessage')}}',
                        method: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            name: name,
                            phone: phone,
                            email: email,
                            subject: subject,
                            message: message,
                        },
                        beforeSend: function () {
                            $('.btn-send').prop('disable', true);
                        },
                        success: function (res) {

                            $('#reviewTitle').val('');
                            $('#reviewRating').val('');
                            $('#reviewMessage').val('');
                            $('.alert-success').html('پیام شما ارسال شد، در صورت نیاز با شما تماس می گیریم')
                            $('.alert-success').fadeIn().delay(4000).fadeOut();
                        },
                        error: function (err) {

                            if (err.status === 402) {
                                let errors = $.parseJSON(err.responseText);
                                $.each(errors['data'], function (key, val) {
                                    toastr.warning(val[0]);
                                });
                            } else if (err.status === 422) {
                                let errors = $.parseJSON(err.responseText);
                                $.each(errors['errors'], function (key, val) {
                                    toastr.warning(val[0]);
                                });
                            } else if (err.status === 422) {
                                location.preload()
                            } else if (err.status === 500) {
                                err = JSON.parse(err.responseText);
                                toastr.error(err.message);
                            }
                        },
                        complete: function () {
                            $('.btn-send').prop('disable', false);
                        }
                    });

                    return false;
                }
            });
        })
    </script>
@endsection


