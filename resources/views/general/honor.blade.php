@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] . ' | ' .__('general.honors'))
@section('main-content')
    @include('component.header.pageHeader',['img'=>env('APP_URL').'/img/bg/honor-page-title-cover.jpg','title'=>__('general.honors'),'breadcrumbs'=>['honors']])
    <section>

        <div class="container">
            <div class="section-heading">
                <h2>{{__('general.honors')}}</h2>
                <p class="w-md-75 w-lg-55">{{__('general.honorMessage')}}</p>
            </div>

            <div class="row portfolio-gallery mt-n1-9">
                @foreach($honors as $honor)
                <div class="col-md-6 col-lg-4 mt-1-9 wow fadeIn" data-wow-delay="100ms" data-src="{{$honor->img_url}}" data-sub-html="<h4 class='text-white'>{{$honor->langs[0]->title}}</h4>">
                    <div class="portfolio-style1">
                        <img src="{{$honor->img_url}}" class="border-radius-10" alt="{{$honor->langs[0]->title}}">
                        <a href="#!" class="portfolio-text">
                            <h3 class="mb-0 h5">{{$honor->langs[0]->title}}</h3>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </section>
    @includeWhen(true,'component.home.address',[
 'title'=>$siteConfigs->title[app()->getLocale()],
 'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
 'tel1'=>$siteConfigs->tel1,
 'tel2'=>$siteConfigs->tel2,
 'postalCode'=>$siteConfigs->postalCode,
 'address'=>$siteConfigs->address[app()->getLocale()],
 'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
 'email'=>$siteConfigs->email,
 ])
@endsection

@section('scripts')

@endsection


