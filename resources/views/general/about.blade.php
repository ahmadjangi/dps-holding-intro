@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] .' | '.__('general.AboutUS'))
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')
    <section class="page-title-section2 bg-img cover-background top-position1" data-overlay-dark="6"
             data-background="img/bg/bg15.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{__('general.AboutUS')}}</h1>
                </div>
                <div class="col-md-12">
                    <ul class="ps-0">
                        <li><a href="{{route('home')}}">{{__('general.Home')}}</a></li>
                        <li><a href="{{route('about')}}">{{__('general.AboutUS')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="pe-lg-5">

                        <h2 class="font-weight-600">{{__('general.AboutDPS')}}</h2>
                        <div style="text-align: justify">
                            @php echo $siteConfigs->abouteUsContent[app()->getLocale()] @endphp
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <div class="bg-img cover-background box-shadow-primary h-100" data-overlay-dark="0"
                         data-background="{{$siteConfigs->aboutUsImage}}">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="parallax" data-overlay-dark="7" data-background="img/bg/about-us-cover.jpg">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-md-5 mb-1-9 mb-md-0">
                    <div class="story-video">

                        <div class="text-center z-index-1">
                            <a class="popup-youtube video_btn" href="https://www.youtube.com/watch?v=CiBu9_lkysA"><i
                                    class="fas fa-play"></i>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="col-md-7">

                    <h2 class="text-white font-weight-600 mb-3" style="text-align: justify">{{__('general.CustomerSuccessIsWhatWeSeek')}}</h3>

                        <p class="display-29 text-white lh-base w-90 m-0 text-justify">{{__('general.DPSSlogan')}}</p>
                        <div class="separator-line-horrizontal-full bg-white opacity2 my-1-6 my-sm-1-9 my-md-2-9"></div>

                        <div class="row">
                            <div class="col-4">
                                <div class="counter-box text-center text-md-start">

                                    <h4 class="countup text-white d-block">{{$siteConfigs->statisticalNumberValue_1[app()->getLocale()]}}</h4>
                                    <div
                                        class="separator-line-horrizontal-medium-light3 bg-primary my-2 my-lg-3 opacity5 mx-auto mx-md-0"></div>
                                    <p class="lead font-weight-600 text-white m-0">{{$siteConfigs->statisticalNumberTitle_1[app()->getLocale()]}}</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="counter-box text-center text-md-start">
                                    <h4 class="countup text-white d-block">{{$siteConfigs->statisticalNumberValue_2[app()->getLocale()]}}</h4>
                                    <div
                                        class="separator-line-horrizontal-medium-light3 bg-primary my-2 my-lg-3 opacity5 mx-auto mx-md-0"></div>
                                    <p class="lead font-weight-600 text-white m-0">{{$siteConfigs->statisticalNumberTitle_2[app()->getLocale()]}}</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="counter-box text-center text-md-start">
                                    <h4 class="countup text-white d-block">{{$siteConfigs->statisticalNumberValue_3[app()->getLocale()]}}</h4>
                                    <div
                                        class="separator-line-horrizontal-medium-light3 bg-primary my-2 my-lg-3 opacity5 mx-auto mx-md-0"></div>
                                    <p class="lead font-weight-600 text-white m-0">{{$siteConfigs->statisticalNumberTitle_3[app()->getLocale()]}}</p>
                                </div>
                            </div>

                        </div>
                </div>

            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="section-heading">
                <h2>{{__('general.WhatWeDo')}}</h2>
            </div>
            <div class="row feature-boxes-container mt-n1-9">

                <div class="col-md-6 col-lg-4 mt-1-9 feature-box-04">
                    <div class="feature-box-inner h-100">
                        <i class="icon-layers display-16"></i>
                        <h4 class="h6 mt-2 text-uppercase font-weight-600">{{$siteConfigs->whatWeDoTitle_1[app()->getLocale()]}}</h4>
                        <div class="sepratar"></div>
                        <p style="text-align: justify">{{$siteConfigs->whatWeDoValue_1[app()->getLocale()]}}</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mt-1-9 feature-box-04">
                    <div class="feature-box-inner h-100">
                        <i class="icon-genius display-16"></i>
                        <h4 class="h6 mt-2 text-uppercase font-weight-600">{{$siteConfigs->whatWeDoTitle_2[app()->getLocale()]}}</h4>
                        <div class="sepratar"></div>
                        <p style="text-align: justify">{{$siteConfigs->whatWeDoValue_2[app()->getLocale()]}}</p>
                    </div>

                </div>

                <div class="col-md-6 col-lg-4 mt-1-9 feature-box-04">
                    <div class="feature-box-inner h-100">
                        <i class="icon-hotairballoon display-16"></i>
                        <h4 class="h6 mt-2 text-uppercase font-weight-600">{{$siteConfigs->whatWeDoTitle_3[app()->getLocale()]}}</h4>
                        <div class="sepratar"></div>
                        <p style="text-align: justify">{{$siteConfigs->whatWeDoValue_3[app()->getLocale()]}}</p>
                    </div>
                </div>

            </div>

        </div>
    </section>

    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection

@section('scripts')

@endsection


