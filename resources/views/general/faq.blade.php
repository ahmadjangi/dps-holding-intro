@extends('layout.app')
@section('title','سوالات متداول | ' . $siteConfigs->title[app()->getLocale()])
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')
    <main class="main page-faq">
        <div class="page-content mt-5">
            <div class="container">
                <div class="row">
                    <div class="mb-4">
                        <h5>سوالات پرتکرار</h5>
                    </div>
                </div>
                @foreach($faqs as $groupIndex => $faq)
                    <h6 class="mb-3">{{$faq->title}}</h6>
                    <div class="accordion accordion-flush" id="{{'accordion-'.$faq->id}}">
                        @foreach($faq->faqs as $question)
                            @if($groupIndex === 0 && $loop->first)
                                <div class="accordion-item mb-5">
                                    <h2 class="accordion-header"  id="{{'heading-'.$question->id}}">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="{{'#collapse-'.$question->id}}" aria-expanded="true" aria-controls="{{'#collapse-'.$question->id}}">
                                            {{$question->title}}
                                        </button>
                                    </h2>
                                    <div id="{{'collapse-'.$question->id}}" class="accordion-collapse collapse show" aria-labelledby="{{'heading-'.$question->id}}">
                                        <div class="accordion-body">
                                            {{$question->description}}
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="accordion-item mb-5">
                                    <h2 class="accordion-header" id="{{'heading-'.$question->id}}">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="{{'#collapse-'.$question->id}}" aria-expanded="true" aria-controls="{{'#collapse-'.$question->id}}">
                                            {{$question->title}}
                                        </button>
                                    </h2>
                                    <div id="{{'collapse-'.$question->id}}" class="accordion-collapse collapse" aria-labelledby="{{'heading-'.$question->id}}">
                                        <div class="accordion-body">
                                            {{$question->description}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </main>
@endsection


