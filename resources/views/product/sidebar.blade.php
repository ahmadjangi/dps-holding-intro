<div class="side-bar">
    <div class="widget">
        <div id="accordion" class="accordion-style2">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link {{app()->getLocale() === 'fa' ? 'text-start' : ''}}"
                                data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                                aria-controls="collapseOne">{{__('general.productCategory')}}</button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion">
                    <div class="card-body">
                        <ul class="mb-0 list-unstyled">
                            <li><a href="{{route('products')}}">{{__('general.showAll')}}</a></li>
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{route('showCategory',$category->slug)}}">{{$category->langs[0]->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget">
        <div class="offer-banner text-center d-none d-lg-block">
            <a href="{{$siteConfigs->aparatUrl}}"><img src="{{env('App_URL')}}/img/bg/product-sidebar-banner.jpg"
                                                       alt="کانال آپارات"></a>
        </div>
    </div>

</div>
