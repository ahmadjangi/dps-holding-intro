@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] .' | ' .  __('general.Products'))
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')
    @include('component.header.pageHeader',['img'=>env('APP_URL').'/img/bg/bg15.jpg','title'=>__('general.Products'),'breadcrumbs'=>['products']])
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    @include('product.sidebar')
                </div>
                <div class="col-lg-9 ps-lg-1-9">
                    <div class="row product-grid">
                        @if($products)
                        @foreach($products as $product)

                            <div class="col-xl-6 col-sm-6">
                                <a href="{{route('showProduct',$product->slug)}}">
                                    <div class="product-details">
                                        <div class="product-img">
                                            <img
                                                src="{{env('APP_URL').'/storage/productImage/thumbnail/600/'.$product->images[0]->url}}"
                                                alt="{{$product->langs[0]->title}}">
                                        </div>

                                        <div class="product-info">
                                            <a href="{{route('showProduct',$product->slug)}}">{{$product->langs[0]->title}}</a>
                                        </div>

                                    </div>
                                </a>
                            </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="row mt-1-9 mt-lg-6">
                        {{$products->links('vendor.pagination.default')}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection

@section('scripts')

@endsection


