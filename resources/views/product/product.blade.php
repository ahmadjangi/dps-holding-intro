@extends('layout.app')
@section('title',$siteConfigs->title[app()->getLocale()] .' | ' . $product->langs[0]->title)
@section('keywords',$product ? $product->langs[0]->meta_keyword : __('general.Blog'))
@section('description',$product ? $product->langs[0]->meta_description : __('general.Blog'))
@section('main-content')
    @include('component.header.pageHeader',['img'=>env('APP_URL').'/img/bg/bg15.jpg','title'=>__('general.Products'),'breadcrumbs'=>['products']])
    <section>
        <div class="container">
            <div class="row mb-6 mb-sm-7 mb-md-8 mb-lg-9">
                <div class="col-lg-5 text-center text-lg-start mb-1-9 mb-lg-0">
                    <div class="xzoom-container">
                        <img class="xzoom5 mb-1-9" id="xzoom-magnific"
                             src="{{env('APP_URL').'/storage/productImage/thumbnail/600/'.$product->images[0]->url}}"
                             xoriginal="{{env('APP_URL').'/storage/productImage/'.$product->images[0]->url}}"
                             alt="{{$product->langs[0]->title}}">
                        <div class="xzoom-thumbs m-0">
                            @foreach($product->images as $productImage)
                                <a href="{{env('APP_URL').'/storage/productImage/thumbnail/600/'.$productImage->url}}"><img
                                        class="xzoom-gallery5" width="80"
                                        src="{{env('APP_URL').'/storage/productImage/thumbnail/600/'.$productImage->url}}"
                                        {{--                                                                            xpreview="img/shop/preview/01_product.jpg"--}}
                                        alt="{{$product->langs[0]->title}}" title="{{$product->langs[0]->title}}"></a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 ps-lg-2-3 d-flex justify-content-between flex-column">
                    <div class="product-detail">
                        <h3 class="mb-2">{{$product->langs[0]->title}}</h3>
                        <div class="bg-primary separator-line-horrizontal-full mb-4"></div>
                        <p>{{$product->langs[0]->short_description}}</p>
                    </div>
                    <div class="row">
                        <div class="col-lg-7">
                            <label>{{__('general.ShareOn')}}</label>
                            <ul class="social-icon-style3 ps-0">
                                {{--                                    TOdo : Edit share Link--}}
                                <li><a href="#!"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#!"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#!"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#!"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#!"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-6 mb-sm-7 mb-md-8 mb-lg-9">
                <div class="col-12">
                    <div class="horizontaltab tab-style2">
                        <ul class="resp-tabs-list hor_1 text-start">
                            <li>توضیحات محصول</li>
{{--                            <li>ویژگی ها و امکانات</li>--}}
                        </ul>
                        <div class="resp-tabs-container hor_1">
                            <div>@php echo $product->langs[0]->description @endphp</div>
{{--                            <div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-lg-6 mb-1-9 mb-lg-0">--}}

{{--                                        <h3 class="h6">Information:</h3>--}}
{{--                                        <ul class="list-style-14 mb-4">--}}
{{--                                            <li><strong>Fit Type:</strong> Regular Fit</li>--}}
{{--                                            <li><strong>Fabric:</strong> 100% Cotton</li>--}}
{{--                                            <li><strong>Style:</strong> Regular</li>--}}
{{--                                            <li><strong>Pattern:</strong> Printed</li>--}}
{{--                                            <li><strong>Sleeve Type:</strong> Half Sleeve</li>--}}
{{--                                        </ul>--}}

{{--                                        <p class="mb-0">Sed ut perspiciatis unde omnis iste natus error sit voluptatem--}}
{{--                                            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab--}}
{{--                                            illo inventore veritatis et quasi architecto in ea voluptate velit.</p>--}}

{{--                                    </div>--}}
{{--                                    <div class="col-lg-6 ps-lg-1-9">--}}

{{--                                        <div class="table-responsive">--}}
{{--                                            <table class="table bordered">--}}
{{--                                                <thead>--}}
{{--                                                <tr>--}}
{{--                                                    <th>Shipping Options</th>--}}
{{--                                                    <th>Delivery Time</th>--}}
{{--                                                    <th>Price</th>--}}
{{--                                                </tr>--}}
{{--                                                </thead>--}}
{{--                                                <tbody>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        Standard Shipping--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        Delivery in 5 - 7 working days--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        $8.00--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        Express Shipping--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        28 August 2019--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        $12.00--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        1 - 2 day Shipping--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        27 August 2019--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        $12.00--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        Free Shipping--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        25 August 2019--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                        $0.00--}}
{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                                </tbody>--}}
{{--                                            </table>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="section-heading title-style3">
                        <h3 class="h4">{{__('general.OtherProducts')}}</h3>
                    </div>
                </div>
                <div class="carousel-style1 col-12" dir="ltr">
                    <div class="product-grid control-top owl-carousel owl-theme">
                        @foreach($products as $product)
                            <div>
                                <div class="product-details">
                                    <a href="{{route('showProduct',$product->slug)}}">
                                        <div class="product-img">
                                            <img
                                                src="{{env('APP_URL').'/storage/productImage/thumbnail/600/'.$product->images[0]->url}}"
                                                alt="{{$product->langs[0]->title}}">
                                        </div>
                                    </a>
                                    <div class="product-info">
                                        <a href="{{route('showProduct',$product->slug)}}">{{$product->langs[0]->title}}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>

    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection

@section('scripts')

@endsection


