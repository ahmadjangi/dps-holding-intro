<div class="side-bar">
    <div class="widget">
        <form class="search-form w-sm-90 mx-auto mx-lg-0" action="{{route('blog.search')}}" method="get">
            <div class="search-elements">
                <div class="row">
                    <div class="col-md-12">
                        <div class="search-element">
                            <div class="search-input">
                                <input class="form-control" id="ws" type="text" name="term" placeholder="{{__('general.Search')}}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="search-submit-inner">
                            <button class="btn btn-white text-primary m-0 sidebar--search-button" type="submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="widget">
        <div class="widget-title">
            <h6>{{__('blogCategory.RecentPosts')}}</h6>
        </div>
        <ul class="list-unstyled">
            @foreach($recentPosts as $recentPost)
                <li><a href="{{route('blog.post',$recentPost)}}">{{str_limit($recentPost->langs[0]->title,70)}}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="widget">
        <div class="widget-title">
            <h6>{{__('blogCategory.Categories')}}</h6>
        </div>
        <ul class="list-unstyled">
            @foreach($categories as $category)
                <li><a href="{{route('blog.category',$category->slug)}}">{{$category->langs[0]->title}}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="widget">
        <div class="widget-title">
            <h6>{{__('general.FollowUs')}}</h6>
        </div>
        <ul class="social-listing ps-0">
            @if($siteConfigs->facebookUrl)
                <li><a href="{{$siteConfigs->facebookUrl}}"><i class="fab fa-facebook-f"></i></a></li>
            @endif
            @if($siteConfigs->twitterUrl)
                <li><a href="{{$siteConfigs->twitterUrl}}"><i class="fab fa-twitter"></i></a></li>
            @endif
            @if($siteConfigs->instagramUrl)
                <li><a href="{{$siteConfigs->instagramUrl}}"><i class="fab fa-instagram"></i></a></li>
            @endif
            @if($siteConfigs->aparatUrl)
                <li><a href="{{$siteConfigs->aparatUrl}}"><i class="fab fa-youtube"></i></a></li>
            @endif
            @if($siteConfigs->linkedinUrl)
                <li><a href="{{$siteConfigs->linkedinUrl}}"><i class="fab fa-linkedin-in"></i></a></li>
            @endif
        </ul>
    </div>
</div>
