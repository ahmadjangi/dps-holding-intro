@extends('layout.app')
@section('title',$category ? $category->langs[0]->title : __('general.Blog'))
@section('keywords',$category ? $category->langs[0]->description : __('general.Blog'))
@section('description',$category ? $category->langs[0]->description : __('general.Blog'))
@section('main-content')
    @include('component.header.pageHeader',['img'=>env('APP_URL').'/img/bg/bg15.jpg','title'=>__('general.Blog').' '.$siteConfigs->title[app()->getLocale()],'breadcrumbs'=>['products']])
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    @include('blog.sidebar')
                </div>
                <div class="col-lg-9 pe-lg-1-9 mb-1-9 mb-lg-0">
                    <div class="row">
                        @foreach($posts as $post)
                        <div class="col-lg-12 mb-1-9">
                            <article class="card blog-card">
                                <div class="row g-0">
                                    <div class="col-md-6">
                                        <div class="card-img bg-img cover-background h-100" data-overlay-dark="0" data-background="{{is_null($post->image_url) === false ? url( $post->image_url) : 'img/blog/default/blog-46.jpg'}}"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card-body">
{{--                                            <div class="d-block text-muted mb-2 small">{{Verta::instance($post->created_at)->format('%d %B %Y')}}</div>--}}
                                            <h3 style="min-height: 50px"><a href="{{route('blog.post',$post)}}">{{\Illuminate\Support\Str::limit($post->langs[0]->title,80,' ...')}}</a></h3>
                                            <p>@php echo \Illuminate\Support\Str::limit($post->langs[0]->description_short,145,' ...') @endphp</p>
                                            <span class="read-more"><a href="{{route('blog.post',$post)}}">@lang('general.ReadeMore')</a></span>

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        @endforeach
                    </div>

                    <div class="row mt-1-9 mt-lg-6">
                        <div class="col-12">
                            {{$posts->links('vendor.pagination.default')}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection
