@extends('layout.app')
@section('title',$post ? $post->langs[0]->title : __('general.Blog'))
@section('keywords',$post ? $post->langs[0]->meta_keyword : __('general.Blog'))
@section('description',$post ? $post->langs[0]->meta_description : __('general.Blog'))
@section('main-content')
    @include('component.header.pageHeader',['img'=>env('APP_URL').'/img/bg/bg15.jpg','title'=>__('general.Blog').' '.$siteConfigs->title[app()->getLocale()],'breadcrumbs'=>['products']])

    <section class="blogs">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    @include('blog.sidebar')
                </div>
                <div class="col-lg-9 pe-lg-1-9 mb-1-9 mb-lg-0">
                    <div class="posts">
                        <div class="post">
                            <div class="post-img">
                                <img src="{{$post->image_url}}" alt="{{$post->langs[0]->title}}">
                            </div>
                            <div class="content">
                                <div class="blog-list-simple-text">
                                    <div class="post-title">
                                        <h5>{{$post->langs[0]->title}}</h5>
                                    </div>
                                    <ul class="meta ps-0">
                                        <li>
                                            <a href="javascript:">
                                                @php $user = \App\Models\User::find($post->user_id) @endphp
                                                <i aria-hidden="true" class="fa fa-user"></i> {{$user->fullName}}
                                            </a>
                                        </li>
                                        <li>
                                            @foreach($categories as $category)
                                                <a href="{{route('blog.category',$category)}}"><i aria-hidden="true" class="fa fa-folder-open"></i> {{$category->title}}</a>,
                                            @endforeach
                                        </li>
                                        <li>
                                            <a href="#!">
                                                <i aria-hidden="true" class="fas fa-calendar-alt"></i> <a href="#">{{$post->created_at}}</a>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-cont">@php echo $post->langs[0]->description @endphp</div>
                                <div class="share-post d-flex">

                                    <div>{{__('general.SharePost')}} : </div>
                                    <ul class="ps-0">
                                        <a href="https://telegram.me/share/url?url={{route('blog.post',$post)}}" class="social-icon" title="تلگرام" target="_blank"><i class="bi bi-telegram"></i></a>
                                        <li><a href="http://www.facebook.com/sharer/sharer.php?m2w&s=100&p[url]={{route('blog.post',$post)}}"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="http://twitter.com/intent/tweet/?text={{route('blog.post',$post)}}"><i class="fab fa-twitter"></i></a></li>
                                        <a href="https://api.whatsapp.com/send?text={{route('blog.post',$post)}}" class="social-icon" title="واتس اپ" target="_blank"><i class="bi bi-whatsapp"></i></a>
                                        <li><a href="http://www.linkedin.com/shareArticle?mini=true&url={{route('blog.post',$post)}}"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @includeWhen(true,'component.home.address',[
'title'=>$siteConfigs->title[app()->getLocale()],
'logoAlternative'=>env('APP_URL') . $siteConfigs->siteLogoUrlAlternative,
'tel1'=>$siteConfigs->tel1,
'tel2'=>$siteConfigs->tel2,
'postalCode'=>$siteConfigs->postalCode,
'address'=>$siteConfigs->address[app()->getLocale()],
'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
'email'=>$siteConfigs->email,
])
@endsection
