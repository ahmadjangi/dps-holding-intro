
<section class="{{$grayBackground ?  'bg-light-gray pb-14 pb-md-18 pb-xl-20 mx-sm-1-9 border-radius-10 overflow-visible' : ''}}" >
    <div class="container">
        <div class="section-heading">
            <h2>{{__('general.OurServices')}}</h2>
        </div>
        <div class="row mt-n1-9">
            @foreach($services as $service)
                <div class="col-md-6 col-lg-4 mt-1-9">
                    <div class="service-block4 h-100">
                        <div class="service-icon">
                            <i class="icon-tools"></i>
                        </div>
                        <div class="service-desc">
                            <h4><a href="{{route('showService',$service->slug)}}"> {{$service->langs[0]->title}}</a></h4>
                            {{--                            <h5>Market Plan</h5>--}}
                            <p class="text-justify" >{{\Illuminate\Support\Str::limit($service->langs[0]->short_description,120,' ...')}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
