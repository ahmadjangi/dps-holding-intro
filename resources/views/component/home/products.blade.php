<section>
    <div class="container">
        <div class="title-style03 text-center position-relative mb-2-3 mb-md-6">
            <h2 class="h1 z-index-1">{{__('general.SomeProducts')}}</h2>
            <span
                class="title-style02 text-secondary opacity06 position-absolute z-index-0 bottom-n5 left-30 left-sm-15 left-md-30 ani-left-right alt-font">{{__('general.HoldingDPS')}}</span>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="horizontaltab tab-style9">
                    <ul class="resp-tabs-list hor_1 mx-auto text-center mb-5">
                        @foreach($products as $product)
                        <li>{{$product->langs[0]->title}}</li>
                        @endforeach
                    </ul>
                    <div class="resp-tabs-container hor_1 p-0">
                        @foreach($products as $product)
                        <div>
                            <div class="row align-items-center">
                                <div class="col-lg-6 mb-4 mb-lg-0">
                                    <div class="pe-lg-1-9">
                                        <h3>{{$product->langs[0]->title}}</h3>
                                        <div class="text-justify">@php echo $product->langs[0]->description @endphp</div>
                                    </div>

                                </div>
                                <div class="col-lg-6 text-center text-lg-start">
                                    <div>
                                        <img src="{{'/storage/productImage/thumbnail/600/'.$product->images[0]->url}}" class="border-radius-10 tilt" alt="{{$product->langs[0]->title}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
{{--                        <div>--}}

{{--                            <div class="row align-items-center">--}}
{{--                                <div class="col-lg-6 text-center text-lg-start mb-4 mb-lg-0">--}}
{{--                                    <div>--}}
{{--                                        <img src="img/content/tab-2.jpg" class="tilt border-radius-10" alt="...">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-6">--}}
{{--                                    <div class="ps-lg-1-9">--}}
{{--                                        <h3>Vision to planning</h3>--}}
{{--                                        <p>Build integration aute irure design in reprehenderit in voluptate velit--}}
{{--                                            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat--}}
{{--                                            cupidatat design proident.</p>--}}

{{--                                        <ul class="list-style-14">--}}
{{--                                            <li>Life time supports</li>--}}
{{--                                            <li>Exclusive design</li>--}}
{{--                                            <li>Solve your problem with us</li>--}}
{{--                                            <li>We Provide Awesome Services</li>--}}
{{--                                            <li>Your business deserves best software</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div>--}}

{{--                            <div class="row align-items-center">--}}
{{--                                <div class="col-lg-6 mb-4 mb-lg-0">--}}
{{--                                    <div class="pe-lg-1-9">--}}
{{--                                        <h3>Plan to marketing</h3>--}}
{{--                                        <p>Ready site integration aute irure design in reprehenderit in voluptate--}}
{{--                                            velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint--}}
{{--                                            occaecat cupidatat design proident.</p>--}}

{{--                                        <ul class="list-style-14">--}}
{{--                                            <li>Creating Stunning design</li>--}}
{{--                                            <li>Solve your problem with us</li>--}}
{{--                                            <li>Grow your business</li>--}}
{{--                                            <li>We Provide Awesome Services</li>--}}
{{--                                            <li>Exclusive design</li>--}}

{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-6 text-center text-lg-start">--}}
{{--                                    <div>--}}
{{--                                        <img src="img/content/tab-3.jpg" class="tilt border-radius-10" alt="...">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div>--}}

{{--                            <div class="row align-items-center">--}}
{{--                                <div class="col-lg-6 mb-4 mb-lg-0 text-center text-lg-start">--}}
{{--                                    <div>--}}
{{--                                        <img src="img/content/tab-4.jpg" class="tilt border-radius-10" alt="...">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-6">--}}
{{--                                    <div class="ps-lg-1-9">--}}
{{--                                        <h3>Start to growth</h3>--}}
{{--                                        <p>Build integration aute irure design in reprehenderit in voluptate velit--}}
{{--                                            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat--}}
{{--                                            cupidatat design proident.</p>--}}

{{--                                        <ul class="list-style-14">--}}
{{--                                            <li>Research & Devloping</li>--}}
{{--                                            <li>Fully customizable</li>--}}
{{--                                            <li>Solve your problem faster</li>--}}
{{--                                            <li>There are many variations</li>--}}
{{--                                            <li>Your business deserves best software</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{env('App_Folder').'/img/shape/11.png'}}" class="position-absolute left-5 bottom-15 ani-move d-none d-xl-block">
</section>
