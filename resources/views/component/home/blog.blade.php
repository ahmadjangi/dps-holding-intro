<section class="pt-0 overflow-visible">
    <div class="container">
        <div class="bg-light-gray border-radius-10 p-1-6 p-sm-1-9 p-lg-10 position-relative">
            <div class="title-style03 text-center mb-2-3 mb-md-6">
                <h2 class="h1">{{__('general.OurLatestNews')}}</h2>
            </div>
            <div class="row mt-n1-9">
                @foreach($blog as $post)
                    <div class="col-md-6 col-lg-4 mt-1-9">
                        <div class="shadow border-radius-10">
                            <img src="{{$post->image_url}}" class="border-top-radius-10" alt="{{$post->langs[0]->title}}">
                            <div class="p-1-6 p-sm-1-9 bg-white border-radius-10">
                                <h3 class="mb-4 h5" style="min-height: 48px"><a href="{{route('blog.post',$post->slug)}}">{{\Illuminate\Support\Str::limit($post->langs[0]->title,58,$end=' ...')}}</a></h3>
                                <div class="d-flex align-items-center justify-content-between">
                                    <div>
                                        {{--                                <img src="img/avatar/avatar-09.jpg" class="w-30px rounded-circle" alt="...">--}}
                                        <span class="small text-primary font-weight-600 ms-1 ms-sm-2">{{$post->user->full_name}}</span>
                                    </div>
                                    <div class="d-flex">
                                        <i class="ti-calendar mx-1"></i>
                                        <span class="small font-weight-600">{{$post->created_at}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <img src="{{env('App_Folder').'/img/shape/12.png'}}" class="position-absolute right-5 bottom-n5 ani-move d-none d-sm-block">
</section>
