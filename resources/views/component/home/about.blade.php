<section class="pt-0 overflow-visible {{'mt-'.$topMargin}}">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            @if($image)
                <div class="col-lg-6 mb-2-3 mb-lg-0">
                    <div class="position-relative pe-xl-1-9">
                        <img src="{{$image}}" class="border-radius-10" alt="دینامیک پایش">
                        <div
                            class="bg-secondary d-inline-block border-radius-10 p-1-9 position-absolute left-5 left-sm-n5 bottom-n5">
                            <p class="text-white mb-1">@lang('general.MoreThan')</p>
                            <h3 class="mb-0 h1 text-white">@lang('general.AdecadeOfExperience')</h3>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-lg-6">
                <div class="ps-xl-2-3 position-relative">
                    <span class="title-style02 text-secondary opacity06 position-absolute z-index-0 top-n10 ani-left-right alt-font">{{__('general.AboutUS')}}</span>
                    <div class="z-index-1 position-relative">
                        <h2 class="mb-lg-4 h2">{{$slogan}}</h2>
                        <p class="mb-1-9 text-justify">{{$about}}</p>
                        <div class="row align-items-center">
                            <div class="col-lg-6">
                                @if($services)
                                    @foreach($services as $service)
                                        @if($loop->iteration ^ 2 === 0)
                                            <div class="d-flex mb-1-9">
                                                <div class="flex-shrink-0">
                                                    <i class="ti-shopping-cart-full display-14 text-primary"></i>
                                                </div>
                                                <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">
                                                    <h4 class="h6">{{$service->langs[0]->title}}</h4>
                                                    <p class="mb-0">{{str_limit($service->langs[0]->title,25,'...')}}</p>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-lg-6">
                                @if($services)
                                    @foreach($services as $service)
                                        @if($loop->iteration ^ 2 !== 0)
                                            <div class="d-flex mb-1-9">
                                                <div class="flex-shrink-0">
                                                    <i class="ti-shopping-cart-full display-14 text-primary"></i>
                                                </div>
                                                <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">
                                                    <h4 class="h6">{{$service->langs[0]->title}}</h4>
                                                    <p class="mb-0">{{str_limit($service->langs[0]->title,25,'...')}}</p>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            {{--                                    <div class="col-lg-6">--}}
                            {{--                                        <div class="d-flex mb-1-9">--}}
                            {{--                                            <div class="flex-shrink-0">--}}
                            {{--                                                <i class="ti-direction-alt display-14 text-primary"></i>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">--}}
                            {{--                                                <h4 class="h6">راهبری تولید</h4>--}}
                            {{--                                                <p class="mb-0">-</p>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="d-flex mb-1-9">--}}
                            {{--                                            <div class="flex-shrink-0">--}}
                            {{--                                                <i class="ti-settings display-14 text-primary"></i>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">--}}
                            {{--                                                <h4 class="h6">ماشین سازی</h4>--}}
                            {{--                                                <p class="mb-0">-</p>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="d-flex">--}}
                            {{--                                            <div class="flex-shrink-0">--}}
                            {{--                                                <i class="ti-package display-14 text-primary"></i>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">--}}
                            {{--                                                <h4 class="h6">تامین کالا</h4>--}}
                            {{--                                                <p class="mb-0">-</p>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
