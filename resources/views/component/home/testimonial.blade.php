<section class="bg-dark pb-24" dir="ltr">
    <div class="container">

        <div class="title-style03 white text-center mb-2-3 mb-md-6">
            <h2 class="h1 text-white">Customer love us</h2>
        </div>

        <div class="testimonial-02 owl-theme owl-carousel" data-slider-id="1">
            <div class="text-center">
                <p class="lead w-md-80 w-lg-65 mx-auto text-white mb-2-3 fst-italic">“Thank you so much for your
                    help. The best on the net! It's really wonderful. We can't understand how we've been living
                    without business. Definitely worth the investment. It's all good.”</p>
                <h5 class="text-white mb-1 h6">Grace Simmons</h5>
                <h6 class="small letter-spacing-1 font-weight-500 text-white opacity6 mb-2-9">Mystery Shopper</h6>
            </div>
            <div class="text-center">
                <p class="lead w-md-80 w-lg-65 mx-auto text-white mb-2-3 fst-italic">“You've saved our business!
                    Business has completely surpassed our expectations. I like business more and more each day
                    because it makes my life a lot easier. Business is the coolest, most happening thing
                    around!”</p>
                <h5 class="text-white mb-1 h6">Andrew Nichols</h5>
                <h6 class="small letter-spacing-1 font-weight-500 text-white opacity6 mb-2-9">Ship Purser</h6>
            </div>
            <div class="text-center">
                <p class="lead w-md-80 w-lg-65 mx-auto text-white mb-2-3 fst-italic">“Business is both attractive
                    and highly adaptable. If you aren't sure, always go for business. Business is exactly what our
                    business has been lacking. It fits our needs perfectly. Really good.”</p>
                <h5 class="text-white mb-1 h6">Russell Long</h5>
                <h6 class="small letter-spacing-1 font-weight-500 text-white opacity6 mb-2-9">Marketing
                    Specialist</h6>
            </div>
        </div>
        <div class="owl-thumbs text-center" data-slider-id="1">
            <button class="owl-thumb-item border-radius-50 w-70px me-1"><img src="img/avatar/avatar-13.jpg"
                                                                             class="border-radius-50" alt="...">
            </button>
            <button class="owl-thumb-item w-70px border-radius-50 me-1"><img src="img/avatar/avatar-14.jpg"
                                                                             class="border-radius-50" alt="...">
            </button>
            <button class="owl-thumb-item w-70px border-radius-50"><img src="img/avatar/avatar-12.jpg"
                                                                        class="border-radius-50" alt="..."></button>
        </div>
    </div>
    <span
        class="title-style02 text-white opacity05 position-absolute z-index-0 bottom-15 bottom-sm-10 left-0 ani-left-right alt-font">feedback</span>
    <img src="img/shape/10.png" class="position-absolute ani-move bottom-0 end-0" alt="...">
    <img src="img/shape/08.png" class="position-absolute ani-top-bottom top-5 right-10" alt="...">
</section>
