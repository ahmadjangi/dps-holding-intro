<section class="pt-0 team-style9 bg-transparent">
    <div class="container">
        <div class="bg-light-gray border-radius-10 p-1-6 p-sm-1-9 p-lg-10 position-relative">
            <div class="title-style03 text-center mb-2-3 mb-lg-6">
                <h2 class="h1">Our team members</h2>
            </div>
            <div class="row mt-n1-9">
                <div class="col-md-6 col-xl-3 mt-1-9">
                    <div class="team-02">
                        <div class="team-img">
                            <div class="team-social-icon">
                                <span class="team-icon fas fa-share-alt"></span>
                                <a href="#!" class="team-icon fab fa-twitter"></a>
                                <a href="#!" class="team-icon fab fa-facebook-f"></a>
                                <a href="#!" class="team-icon fab fa-linkedin-in"></a>
                                <a href="#!" class="team-icon fab fa-instagram"></a>
                            </div>
                            <img src="img/team/team-15.jpg" alt="...">
                        </div>
                        <div class="bg-white pb-4 pt-1-9 px-4 rounded-bottom">
                            <h3 class="h6 mb-0"><a href="#!">Gary Meyers</a></h3>
                            <div class="small">Booking Manager</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3 mt-1-9">
                    <div class="team-02">
                        <div class="team-img">
                            <div class="team-social-icon">
                                <span class="team-icon fas fa-share-alt"></span>
                                <a href="#!" class="team-icon fab fa-twitter"></a>
                                <a href="#!" class="team-icon fab fa-facebook-f"></a>
                                <a href="#!" class="team-icon fab fa-linkedin-in"></a>
                                <a href="#!" class="team-icon fab fa-instagram"></a>
                            </div>
                            <img src="img/team/team-16.jpg" alt="...">
                        </div>
                        <div class="bg-white pb-4 pt-1-9 px-4 rounded-bottom">
                            <h3 class="h6 mb-0"><a href="#!">Alma Dent</a></h3>
                            <div class="small">Business Analyst</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3 mt-1-9">
                    <div class="team-02">
                        <div class="team-img">
                            <div class="team-social-icon">
                                <span class="team-icon fas fa-share-alt"></span>
                                <a href="#!" class="team-icon fab fa-twitter"></a>
                                <a href="#!" class="team-icon fab fa-facebook-f"></a>
                                <a href="#!" class="team-icon fab fa-linkedin-in"></a>
                                <a href="#!" class="team-icon fab fa-instagram"></a>
                            </div>
                            <img src="img/team/team-17.jpg" alt="...">
                        </div>
                        <div class="bg-white pb-4 pt-1-9 px-4 rounded-bottom">
                            <h3 class="h6 mb-0"><a href="#!">Julianne Luna</a></h3>
                            <div class="small">Commercial Specialist</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3 mt-1-9">
                    <div class="team-02">
                        <div class="team-img">
                            <div class="team-social-icon">
                                <span class="team-icon fas fa-share-alt"></span>
                                <a href="#!" class="team-icon fab fa-twitter"></a>
                                <a href="#!" class="team-icon fab fa-facebook-f"></a>
                                <a href="#!" class="team-icon fab fa-linkedin-in"></a>
                                <a href="#!" class="team-icon fab fa-instagram"></a>
                            </div>
                            <img src="img/team/team-18.jpg" alt="...">
                        </div>
                        <div class="bg-white pb-4 pt-1-9 px-4 rounded-bottom">
                            <h3 class="h6 mb-0"><a href="#!">Joseph Cardenas</a></h3>
                            <div class="small">Business Consultant</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5 pt-5 border-top align-items-center z-index-1 position-relative">
                <div class="col-lg-8">
                    <div>
                        <h2 class="h3 mb-3">We care about our business growth</h2>
                        <p class="mb-0 w-lg-90">Growth of business processes to the web, with companies having a
                            greater ability and interest in adopting new technologies that are being developed by
                            many businesses around us.</p>
                    </div>
                </div>
                <div class="col-lg-4 text-end d-none d-lg-block">
                    <img src="img/content/about-12.jpg" class="border-radius-10" alt="...">
                </div>
            </div>
            <img src="img/bg/bg16.png" class="position-absolute bottom-0 z-index-0 start-0 opacity1" alt="...">
        </div>
    </div>
</section>
