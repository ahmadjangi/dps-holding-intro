<div class="overflow-visible">
    <div class="container">
        <div class="row footer-address">
            <div class="col-lg-3">
                <div class="bg-secondary p-1-9 border-radius-10 text-center h-100">
                    <img src="{{$logoAlternative}}" alt="{{$title}}">
                </div>
            </div>
            <div class="col-lg-9">
                <div class="bg-white p-1-9 shadow border-radius-10 position-relative">
                    <div class="row">
                        <div class="col-md-5 mb-3 mb-md-0">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0">
                                    <i class="ti-location-pin display-26 text-secondary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    @if(!empty($address))
                                        <p class="mb-0">{{$address}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1 mx-3">
                                    @if(!empty($addressTwo))
                                        <p class="mb-0 ms-3">{{$addressTwo}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3 mb-md-0">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0">
                                    <i class="ti-mobile display-26 text-secondary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    @if(!empty($tel1))
                                        <p class="mb-0">
                                            <a href="tel:{{$tel1}}">{{$tel1}}</a>
                                        </p>
                                    @endif
                                    @if(!empty($tel2))
                                        <p class="mb-0">
                                            <a href="tel:{{$tel2}}">{{$tel2}}</a>
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0">
                                    <i class="ti-email display-26 text-secondary"></i>
                                </div>
                                <div class="flex-grow-1 mx-3">
                                    @if(!empty($email))
                                        <p class="mb-0">
                                            <a href="mailto:{{$email}}">{{$email}}</a>
                                        <p class="mb-0">
                                    @endif
                                    @if(!empty($postalCode))
                                        <p>@lang('general.PostalCode'): {{$postalCode}}</p>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
