<section class="pt-sm-0 mt-sm-n6 bg-transparent z-index-3 position-relative">
    <div class="container">
        <div class="row mt-n1-9">
            <div class="col-md-6 col-lg-4 mt-1-9">
                <div class="d-flex bg-white p-1-9 p-lg-2-3 shadow border-radius-10 h-100">
                    <div class="flex-shrink-0">
                        <i class="icon-recycle display-15 text-secondary"></i>
                    </div>
                    <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">
                        <h4 class="h6">@lang('general.PaperAndCardboardProductionLines')</h4>
                        <p class="small mb-0">@lang('general.ImplementationOfZero')</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mt-1-9">
                <div class="d-flex bg-white p-1-9 p-lg-2-3 shadow border-radius-10 h-100">
                    <div class="flex-shrink-0">
                        <i class="icon-gears display-15 text-secondary"></i>
                    </div>
                    <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">
                        <h4 class="h6">@lang('general.Supply')</h4>
                        <p class="small mb-0">@lang('general.SupplyDyeStarch')</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mt-1-9">
                <div class="d-flex bg-white p-1-9 p-lg-2-3 shadow border-radius-10 h-100">
                    <div class="flex-shrink-0">
                        <i class="icon-hotairballoon display-15 text-secondary"></i>
                    </div>
                    <div class="flex-grow-1 {{app()->getLocale() === 'fa' ? ' ms-3' : ' me-3'}}">
                        <h4 class="h6">@lang('general.EngineeringAndConsultingServices')</h4>
                        <p class="small mb-0">@lang('general.FeasibilityAnalysisOfTheIndustry')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><?php
