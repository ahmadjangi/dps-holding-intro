<section class="bg-light-gray pb-14 pb-md-18 pb-xl-20 mx-sm-1-9 border-radius-10 overflow-visible">
    <div class="container">
        <div class="title-style03 text-center mb-2-3 mb-md-6">
            <h2 class="h1 z-index-1">{{(__('general.DynamicPayeshHoldingService'))}}</h2>
        </div>
        <div class="row mt-n1-9">
            @foreach($specialServices as $service)
                <div class="col-md-6 col-lg-4 mt-1-9">
                    <div class="service-02">
                        <img class="border-radius-10" src="{{env('APP_URL').'/storage/serviceImage/thumbnail/551/'.$service->img_url}}" alt="{{$service->langs[0]->title}}">
                        <div class="opacity-light bg-extra-dark-gray border-radius-10"></div>
                        <div class="service-overlay bg-dark"></div>
                        <div class="service-inner">
                            <div class="service-content align-items-start px-2-3 py-2-9">
                                <span class="text-white d-block mb-2 position-relative z-index-1">0{{$loop->iteration}}</span>
                                <h5 class="text-white position-relative z-index-1"><a class="text-white" href="{{route('showService',$service->slug)}}">{{$service->langs[0]->title}}</a> </h5>
{{--                                <span class="service-icon"><i class="icon-strategy text-white display-15"></i></span>--}}
                            </div>
                            <div class="service-text align-items-end d-flex">
                                <div class="p-2-3">
                                    <p class="text-white mb-3">{{$service->langs[0]->short_description}}</p>
                                    <a href="{{$service->slug}}" class="text-white">{{__('general.ReadeMore')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <span
        class="title-style02 text-primary opacity05 position-absolute z-index-0 bottom-5 right-5 ani-left-right alt-font">{{__('general.service')}}</span>
    <img src="img/shape/07.png" class="position-absolute bottom-0 start-0 opacity2 ani-top-bottom d-none d-md-block"
         alt="...">
    <img src="img/shape/09.png" class="position-absolute top-n10 right-5 ani-top-bottom d-none d-md-block"
         alt="...">
</section>
