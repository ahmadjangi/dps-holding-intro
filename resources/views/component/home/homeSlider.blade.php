<section class="screen-height py-0 top-position1" dir="ltr">
    <div class="container-fluid px-0">
        <div class="slider-fade1 owl-carousel owl-theme w-100">
            @foreach($slides as $slide)
                <div class="item bg-img cover-background" data-overlay-dark="5"
                     data-background="{{$slide->image_url}}">
                    <div class="container h-100">
                        <div class="d-table w-100 h-100">
                            <div class="d-table-cell align-middle">
                                <div class="row align-items-center {{app()->getLocale() === 'fa' ? 'justify-content-end' : ''}}">
                                    <div class="col-md-10 col-lg-8 col-xl-7 col-xxl-6 z-index-3 position-relative">
                                        <h1 class="display-19 display-sm-13 display-md-9 display-lg-8 display-xl-4 text-white mb-3 {{app()->getLocale() === 'fa' ? 'text-start' : ''}}">
                                            {{$slide->langs[0]->title}}
                                        </h1>
                                        <p class="lead w-95 w-md-90 w-lg-85 mb-2-2 text-white opacity8 d-none d-sm-block {{app()->getLocale() === 'fa' ? 'text-start' : ''}}">
                                            {{$slide->description}}
                                        </p>
                                        @if($slide->url !== '#')
                                            <a href="{{$slide->url}}" class="butn-style3 white-hover me-2 my-1 my-sm-0">
                                                <span>@lang('general.ReadeMore')</span>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <img src="img/shape/11.png" class="position-absolute right-10 top-20 ani-top-bottom z-index-3">
    <img src="img/shape/10.png" class="position-absolute left-5 top-15 ani-move d-none d-xl-block z-index-3">
</section>
