<section class="p-0 mx-md-9 mt-n6 bg-transparent" dir="ltr">
    <div class="container-fluid">
        <div class="bg-secondary px-4 px-sm-5 py-1-9 border-radius-10">
            <div class="owl-carousel owl-theme client-02">
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/01.png" alt="...">
                    <img class="main-image" src="img/partners/09.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/02.png" alt="...">
                    <img class="main-image" src="img/partners/10.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/03.png" alt="...">
                    <img class="main-image" src="img/partners/11.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/04.png" alt="...">
                    <img class="main-image" src="img/partners/12.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/05.png" alt="...">
                    <img class="main-image" src="img/partners/13.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/06.png" alt="...">
                    <img class="main-image" src="img/partners/14.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/07.png" alt="...">
                    <img class="main-image" src="img/partners/15.png" alt="...">
                </div>
                <div class="image-wrapper">
                    <img class="hover-image" src="img/partners/08.png" alt="...">
                    <img class="main-image" src="img/partners/16.png" alt="...">
                </div>
            </div>
        </div>
    </div>
</section>
