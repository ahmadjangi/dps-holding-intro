<ul class="navbar-nav {{app()->getLocale() === 'fa' ? 'ms-auto' : 'me-auto'}}" id="nav" style="display: none;">
    <li><a href="{{route('home')}}">{{__('general.HoldingDPS')}}</a>
        <ul>
            <li><a href="{{route('honors')}}">{{__('general.HonorsAndStatues')}}</a></li>
            <li><a href="#">{{__('general.Certificates')}}</a></li>
        </ul>
    </li>
    <li><a href="#">{{__('general.SubsidiaryCompanies')}}</a>
        <ul>
            @foreach(\App\Models\SubsetCompanies::where('published',1)->with('langs',function ($query){
    $query->where('lang_id',app()->getLocale());
})->get() as $substCompany)
                <li>
                    <a href="{{route('showSubsetCompany',$substCompany->slug)}}">{{$substCompany->langs[0]->title}}</a>
                </li>
            @endforeach
        </ul>
    </li>
    <li><a href="{{route('products')}}">{{__('general.Products')}}</a></li>
    <li><a href="{{route('services')}}">{{__('general.Services')}}</a></li>
    <li><a href="{{route('blog')}}">{{__('general.News')}}</a></li>
    <li><a href="{{route('about')}}">{{__('general.AboutUS')}}</a></li>
    <li><a href="{{route('contact.show')}}">{{__('general.ContactUs')}}</a></li>
</ul>

