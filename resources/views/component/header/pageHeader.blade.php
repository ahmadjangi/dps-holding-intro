<section class="page-title-section2 bg-img cover-background top-position1" data-overlay-dark="6"
         data-background="{{$img}}">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{$title}}</h1>
            </div>
            @if($breadcrumbs)
            <div class="col-md-12">
                <ul class="ps-0">
                    <li><a href="{{route('home')}}">{{__('general.Home')}}</a></li>
                    @foreach($breadcrumbs as $breadcrumb)
                        <li><a href="{{route($breadcrumb)}}">{{$title}}</a></li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</section>
