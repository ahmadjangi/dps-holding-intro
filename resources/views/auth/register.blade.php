@extends('layout.auth')
@section('title','ثبت نام' )
@section('main-content')
    <main class="main">
        <div class="page-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="login">
                        <div class="card">
                            <div class="header text-center mb-3">
                                <a href="/" class="logo">
                                    <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
                                </a>
                                <h5><strong>ثبت نام</strong></h5>
                            </div>

                            <form id="login">
                                <div class="form-group">
                                    <label for="register-password"> کد ارسال شده شماره موبایل {{session('mobile')}} را وارد نمایید</label>
                                    <div class="d-flex">
                                        <input id="verifyCode_one" type="text" pattern="\d*" tabindex="1" class="form-control verifyCode order-4 text-center" name="verifyCode_one" maxlength="1" size="1">
                                        <input id="verifyCode_two" type="text" pattern="\d*" tabindex="2" class="form-control verifyCode order-3 text-center"  name="verifyCode_two" maxlength="1" size="1">
                                        <input id="verifyCode_three" type="text" pattern="\d*" tabindex="3" class="form-control verifyCode order-2 text-center"  name="verifyCode_three" maxlength="1" size="1">
                                        <input id="verifyCode_four" type="text" pattern="\d*" tabindex="4" class="form-control verifyCode order-1 text-center"  name="verifyCode_four" maxlength="1" size="1">
                                    </div>

                                </div>
                                {{--                                <div class="form-group">--}}
                                {{--                                    <label for="register-password"> کد ارسال شده شماره موبایل {{session('mobile')}} را وارد نمایید</label>--}}
                                {{--                                    <input id="verifyCode" type="password" class="form-control" value="{{session('verifyCode')}}" name="verifyCode" maxlength="6">--}}
                                {{--                                    <label class="error" >فروشگاه آزمایشی است و نیازی به دریافت پیامک تایید نیست</label>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="form-footer">--}}
                                {{--                                    <button type="submit" class="btn btn-outline-primary-2 w-100 register-form-submit">--}}
                                {{--                                        <span>ثبت نام</span>--}}
                                {{--                                    </button>--}}
                                {{--                                </div>--}}

                                <div class="form-footer mt-3">
                                    <button type="submit" class="btn-primary-t2 w-100 register-form-submit">
                                        <span class="verify-btn__cation">ثبت نام</span>
                                        <span class="verify-btn__animate-cation" style="display: none">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                                                      <rect x="0" y="10" width="4" height="10" fill="#fff" opacity="0.2">
                                                        <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                                                      </rect>
                                                      <rect x="8" y="10" width="4" height="10" fill="#fff"  opacity="0.2">
                                                        <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                                      </rect>
                                                      <rect x="16" y="10" width="4" height="10" fill="#fff"  opacity="0.2">
                                                        <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                                      </rect>
                                                </svg>
                                            </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script>

        $(document).ready(function () {

            let verifyMobileElement = document.querySelector('.verifyCode');
            if (verifyMobileElement !== null){
                verifyMobileElement.focus();
                document.querySelectorAll('.verifyCode').forEach(forfnc =>{
                    forfnc.addEventListener('keyup',fnc =>{

                        let nextElemnt = fnc.currentTarget.nextElementSibling;

                        if (fnc.currentTarget.id === 'verifyCode_four'){
                            $( "#login" ).submit()
                        }else {
                            nextElemnt.focus()
                        }
                    });
                })
            }

            $( "#login" ).validate({
                rules: {
                    verifyCode: {},
                },
                messages: {
                    verifyCode: {},
                },
                submitHandler: function (form) {

                    let verifyCode_one = $('#verifyCode_one').val();
                    let verifyCode_two = $('#verifyCode_two').val();
                    let verifyCode_three = $('#verifyCode_three').val();
                    let verifyCode_four = $('#verifyCode_four').val();

                    if(verifyCode_one !== '' && verifyCode_two !== '' && verifyCode_three !== '' && verifyCode_four !== ''){
                        register();
                        return false;
                    }else {
                        toastr.warning('کد تایید را وارد نمایید.');
                        return false;
                    }
                }
            });
        })

        function register(){


            let verifyCode_one = $('#verifyCode_one').val() === undefined ? null : $('#verifyCode_one').val();
            let verifyCode_two = $('#verifyCode_two').val() === undefined ? null : $('#verifyCode_two').val();
            let verifyCode_three = $('#verifyCode_three').val() === undefined ? null : $('#verifyCode_three').val();
            let verifyCode_four = $('#verifyCode_four').val() === undefined ? null : $('#verifyCode_four').val();

            let verifyCode = verifyCode_one + verifyCode_two + verifyCode_three + verifyCode_four

            $.ajax( {
                url: "{{route('register')}}",
                method:'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    verifyCode:verifyCode === 0 ? undefined : verifyCode,
                },
                beforeSend: function(){

                    $('#verifyCode_one').attr('disabled','disabled')
                    $('#verifyCode_two').attr('disabled','disabled')
                    $('#verifyCode_three').attr('disabled','disabled')
                    $('#verifyCode_four').attr('disabled','disabled')
                    $('.register-form-submit').attr('disabled','disabled')
                    $('.verify-btn__cation').addClass('d-none')
                    $('.verify-btn__animate-cation').addClass('d-block')
                },
                success: function( res ) {

                    if(res.data.redirectUrl){
                        window.location.replace(res.data.redirectUrl)
                    }
                },
                error:function(err){

                    $('#verifyCode_one').attr('disabled',false)
                    $('#verifyCode_two').attr('disabled',false)
                    $('#verifyCode_three').attr('disabled',false)
                    $('#verifyCode_four').attr('disabled',false)
                    $('.register-form-submit').attr('disabled',false)
                    verifyCode_one = $('#verifyCode_one').val('');
                    verifyCode_two = $('#verifyCode_two').val('');
                    verifyCode_three = $('#verifyCode_three').val('');
                    verifyCode_four = $('#verifyCode_four').val('');
                    $('.verify-btn__cation').removeClass('d-none')
                    $('.verify-btn__animate-cation').removeClass('d-block')

                    let verifyMobileElement = document.querySelector('.verifyCode');
                    if (verifyMobileElement !== null) {

                        verifyMobileElement.focus();
                    }

                    if( err.status === 422 ) {
                        toastr.options.rtl = true;
                        toastr.warning(err.responseJSON.status.message);
                    }else if(err.status === 419){
                        window.location.replace('/login-register')
                    }else if(err.status === 402){
                        toastr.options.rtl = true;
                        let errors = $.parseJSON(err.responseText);
                        $.each(errors['data'], function (key, val) {
                            toastr.error(val[0]);
                        });
                    }else if(err.status === 500){
                        toastr.options.rtl = true;
                        err = JSON.parse(err.responseText);
                        toastr.error(err.message);
                    }
                }
            } );
        }

    </script>

@endsection
