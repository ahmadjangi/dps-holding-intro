@extends('layout.auth')
@section('title','بازیابی رمزعبور')
@section('main-content')
    <main class="main">
        <div class="page-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="login">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="card">
                            <div class="header text-center mb-3">
                                <a href="/" class="logo">
                                    <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
                                </a>
                                <h5><strong>کد تایید را وارد نمایید</strong></h5>
                            </div>

                            <form id="login" action="{{route('auth.showNewPassword')}}" method="POST">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="mobile">کد تایید برای شماره موبایل {{$mobile}} ارسال گردید</label>
                                    <input id="verifyCode" type="text" class="form-control" name="verifyCode" maxlength="4" minlength="4">
                                    <input id="mobile" type="hidden" value="{{$mobile}}" name="mobile" maxlength="11" minlength="11">
                                </div>

                                <div class="form-footer">
                                    <button type="submit" class="btn-primary-t2 w-100 login-form-submit mb-2">
                                        <span>ادامه</span>
                                    </button>
                                    <div class="d-flex">
                                        <a class="btn-primary-t2 w-100 text-center login-form-submit" href="{{ route('password.request') }}">
                                            <span>بازگشت</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
