@extends('layout.auth')
@section('title','بازیابی رمزعبور')
@section('main-content')
    <main class="main">
        <div class="page-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="login">
                        <div class="card">
                            <div class="header text-center mb-3">
                                <a href="/" class="logo">
                                    <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
                                </a>
                                <h5><strong>تعیین رمز عبور جدید</strong></h5>
                            </div>

                            <form id="login" action="{{route('auth.newPassword')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="password">رمز عبور جدید</label>
                                    <input id="password" type="password" class="form-control" name="password" minlength="8">
                                </div>
                                <div class="form-group">
                                    <label for="mobile">تکرار رمز عبور جدید</label>
                                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" minlength="8">
                                    <input id="mobile" type="hidden" value="{{$mobile}}" name="mobile" minlength="11">
                                </div>

                                <div class="form-footer mt-3">
                                    <button type="submit" class="btn-primary-t2 w-100 login-form-submit mb-2">
                                        <span>ادامه</span>
                                    </button>
                                    <div class="d-flex">
                                        <a class="btn-primary-t2 text-center w-100 login-form-submit" href="{{ route('password.request') }}">
                                            <span>بازگشت</span>
                                        </a>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
