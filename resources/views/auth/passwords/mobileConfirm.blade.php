@extends('layout.auth')
@section('title','بازیابی رمزعبور')
@section('main-content')
    <main class="main rtl">
        <div class="page-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <div class="card login ">
                            <div class="header center mb-2">
                                <a href="/" class="logo">
                                    <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
                                </a>

                                <div class="alert text-center mb-3" role="alert">
                                    رمز عبور با موفقیت تغییر یافت.
                                </div>
                            </div>
                            <div class="form-footer">
                                <a class="btn btn-outline-primary-2 w-100 login-form-submit" href="{{ route('auth.doLoginRegister') }}">
                                    <span>ورود به سایت</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
