@extends('layout.auth')
@section('title','بازیابی رمزعبور')
@section('main-content')
    <main class="main rtl">
        <div class="page-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="login">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                            </div>
                        @endif
                        <div class="card">
                            <div class="header text-center mb-3">
                                <a href="/" class="logo">
                                    <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
                                </a>
                                <h5><strong>درخواست بازیابی رمز عبور</strong></h5>

                            </div>
                            <form id="login" action="{{route('auth.sendVerifyCode')}}" method="GET">
                                @csrf
                                <div class="form-group">
                                    <label for="mobile">لطفا شماره موبایل خود را وارد نمایید.</label>
                                    <input id="mobile" type="text" class="form-control" name="mobile" maxlength="11" minlength="11">
                                </div>

                                <div class="form-footer mt-3">
                                    <button type="submit" class="btn-primary-t2 w-100 login-form-submit">
                                        <span>ادامه</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
