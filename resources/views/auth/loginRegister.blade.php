@extends('layout.auth')
@section('title','ورود / ثبت نام' )
@section('main-content')
    <main class="main rtl">
        <div class="page-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="login">
                        <div class="card">
                            <div class="header text-center mb-3">
                                <a href="/" class="logo">
                                    <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
                                </a>
                                <h5><strong>ورود / ثبت نام</strong></h5>
                            </div>

                            <form id="login" class="login__form">
                                <div class="form-group">
                                    <label for="email">شماره موبایل خود را وارد نمایید</label>
                                    <input id="emailMobile" type="number" class="form-control" name="emailMobile">
                                    <label id="emailMobile-error" class="error" for="emailMobile"></label>
                                </div>

                                <div class="form-footer">
                                    <button type="button" class="btn-primary-t2 w-100 login-form-submit">
                                        <span class="login-btn__cation">ورود به {{$siteConfigs->title[app()->getLocale()]}}</span>
                                        <span class="login-btn__animate-cation" style="display: none">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                                                      <rect x="0" y="10" width="4" height="10" fill="#fff" opacity="0.2">
                                                        <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
                                                      </rect>
                                                      <rect x="8" y="10" width="4" height="10" fill="#fff"  opacity="0.2">
                                                        <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
                                                      </rect>
                                                      <rect x="16" y="10" width="4" height="10" fill="#fff"  opacity="0.2">
                                                        <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                                        <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
                                                      </rect>
                                                </svg>
                                            </span>
                                    </button>
                                </div>
                                <p class="text-center mt-3 text-small">با ورود و یا ثبت نام در {{$siteConfigs->title[app()->getLocale()]}} شما شرایط و قوانین استفاده از سرویس های سایت و قوانین حریم خصوصی آن را می‌پذیرید.</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script>

        $().ready(function () {
            $('.login-form-submit').click(function () {

                let emailMobile = $('#emailMobile').val()

                if(emailMobile === ''){
                    $('#emailMobile-error').html('شماره موبایل یا ایمیل را وارد نمایید')
                    return
                }else{
                    let numberRegex = new RegExp('^[0-9]+$');
                    let mobileRegex = new RegExp('^(\\+98|0)?9\\d{9}$');
                    if (numberRegex.test(emailMobile)){
                        if(!mobileRegex.test(emailMobile)){
                            $('#emailMobile-error').html('شماره موبایل یا ایمیل نامعتبر است')
                            return;
                        }
                    }else {
                        const emailRegex = new RegExp('^(([^<>()[\\]\\\\.,;:\\s@"]+(\\.[^<>()[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$');
                        if(!emailRegex.test(String(emailMobile).toLowerCase())){
                            $('#emailMobile-error').html('شماره موبایل یا ایمیل نا معتبر است')
                            return;
                        }
                    }
                }

                $('#emailMobile-error').html('')
                login();
            })
        });

        function login(){

            let emailMobile = $('#emailMobile').val();

            $.ajax( {
                url: "{{route('auth.doLoginRegister')}}",
                method:'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    emailMobile:emailMobile,
                },
                beforeSend: function(){

                    $('#emailMobile').attr('disabled','disabled')
                    $('.login-form-submit').attr('disabled','disabled')
                    $('.login-btn__cation').addClass('d-none')
                    $('.login-btn__animate-cation').addClass('d-block')
                },
                success: function( res ) {
                    if(res.data.redirectUrl){
                        window.location.replace(res.data.redirectUrl)
                    }
                },
                error:function(err){

                    $('#emailMobile').attr('disabled',false)
                    $('.login-form-submit').attr('disabled',false)
                    $('#emailMobile').val('');
                    $('.login-btn__cation').removeClass('d-none')
                    $('.login-btn__animate-cation').removeClass('d-block')

                    let emailMobile = document.querySelector('.emailMobile');
                    if (emailMobile !== null) {
                        emailMobile.focus();
                    }

                    toastr.options.rtl = true;
                    if( err.status === 422 ) {
                        toastr.warning(err.responseJSON.status.message);
                    }else if(err.status === 419) {
                        window.location.replace('/login-register')
                    }else if(err.status === 403){
                        toastr.warning(err.responseJSON.status.message);
                    }else if(err.status === 500){

                        toastr.error(err.responseJSON.status.message);
                    }
                }
            });
        }

    </script>
@endsection

