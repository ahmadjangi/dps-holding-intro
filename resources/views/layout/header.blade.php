<header class="header-style1 menu_area-light">

    <div class="navbar-default border-bottom border-color-light-white">
        <div class="top-search bg-primary">
            <div class="container-fluid px-lg-1-6 px-xl-2-5 px-xxl-2-9">
                <form class="search-form" action="search.html" method="GET" accept-charset="utf-8">
                    <div class="input-group">
                                <span class="input-group-addon cursor-pointer">
                                    <button class="search-form_submit fas fa-search text-white" type="submit"></button>
                                </span>
                        <input type="text" class="search-form_input form-control" name="s" autocomplete="off" placeholder="Type & hit enter...">
                        <span class="input-group-addon close-search mt-1"><i class="fas fa-times"></i></span>
                    </div>
                </form>
            </div>
        </div>

        <div class="container-fluid px-lg-1-6 px-xl-2-5 px-xxl-2-9">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="menu_area alt-font">
                        <nav class="navbar navbar-expand-lg navbar-light p-0">
                            <div class="navbar-header navbar-header-custom">
                                <a href="{{route('home')}}" class="navbar-brand h-default logo5"><img id="logo" src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}" alt="logo"></a>
                            </div>
                            <div class="navbar-toggler"></div>
                            @include('component.header.manMenu')
                            <div class="attr-nav align-items-lg-center main-font {{app()->getLocale() === 'fa' ? 'ms-lg-auto' : 'me-lg-auto'}}">
                                <p class="m-0">
                                    @if(app()->getLocale() =='fa')
                                        <a href="{{route('changeLang','en')}}"><span class="text-white">English</span></a>
                                    @else
                                        <a href="{{route('changeLang','fa')}}" style="font-family: iranyekan"><span class="text-white">فارسی</span></a>
                                    @endif
                                </p>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
