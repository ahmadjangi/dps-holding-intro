<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="AhmadJangi">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset("images/icons/favicon.ico")}}">
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}



<!-- Favicon -->
{{--    --}}
{{--    <meta name="apple-mobile-web-app-title" content="LaraMode">--}}
{{--    <meta name="application-name" content="LaraMode">--}}
{{--    <meta name="msapplication-TileColor" content="#cc9966">--}}
{{--    <meta name="msapplication-config" content="{{asset("images/icons/browserconfig.xml")}}">--}}
{{--    <meta name="theme-color" content="#ffffff">--}}
{{--    <link rel="stylesheet" href="{{asset("vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css")}}">--}}
<!-- Plugins CSS File -->
    {{--    <link rel="stylesheet" href="{{asset("css/bootstrap-rtl.min.css")}}">--}}
    {{--    <link rel="stylesheet" href="{{asset("css/plugins/owl-carousel/owl.carousel.css")}}">--}}
    {{--    <link rel="stylesheet" href="{{asset("css/plugins/magnific-popup/magnific-popup.css")}}">--}}
    {{--    <link rel="stylesheet" href="{{asset("css/plugins/jquery.countdown.css")}}">--}}
    {{--    <link rel="stylesheet" href="{{ asset('css/plugins/jquery-ui.min.css') }}">--}}
    {{--    <!-- Main CSS File -->--}}
    {{--    <link rel="stylesheet" href="{{asset("css/style.css")}}">--}}
    {{--    <link rel="stylesheet" href="{{asset("css/demos/demo-18.css")}}">--}}
    {{--    <link rel="stylesheet" href="{{asset("css/custom.css")}}">--}}
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    @yield('styles')
</head>
<body>
    <div class="page-wrapper" id="app">
        @yield('main-content')
    </div><!-- End .page-wrapper -->

{{--    <div class="container newsletter-popup-container mfp-hide" id="newsletter-popup-form">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-10">--}}
{{--                <div class="row no-gutters bg-white newsletter-popup-content">--}}
{{--                    <div class="col-xl-3-5col col-lg-7 banner-content-wrap">--}}
{{--                        <div class="banner-content text-center">--}}
{{--                            <img src="assets/images/popup/newsletter/logo.png" class="logo" alt="logo" width="60" height="15">--}}
{{--                            <h2 class="banner-title">get <span>25<light>%</light></span> off</h2>--}}
{{--                            <p>Subscribe to the Molla eCommerce newsletter to receive timely updates from your favorite category.</p>--}}
{{--                            <form action="#">--}}
{{--                                <div class="input-group input-group-round">--}}
{{--                                    <input type="email" class="form-control form-control-white" placeholder="Your Email Address" aria-label="Email Adress" required>--}}
{{--                                    <div class="input-group-append">--}}
{{--                                        <button class="btn" type="submit"><span>go</span></button>--}}
{{--                                    </div><!-- .End .input-group-append -->--}}
{{--                                </div><!-- .End .input-group -->--}}
{{--                            </form>--}}
{{--                            <div class="custom-control custom-checkbox">--}}
{{--                                <input type="checkbox" class="custom-control-input" id="register-policy-2" required>--}}
{{--                                <label class="custom-control-label" for="register-policy-2">Do not show this popup again</label>--}}
{{--                            </div><!-- End .custom-checkbox -->--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-2-5col col-lg-5 ">--}}
{{--                        <img src="assets/images/popup/newsletter/img-1.jpg" class="newsletter-img" alt="newsletter">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

<!-- Plugins JS File -->

<script src="{{asset("js/app.js")}}"></script>
{{--<script src="{{asset('js/jquery-ui.min.js') }}"></script>--}}
{{--<script src="{{asset("js/bootstrap.bundle.min.js")}}"></script>--}}
{{--<script src="{{asset("js/bootstrap-input-spinner.js")}}"></script>--}}
{{--<script src="{{asset("js/jquery.plugin.min.js")}}"></script>--}}
<!-- Main JS File -->
{{--<script src="{{asset("js/main.js")}}"></script>--}}
{{--<script src="{{asset("js/demos/demo-4.js")}}"></script>--}}
@yield('scripts')
</body>
</html>
