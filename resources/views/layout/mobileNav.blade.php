<div class="offcanvas offcanvas-end side--navbar" tabindex="-1" id="offcanvasRight"
     aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <div class="logo__image">
            <img src="{{env('APP_URL') . $siteConfigs->siteLogoUrl}}" alt="{{$siteConfigs->title[app()->getLocale()]}}">
        </div>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body p-0 pt-2">
        <div class="nav flex-column">
            @foreach($navTreeCategories as $category)
                @if(array_key_exists('children',$category))
                    <a href="#{{$category['slug']}}" class="nav-link collapsed side--navbar-parent"
                       data-bs-toggle="collapse"
                       role="button">{{$category['name']}}</a>
                    @if(count($category['children']))
                        <div class="collapse ps-2 side--navbar-level" id="{{$category['slug']}}"
                             data-bs-parent="#sidebar">
                            @foreach($category['children'] as $subcategory)
                                @if(array_key_exists('children',$subcategory))
                                    <a href="#{{$category['slug'].$subcategory['slug']}}" class="nav-link"
                                       data-bs-toggle="collapse" aria-expanded="false">{{$subcategory['name']}}</a>
                                    @if(count($subcategory['children']))
                                        <div class="collapse ps-2 side--navbar-level"
                                             id="{{$category['slug'].$subcategory['slug']}}"
                                             data-bs-parent="#{{$category['slug']}}">
                                            <a href="{{route('showCategory',$subcategory['slug'])}}"
                                               class="nav-link side--navbar-category-link">همه موارد این دسته</a>
                                            @foreach($subcategory['children'] as $item)
                                                <a href="{{route('showCategory',$item['slug'])}}"
                                                   class="nav-link">{{$item['name']}}</a>
                                            @endforeach
                                        </div>
                                    @endif
                                @else
                                    <a href="{{route('showCategory',$subcategory['slug'])}}"
                                       class="nav-link">{{$subcategory['name']}}</a>
                                @endif
                            @endforeach
                        </div>
                    @endif
                @else
                    <a href="{{route('showCategory',$category['slug'])}}"
                       class="nav-link side--navbar-parent">{{$category['name']}}</a>
                @endif
            @endforeach
        </div>
    </div>
</div>
