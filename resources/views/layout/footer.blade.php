<footer class="bg-dark pt-15">
    <div class="container">
        <div class="row mt-n1-9">

            <div class="col-md-6 col-lg-4 mt-1-9 wow fadeIn" data-wow-delay="100ms">

                <h3 class="footer-title-style15 mb-4">{{__('footer.AboutUS')}}</h3>
                <p class="text-white mb-1-9 text-justify" style="text-align: justify">{{$siteConfigs->shortDescription[app()->getLocale()]}}</p>
                <ul class="ps-0 social-list1 list-unstyled mb-0">
                    <li><a href="#!"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#!"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#!"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="#!"><i class="fab fa-youtube"></i></a></li>
                    <li><a href="#!"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>

            <div class="col-md-6 col-lg-4 mt-1-9 wow fadeIn" data-wow-delay="200ms">
                <div class="pe-lg-2-3">
                    <h3 class="footer-title-style15 mb-4">@lang('general.QuickAccess')</h3>
                    <ul class="footer-list-style2 ps-0">
                        <li><a href="{{route('contact.show')}}">{{__('general.ContactUs')}}</a></li>
                        <li><a href="{{route('about')}}">{{__('general.AboutUS')}}</a></li>
                        <li><a href="#">@lang('general.DibaApplication')</a></li>
                        <li><a href="#">@lang('general.Catalogs')</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mt-1-9 wow fadeIn" data-wow-delay="400ms">
                <h3 class="footer-title-style15 mb-2">@lang('general.SubmitComment')</h3>
                <form class="quform" action="" method="post" enctype="multipart/form-data" onclick="">
                    <div class="quform-elements">
                        <div class="row">
                            <!-- Begin Text input element -->
                            <div class="col-md-12 mb-3">
                                <div class="quform-element">
                                    <div class="quform-input">
                                        <p class="mb-2 display-32">@lang('general.SendMobile')</p>
                                        <input class="form-control" id="email_address" type="text" name="email_address" placeholder="@lang('general.SubmitComment')">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="quform-submit-inner">
                                    <button class="butn-style3 white-hover m-0" type="submit">@lang('general.SubmitPhone')</button>
                                </div>
                                <div class="quform-loading-wrap"><span class="quform-loading"></span></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="bg-dark border-top border-color-light-white py-1-9 mt-7">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <p class="mb-0 text-white">&copy; {{$siteConfigs->title[app()->getLocale()]}}. @lang('general.Allrightsreserved')</p>
                </div>
                <div class="col-md-6 text-center">
                    <p class="mb-0 text-white">Designed By <a href="https://webYazilim.ir">Ahmad Jangi</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
