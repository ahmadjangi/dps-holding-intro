<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="{{ app()->getLocale() === 'fa' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Ahmad Jangi"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{'img/logos/apple-touch-icon-57x57.png'}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{'img/logos/apple-touch-icon-72x72.png'}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{'img/logos/apple-touch-icon-114x114.png'}}">
    <meta name="theme-color" content="#fff">
    <link href="{{asset('css/bootstrap/bootstrap.rtl.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/plugins.css?4001')}}">
    <link rel="stylesheet" href="{{asset('css/search.css')}}">
    <link rel="stylesheet" href="{{asset('css/base.css')}}">
    <link href="{{asset('css/styles-4.css?4001')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    @if(app()->getLocale() === 'fa')
        <link href="{{asset('css/rtl.css')}}" rel="stylesheet">
    @endif
    {{--    <link href="{{asset('css/app.css?14008')}}" rel="stylesheet">--}}
    @yield('styles')
    @if($siteConfigs->googleAnalyticsCode)
        <script async
                src="{{'https://www.googletagmanager.com/gtag/js?id=' . $siteConfigs->googleAnalyticsCode}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', '{{$siteConfigs->googleAnalyticsCode}}');
        </script>
    @endif
</head>
<body>
<div id="preloader"></div>
<div class="main-wrapper ">
    @include('layout.header')
    @yield('main-content')
    @include('layout.footer')
</div>
<a href="#!" class="scroll-to-top"><i class="fas fa-angle-up" aria-hidden="true"></i></a>

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/nav-menu.js')}}"></script>
<script src="{{asset('js/search.js')}}"></script>
<script src="{{asset('js/easy.responsive.tabs.js')}}"></script>
<script src="{{asset('js/owl.carousel.js')}}"></script>
<script src="{{asset('js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('js/waypoints.min.js')}}"></script>
<script src="{{asset('js/countdown.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('js/chart.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.thumbs.js')}}"></script>
<script src="{{asset('js/animated-headline.js')}}"></script>
<script src="{{asset('js/wow.js')}}"></script>
<script src="{{asset('js/tilt.js')}}"></script>
<script src="{{asset('js/clipboard.min.js')}}"></script>
<script src="{{asset('js/lightgallery-all.js')}}"></script>
<script src="{{asset('js/jquery.mousewheel.min.js')}}"></script>
<script src="{{asset('js/prism.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

@yield('scripts')
</body>
</html>
