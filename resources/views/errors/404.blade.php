@extends('layout.app')
@section('title','صفحه مورد نطر پیدا نشد')

@section('main-content')
    <main class="main">
        <div class="mt-5">
            <h2 class="text-center">صفحه‌ای که دنبال آن بودید پیدا نشد!</h2>
            <div class="text-center mt-5 mb-5">
                <a class="btn btn-outline-primary btn-outline-primary-t1 p-5 py-2" href="/">صفحه اصلی</a>
            </div>
            <div style="font-size: 12rem; font-weight: bold; text-align: center">
                404
            </div>
        </div>

    </main>
@endsection
