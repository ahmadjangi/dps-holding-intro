@if ($paginator->hasPages())
    <div class="col-12">
        <div class="pagination text-small text-uppercase text-extra-dark-gray">
            <ul>
                @if ($paginator->onFirstPage())
                    <li class="disabled">
                        <a href="#!" aria-label="@lang('pagination.previous')"><i
                                class="fas fa-long-arrow-alt-left me-1 d-none d-sm-inline-block"></i>@lang('pagination.previous')
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ $paginator->previousPageUrl() }}" aria-label="@lang('pagination.previous')"><i
                                class="fas fa-long-arrow-alt-left me-1 d-none d-sm-inline-block"></i>@lang('pagination.previous')
                        </a>
                    </li>
                @endif
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class=" active" aria-current="page"><a href="#">{{ $page }}</a></li>
                                @else
                                    <li><a href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </li>
                @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}">@lang('pagination.next') <i
                                class="fas fa-long-arrow-alt-right ms-1 d-none d-sm-inline-block"></i></a></li>
                @else
                    <li class="disabled"><a href="{{ $paginator->nextPageUrl() }}">@lang('pagination.next') <i
                                class="fas fa-long-arrow-alt-right ms-1 d-none d-sm-inline-block"></i></a></li>
                @endif
            </ul>
        </div>
    </div>
@endif
