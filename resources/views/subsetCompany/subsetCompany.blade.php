@extends('layout.app')
@section('title',$subsetCompany->langs[0]->title)
@section('description',$siteConfigs->metaKeyword[app()->getLocale()])
@section('keywords',$siteConfigs->metaDescription[app()->getLocale()])
@section('main-content')
    @include('component.header.pageHeader',['img'=>$subsetCompany->image_url,'title'=>$subsetCompany->langs[0]->title,'breadcrumbs'=>null])
    @include('component.home.about',['topMargin'=>15,'image'=>$subsetCompany->image_url,'slogan'=>$subsetCompany->langs[0]->slogan,'about'=>$subsetCompany->langs[0]->about])
    @includeWhen($services,'component.services',['services'=>$services,'grayBackground'=>true])
    @includeWhen($products,'component.home.products',['products'=>$products])
    @includeWhen(true,'component.home.address',[
        'title'=>$subsetCompany->langs[0]->title,
        'logoAlternative'=>env('APP_URL') . $subsetCompany->loo_url ?  $subsetCompany->logo_url : $siteConfigs->siteLogoUrlAlternative,
        'tel1'=>$subsetCompany->tel1,
        'tel2'=>$subsetCompany->tel2,
        'postalCode'=>$subsetCompany->postalCode,
        'address'=>$subsetCompany->langs[0]->address,
        'addressTwo'=>$siteConfigs->addressTwo[app()->getLocale()],
        'email'=>$subsetCompany->email,
        ])
@endsection

@section('scripts')

@endsection


