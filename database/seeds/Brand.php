<?php

use Illuminate\Database\Seeder;

class Brand extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Brand::class, 500)->create()->each(function($brand){
            $brand->save();
        });
    }
}
