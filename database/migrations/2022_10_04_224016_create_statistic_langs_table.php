<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistic_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('statistic_id');
            $table->string('lang_id',10);
            $table->string('title',255);
            $table->string('value',255);
            $table->timestamps();
        });

        Schema::table('statistic_langs', function (Blueprint $table) {
            $table->foreign('statistic_id')->references('id')->on('statistics')->onDelete('CASCADE')->onUpdate('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistic_langs');
    }
}
