<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('email')->unique()->nullable();
            $table->string('first_name',255)->nullable();
            $table->string('last_name',255)->nullable();
            $table->string('national_id',255)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('mobile',13)->unique();
            $table->string('card_number',16)->nullable();
            $table->string('job',255)->nullable();
            $table->date('birthday')->nullable();
            $table->string('password',255)->nullable();
            $table->boolean('published')->default(true);
            $table->boolean('completed_profile')->default(false);
            $table->rememberToken();
            $table->timestamps();

            $table->bigInteger('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('id')->on('roles');
        });

        DB::table('users')->insert([
            'email' => 'admin@admin.com',
            'first_name' => 'مدیر',
            'last_name' => 'مدیری',
            'national_id' => '0000000000',
            'mobile' => '09355925595',
            'password' => Hash::make('admin'),
            'published' => '1',
            'role_id' => '1',
        ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
