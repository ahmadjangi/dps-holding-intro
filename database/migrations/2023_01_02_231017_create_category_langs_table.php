<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_langs', function (Blueprint $table) {
//            $table->foreignId('category_id')->unsigned()->references('id')->on('categories')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('title',255);
            $table->mediumText('short_description');
            $table->longText('description');
            $table->string('meta_description',255);
            $table->string('meta_keyword',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_langs');
    }
}
