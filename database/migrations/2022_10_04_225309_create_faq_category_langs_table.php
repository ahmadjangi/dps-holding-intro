<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqCategoryLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_category_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('faq_category_id');
            $table->string('lang_id');
            $table->string('title',255);
        });

        Schema::table('faq_category_langs', function (Blueprint $table) {
            $table->foreign('faq_category_id')->references('id')->on('faq_categories')->onDelete('CASCADE')->onUpdate('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_category_langs');
    }
}
