<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('label',255)->nullable();
            $table->string('description',512)->nullable();
            $table->boolean('published')->default(true)->nullable();
            $table->timestamps();
        });

        DB::table('roles')->insert([
            'name' => 'admin',
            'label' => 'مدیر',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
