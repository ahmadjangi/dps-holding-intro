<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogPostLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('blog_post_id');
            $table->string('lang_id',10);
            $table->string('title',255);
            $table->text('description_short');
            $table->longText('description');
            $table->string('meta_keyword',255);
            $table->string('meta_description',255);
        });

        Schema::table('blog_post_langs', function (Blueprint $table) {
            $table->foreign('blog_post_id')->references('id')->on('blog_posts')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post_langs');
    }
}
