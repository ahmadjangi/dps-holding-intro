<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubsetCompaniesProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subset_companies_projects', function (Blueprint $table) {
            $table->unsignedBigInteger('subset_companies_id');
            $table->unsignedBigInteger('project_id');
        });

        Schema::table('subset_companies_projects', function (Blueprint $table) {
            $table->foreign('subset_companies_id')->references('id')->on('subset_companies')->onDelete('CASCADE')->onUpdate('CASCADE');;
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('CASCADE')->onUpdate('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subset_companies_projects');
    }
}
