<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id');
            $table->string('lang_id',10);
            $table->string('title',255);
            $table->longText('description');
            $table->text('short_description');
            $table->string('meta_description');
            $table->string('meta_keyword');
            $table->string('status');
            $table->string('partner_title');
        });

        Schema::table('project_langs', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_langs');
    }
}
