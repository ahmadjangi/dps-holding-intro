<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('banner_id');
            $table->string('lang_id',10);
            $table->string('title',255);
        });

        Schema::table('banner_langs', function (Blueprint $table) {
            $table->foreign('banner_id')->references('id')->on('banners')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_langs');
    }
}
