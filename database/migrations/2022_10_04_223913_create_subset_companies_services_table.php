<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubsetCompaniesServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subset_companies_services', function (Blueprint $table) {
            $table->unsignedBigInteger('subset_companies_id');
            $table->unsignedBigInteger('services_id');
        });

        Schema::table('subset_companies_services', function (Blueprint $table) {
            $table->foreign('subset_companies_id')->references('id')->on('subset_companies')->onDelete('CASCADE')->onUpdate('CASCADE');;
            $table->foreign('services_id')->references('id')->on('subset_companies')->onDelete('CASCADE')->onUpdate('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subset_companies_services');
    }
}
