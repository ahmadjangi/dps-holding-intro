<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHonorLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('honor_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('honor_id');
            $table->string('lang_id',10);
            $table->string('title',255);
        });

        Schema::table('honor_langs', function (Blueprint $table) {
            $table->foreign('honor_id')->references('id')->on('honors')->onDelete('CASCADE')->onUpdate('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('honor_langs');
    }
}
