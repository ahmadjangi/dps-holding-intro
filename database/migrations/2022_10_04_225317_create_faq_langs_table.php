<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('faq_id');
            $table->string('lang_id');
            $table->string('title',255);
            $table->text('description');
        });

        Schema::table('faq_langs', function (Blueprint $table) {
            $table->foreign('faq_id')->references('id')->on('faqs')->onDelete('CASCADE')->onUpdate('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_langs');
    }
}
