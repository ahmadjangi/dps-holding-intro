<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_category_blog_post', function (Blueprint $table) {
            $table->unsignedBigInteger('blog_category_id')->index();
            $table->unsignedBigInteger('blog_post_id')->index();
        });

        Schema::table('blog_category_blog_post', function (Blueprint $table) {
            $table->foreign('blog_category_id')->references('id')->on('blog_categories');
            $table->foreign('blog_post_id')->references('id')->on('blog_posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_posts');
        Schema::dropIfExists('category_posts');

    }
}
