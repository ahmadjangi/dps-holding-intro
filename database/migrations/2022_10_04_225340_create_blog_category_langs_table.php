<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogCategoryLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_category_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('blog_category_id');
            $table->string('lang_id',10);
            $table->string('title',255);
            $table->text('description');
        });

        Schema::table('blog_category_langs', function (Blueprint $table) {
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onDelete('CASCADE')->onUpdate('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_category_langs');
    }
}
