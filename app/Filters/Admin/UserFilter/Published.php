<?php
/**
 * Created by PhpStorm.
 * User: Ahmad
 * Date: 29/07/2019
 * Time: 01:45 PM
 */

namespace App\Filters\Admin\UserFilter;



use Illuminate\Database\Eloquent\Builder;

class Published implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('published', '=',  $value );
    }
}
