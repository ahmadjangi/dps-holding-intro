<?php

namespace App\Filters\Admin;

use App\Models\Honor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class HonorFilter
{

    public static function filter(Request $filters)
    {

        $query = static::applyDecoratorsFromRequest($filters, (new Honor)->newQuery());

        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }

        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\HonorFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {
        return $query->orderBy($filters->sortBy ? $filters->sortBy:'created_at',$filters->sortAsc ? $filters->sortAsc:'desc')
            ->with('langs',function ($query){$query->where('lang_id','fa');})->paginate($filters->recordCount,
                ['id','img_url','published']);
    }
}
