<?php

namespace App\Filters\Admin;

use App\Models\BlogCategory;
use App\Models\Faq;
use App\Models\FaqCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use DB;

class FaqCategoryFilter
{

    public static function filter(Request $filters)
    {
        $query = static::applyDecoratorsFromRequest($filters, (new FaqCategory)->newQuery());
        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query): Builder
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name): string
    {
        return __NAMESPACE__ . '\\FaqFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator): bool
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {
        $records = [];
        if($filters->recordCount == -1){
            $result = $query->with(['langs'=>function($query)use($filters){
               $query->orderBy($filters->sortBy ? $filters->sortBy:'title',$filters->sortAsc ? $filters->sortAsc:'desc');
            }])->get(['id','published'])->toArray();

            foreach ($result as $item){
                $records[]= getMultiLanguageRecords($item,['title']);
            }

            return $records;
        }else{
            return $query->orderBy($filters->sortBy ? $filters->sortBy:'id',$filters->sortAsc ? $filters->sortAsc:'desc')
                ->with('langs',function ($query){$query->where('lang_id','fa');})->paginate($filters->recordCount,
                    ['id','published']);
        }
    }
}
