<?php

namespace App\Filters\Admin;

use App\Models\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class RoleFilter
{

    public static function filter(Request $filters)
    {

        $query = static::applyDecoratorsFromRequest($filters, (new Role)->newQuery());
        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\RoleFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {

        if ($filters->recordCount == -1){

            return $query->orderBy($filters->sortBy ? $filters->sortBy:'label',$filters->sortAsc ? $filters->sortAsc:'asc')
                ->get(['id','label','published']);
        }else{

            return $query->orderBy($filters->sortBy ? $filters->sortBy:'label',$filters->sortAsc ? $filters->sortAsc:'asc')
                ->paginate($filters->recordCount,
                    ['id','name','label','description','published']);
        }
    }
}
