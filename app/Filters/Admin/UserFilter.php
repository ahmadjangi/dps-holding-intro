<?php

namespace App\Filters\Admin;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use DB;

class UserFilter
{

    public static function filter(Request $filters)
    {

        $query = static::applyDecoratorsFromRequest($filters, (new User)->newQuery());
        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\UserFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {

        if($filters->recordCount == -1) {
            return $query->orderBy($filters->sortBy ? $filters->sortBy:'created_at',$filters->sortAsc ? $filters->sortAsc:'asc')
                ->get(['id',DB::raw('CONCAT(first_name," ",last_name) As full_name'),'mobile','published']);

        }else{
            return $query->orderBy($filters->sortBy ? $filters->sortBy:'created_at',$filters->sortAsc ? $filters->sortAsc:'asc')
                ->paginate($filters->recordCount,
                    ['id','email',DB::raw('CONCAT(first_name," ",last_name) As full_name'),'national_id','email_verified_at','mobile','card_number','created_at','published']);
        }


    }
}
