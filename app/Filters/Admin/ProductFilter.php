<?php

namespace App\Filters\Admin;


use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ProductFilter
{

    public static function filter(Request $filters)
    {

        $query = static::applyDecoratorsFromRequest($filters, (new Product)->newQuery());
        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\ProductFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {
        if ($filters->recordCount == -1){
            return $query->orderBy($filters->sortBy ? $filters->sortBy:'created_at',$filters->sortAsc ? $filters->sortAsc:'desc')
                ->with('langs',function ($query){$query->where('lang_id','fa');})->get(
                    [
                        'id',
                        'published'
                    ]);
        }else{
            return $query->orderBy($filters->sortBy ? $filters->sortBy:'created_at',$filters->sortAsc ? $filters->sortAsc:'desc')
                ->with(['langs'=>function ($query){$query->where('lang_id','fa');},'images'])->paginate($filters->recordCount,
                    ['id','published']);

        }
    }
}
