<?php

namespace App\Filters\Admin;


use App\Models\City;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CityFilter
{

    public static function filter(Request $filters)
    {

        $query = static::applyDecoratorsFromRequest($filters, (new City)->newQuery());
        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\CityFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {

        return $query->orderBy($filters->sortBy ? $filters->sortBy:'name',$filters->sortAsc ? $filters->sortAsc:'asc')
                     ->withOnly('state')->paginate($filters->recordCount,
                         ['id','name','state_id','published']);
    }
}
