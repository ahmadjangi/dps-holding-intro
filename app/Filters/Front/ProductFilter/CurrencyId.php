<?php
namespace App\Filters\Front\ProductFilter;

use Illuminate\Database\Eloquent\Builder;

class CurrencyId implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {

        if($value){
            return $builder->with('prices', function($query) use ($value) {
                    $query->where('currency_id',$value);
                });
        }else{
            return $builder;
        }
    }
}
