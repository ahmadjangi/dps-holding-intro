<?php
namespace App\Filters\Front\ProductFilter;

use http\Client\Curl\User;
use Illuminate\Database\Eloquent\Builder;

class PublishStartDate implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $value ? $builder->where('publish_start_date','<=',$value) : $builder;
    }
}
