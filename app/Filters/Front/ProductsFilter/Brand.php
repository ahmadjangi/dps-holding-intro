<?php
namespace App\Filters\Front\ProductsFilter;

use Illuminate\Database\Eloquent\Builder;

class Brand implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
            return $value ? $builder->whereIn('brand_id',$value) : $builder;

    }
}
