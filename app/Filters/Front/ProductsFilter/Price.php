<?php
namespace App\Filters\Front\ProductsFilter;

use Illuminate\Database\Eloquent\Builder;

class Price implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        if($value){

            return $builder->whereHas('prices', function($query) use ($value) {

                    $query->whereBetween('price',[$value['min'],$value['max']]);
                });
        }else{
            return $builder;
        }
    }
}
