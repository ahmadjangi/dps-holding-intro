<?php
namespace App\Filters\Front\ProductsFilter;

use Illuminate\Database\Eloquent\Builder;

class IsStock implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $value == 1 ? $builder->where('quantity','>=',1) :$builder;
    }
}
