<?php
namespace App\Filters\Front\ProductsFilter;

use Illuminate\Database\Eloquent\Builder;

class Property implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        if($value){

            return $builder->whereHas('properties.propertyValue', function($query) use ($value) {

                    $query->whereIn('property_value_id',$value);
                });
        }else{
            return $builder;
        }
    }
}
