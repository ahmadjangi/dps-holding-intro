<?php

namespace App\Filters\Front;

use App\Classes\PriceFilter;
use App\Http\Requests\ProductPrice\ProductPrices;
use App\Models\PriceProduct;
use App\Models\Product;
use App\Models\ProductProperty;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ProductsFilter
{

    public static function filter($filters)
    {
        $query = static::applyDecoratorsFromRequest($filters, (new Product)->newQuery());
        return static::getResults($query, $filters);
    }

    private static function applyDecoratorsFromRequest($request, Builder $query)
    {
        foreach ($request as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\ProductsFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query, $filters)
    {
        $today = Carbon::now()->format('Y-m-d h:m:s');
        $user = Auth::user();
        $configs = getSiteConfigs();
        $priceMode = $configs->priceMode;
        $result = null;

        if ($filters->sortBy) {
            if ($filters->sortBy === 'price') {
                $sortBy = 'name';
            } else {
                $sortBy = $filters->sortBy;
            }
        } else {
            $sortBy = 'sale_rate';
        }

        if (property_exists($filters, 'count')) {
            $result = $query
                ->orderBy($sortBy, $filters->sortAsc ? $filters->sortAsc : 'desc')
                ->where('published', 1)
                ->take($filters->count)->get(['id', 'name', 'rate', 'sale_rate', 'slug', 'quantity']);

        } else {
            $result = $query
                ->orderBy($sortBy, $filters->sortAsc ? $filters->sortAsc : 'desc')
                ->where('published', 1)->get(['id', 'name', 'rate', 'sale_rate', 'slug', 'quantity']);
        }

        $products = $result->map(function ($item) use ($user, $today, $priceMode) {

            $product = $item->with([
                'prices' => function ($query) use ($user, $item, $today) {
                    if ($user) {
                        $query->where(function ($query) use ($today) {
                            return $query->where('start_date', '<', $today)
                                ->where('end_date', '>', $today)
                                ->orWhere('start_date', '=', null)
                                ->orWhere('end_date', '=', null);
                        })->whereNull('product_property_id')
                            ->where(function ($query) use ($user, $item) {

                                $userPrice = PriceProduct::where('product_id', $item->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                    $userPriceQuery->where('price_product_user_id', $user->id);
                                });

                                $rolePrice = PriceProduct::where('product_id', $item->id)->whereHas('roles', function ($query) use ($user) {
                                    $query->where('price_product_role_id', $user->role_id);
                                });

                                if ($userPrice->count() > 0) {

                                    return $query->where('product_id', $item->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                        $userPriceQuery->where('price_product_user_id', $user->id);
                                    });
                                } elseif ($rolePrice->count() > 0 && $userPrice->count() === 0) {

                                    return $query->where('product_id', $item->id)->whereHas('roles', function ($query) use ($user) {
                                        $query->where('price_product_role_id', $user->role_id);
                                    });
                                } else {
                                    $query->doesntHave('roles')->doesntHave('users');
                                }
                            });
                    } else {
                        $query->where(function ($query) use ($today) {
                            return $query->where('start_date', '<', $today)
                                ->where('end_date', '>', $today)
                                ->orWhere('start_date', '=', null)
                                ->orWhere('end_date', '=', null);
                        })->whereNull('product_property_id')
                            ->doesntHave('roles')
                            ->doesntHave('users');
                    }
                },
                'discounts' => function ($query) use ($today, $user) {
                    return $query->where('published', 1)
                        ->where('type', 'discount')
                        ->where(function ($query) use ($today) {
                            $query
                                ->where('start_date', '<=', $today)
                                ->where('end_date', '>=', $today)
                                ->orWhere('start_date', null)
                                ->orWhere('end_date', null);
                        })
                        ->where(function ($query) use ($user) {
                            if ($user) {
                                $query->whereHas('roles', function ($query) use ($user) {
                                    $query->where('role_id', $user->role_id);
                                })->orDoesntHave('roles');
                            } else {
                                $query->doesntHave('roles');
                            }
                        })
                        ->where(function ($query) use ($user) {
                            if ($user) {
                                $query->whereHas('users', function ($query) use ($user) {
                                    $query->where('user_id', $user->role_id);
                                })->orDoesntHave('users');
                            } else {
                                $query->doesntHave('users');
                            }
                        });
                },
                'properties' => function ($query) {
                    return $query->where('default', 1)->where('published', 1);
                },
                'properties.prices' => function ($query) use ($user, $item, $today) {
                    $productProperty = ProductProperty::where('product_id', $item->id)->where('default', 1)->first('id');

                    if ($user) {
                        $query->where(function ($query) use ($today) {
                            return $query->where('start_date', '<', $today)
                                ->where('end_date', '>', $today)
                                ->orWhere('start_date', '=', null)
                                ->orWhere('end_date', '=', null);
                        })->whereNotNull('product_property_id')
                            ->where(function ($query) use ($user, $productProperty) {

                                $userPrice = PriceProduct::where('product_property_id', $productProperty->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                    $userPriceQuery->where('price_product_user_id', $user->id);
                                });


                                $rolePrice = PriceProduct::where('product_property_id', $productProperty->id)->whereHas('roles', function ($query) use ($user) {
                                    $query->where('price_product_role_id', $user->role_id);
                                });

                                if ($userPrice->count() > 0) {

                                    return $query->where('product_property_id', $productProperty->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                        $userPriceQuery->where('price_product_user_id', $user->id);
                                    });
                                } elseif ($rolePrice->count() > 0 && $userPrice->count() === 0) {

                                    return $query->where('product_property_id', $productProperty->id)->whereHas('roles', function ($query) use ($user) {
                                        $query->where('price_product_role_id', $user->role_id);
                                    });
                                } else {
                                    $query->doesntHave('roles')->doesntHave('users');
                                }
                            });
                    } else {

                        $query->where(function ($query) use ($today) {
                            return $query->where('start_date', '<', $today)
                                ->where('end_date', '>', $today)
                                ->orWhere('start_date', '=', null)
                                ->orWhere('end_date', '=', null);
                        })->whereNotNull('product_property_id')
                            ->doesntHave('roles')
                            ->doesntHave('users')->get()->toArray();
                    }
                },
                'properties.discounts' => function ($query) use ($today, $user) {
                    return $query->where('published', 1)
                        ->where('type', 'discount')
                        ->where(function ($query) use ($today) {
                            $query
                                ->where('start_date', '<=', $today)
                                ->where('end_date', '>=', $today)
                                ->orWhere('start_date', null)
                                ->orWhere('end_date', null);
                        })
                        ->where(function ($query) use ($user) {
                            if ($user) {
                                $query->whereHas('roles', function ($query) use ($user) {
                                    $query->where('role_id', $user->role_id);
                                })->orDoesntHave('roles');
                            } else {
                                $query->doesntHave('roles');
                            }
                        })
                        ->where(function ($query) use ($user) {
                            if ($user) {
                                $query->whereHas('users', function ($query) use ($user) {
                                    $query->where('user_id', $user->role_id);
                                })->orDoesntHave('users');
                            } else {
                                $query->doesntHave('users');
                            }
                        });
                },
                'categories' => function ($query) {
                    return $query->where('main_category', 1);
                },
                'categories.discounts' => function ($query) use ($today, $user) {
                    return $query
                        ->where('published', 1)
                        ->where('for', 'product')
                        ->where('type', 'discount')
                        ->where(function ($query) use ($today) {
                            $query
                                ->where('start_date', '<=', $today)
                                ->where('end_date', '>=', $today)
                                ->orWhere('start_date', null)
                                ->orWhere('end_date', null);
                        })
                        ->where(function ($query) use ($user) {
                            if ($user) {
                                $query->whereHas('roles', function ($query) use ($user) {
                                    if ($user) {
                                        $query->where('role_id', $user->role_id);
                                    }
                                })->orDoesntHave('roles');
                            } else {
                                $query->doesntHave('roles');
                            }
                        })
                        ->where(function ($query) use ($user) {
                            if ($user) {
                                $query->whereHas('users', function ($query) use ($user) {
                                    $query->where('user_id', $user->role_id);
                                })->orDoesntHave('users');
                            } else {
                                $query->doesntHave('users');
                            }
                        });
                },
                'images' => function ($query) {
                    return $query->where('is_main', 1);
                },
                'badges',
                'comments'
            ])->where('id', $item->id)->first()->toArray();

            $product = PriceFilter::applyCategoryDiscount($product, $priceMode);

            return $product;
        });

        return $products->toArray();
    }
}
