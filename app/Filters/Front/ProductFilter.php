<?php

namespace App\Filters\Front;

use App\Models\PriceProduct;
use App\Models\Product;
use App\Models\ProductProperty;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;


class ProductFilter
{

    public static function filter($filters)
    {
        $query = static::applyDecoratorsFromRequest($filters, (new Product)->newQuery());
        return static::getResults($query, $filters);
    }

    private static function applyDecoratorsFromRequest($request, Builder $query)
    {
        foreach ($request as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\ProductFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query, $filters)
    {

        $today = Carbon::now()->format('Y-m-d h:m:s');
        $user = Auth::user();
        $product = Product::where('slug', $filters->Slug)->first('id');

        return $query->with([
            'prices' => function ($query) use ($today, $user, $product) {

                if ($user) {
                    $query->where(function ($query) use ($today) {
                        return $query->where('start_date', '<', $today)
                            ->where('end_date', '>', $today)
                            ->orWhere('start_date', '=', null)
                            ->orWhere('end_date', '=', null);
                    })->whereNull('product_property_id')
                        ->where(function ($query) use ($user, $product) {

                            $userPrice = PriceProduct::where('product_id', $product->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                $userPriceQuery->where('price_product_user_id', $user->id);
                            });

                            $rolePrice = PriceProduct::where('product_id', $product->id)->whereHas('roles', function ($query) use ($user) {
                                $query->where('price_product_role_id', $user->role_id);
                            });

                            if ($userPrice->count() > 0) {

                                return $query->where('product_id', $product->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                    $userPriceQuery->where('price_product_user_id', $user->id);
                                });
                            } elseif ($rolePrice->count() > 0 && $userPrice->count() === 0) {

                                return $query->where('product_id', $product->id)->whereHas('roles', function ($query) use ($user) {
                                    $query->where('price_product_role_id', $user->role_id);
                                });
                            } else {
                                $query->doesntHave('roles')->doesntHave('users');
                            }
                        });
                } else {
                    $query->where(function ($query) use ($today) {
                        return $query->where('start_date', '<', $today)
                            ->where('end_date', '>', $today)
                            ->orWhere('start_date', '=', null)
                            ->orWhere('end_date', '=', null);
                    })->whereNull('product_property_id')
                        ->doesntHave('roles')
                        ->doesntHave('users');
                }
            },
            'prices.users',
            'prices.roles',
            'discounts' => function ($query) use ($today, $user) {
                return $query->where('published', 1)
                    ->where('type', 'discount')
                    ->where(function ($query) use ($today) {
                        $query
                            ->where('start_date', '<=', $today)
                            ->where('end_date', '>=', $today)
                            ->orWhere('start_date', null)
                            ->orWhere('end_date', null);
                    })
                    ->where(function ($query) use ($user) {
                        if ($user) {
                            $query->whereHas('roles', function ($query) use ($user) {
                                $query->where('role_id', $user->role_id);
                            })->orDoesntHave('roles');
                        } else {
                            $query->doesntHave('roles');
                        }
                    })
                    ->where(function ($query) use ($user) {
                        if ($user) {
                            $query->whereHas('users', function ($query) use ($user) {
                                $query->where('user_id', $user->role_id);
                            })->orDoesntHave('users');
                        } else {
                            $query->doesntHave('users');
                        }
                    });
            },
            'discounts.users',
            'discounts.roles',
            'categories' => function ($query) {
                return $query->where('main_category', 1);
            },
            'categories.discounts' => function ($query) use ($today, $user) {
                return $query
                    ->where('published', 1)
                    ->where('for', 'product')
                    ->where('type', 'discount')
                    ->where(function ($query) use ($today) {
                        $query
                            ->where('start_date', '<=', $today)
                            ->where('end_date', '>=', $today)
                            ->orWhere('start_date', null)
                            ->orWhere('end_date', null);
                    })
                    ->where(function ($query) use ($user) {
                        if ($user) {
                            $query->whereHas('roles', function ($query) use ($user) {
                                if ($user) {
                                    $query->where('role_id', $user->role_id);
                                }
                            })->orDoesntHave('roles');

                        } else {
                            $query->doesntHave('roles');
                        }

                    })
                    ->where(function ($query) use ($user) {
                        if ($user) {
                            $query->whereHas('users', function ($query) use ($user) {
                                $query->where('user_id', $user->role_id);
                            })->orDoesntHave('users');
                        } else {
                            $query->doesntHave('users');
                        }
                    });
            },
            'categories.discounts.users',
            'categories.discounts.roles',
            'comments' => function ($query) {
                return $query->where('confirmed', 1);
            },
            'properties.discounts' => function ($query) use ($today, $user) {
                return $query->where('published', 1)
                    ->where('type', 'discount')
                    ->where(function ($query) use ($today) {
                        $query
                            ->where('start_date', '<=', $today)
                            ->where('end_date', '>=', $today)
                            ->orWhere('start_date', null)
                            ->orWhere('end_date', null);
                    })
                    ->where(function ($query) use ($user) {
                        if ($user) {
                            $query->whereHas('roles', function ($query) use ($user) {
                                $query->where('role_id', $user->role_id);
                            })->orDoesntHave('roles');
                        } else {
                            $query->doesntHave('roles');
                        }
                    })
                    ->where(function ($query) use ($user) {
                        if ($user) {
                            $query->whereHas('users', function ($query) use ($user) {
                                $query->where('user_id', $user->role_id);
                            })->orDoesntHave('users');
                        } else {
                            $query->doesntHave('users');
                        }
                    });
            },
            'properties.discounts.users',
            'properties.discounts.roles',
            'properties.prices' => function ($query) use ($today, $user, $product) {
                $productProperty = ProductProperty::where('product_id', $product->id)->where('default', 1)->first('id');

                if ($user) {
                    $query->where(function ($query) use ($today) {
                        return $query->where('start_date', '<', $today)
                            ->where('end_date', '>', $today)
                            ->orWhere('start_date', '=', null)
                            ->orWhere('end_date', '=', null);
                    })->whereNotNull('product_property_id')
                        ->where(function ($query) use ($user, $productProperty) {

                            $userPrice = PriceProduct::where('product_property_id', $productProperty->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                $userPriceQuery->where('price_product_user_id', $user->id);
                            });

                            $rolePrice = PriceProduct::where('product_property_id', $productProperty->id)->whereHas('roles', function ($query) use ($user) {
                                $query->where('price_product_role_id', $user->role_id);
                            });

                            if ($userPrice->count() > 0) {

                                return $query->where('product_property_id', $productProperty->id)->whereHas('users', function ($userPriceQuery) use ($user) {
                                    $userPriceQuery->where('price_product_user_id', $user->id);
                                });
                            } elseif ($rolePrice->count() > 0 && $userPrice->count() === 0) {

                                return $query->where('product_property_id', $productProperty->id)->whereHas('roles', function ($query) use ($user) {
                                    $query->where('price_product_role_id', $user->role_id);
                                });
                            } else {
                                $query->doesntHave('roles')->doesntHave('users');
                            }
                        });
                } else {

                    $query->where(function ($query) use ($today) {
                        return $query->where('start_date', '<', $today)
                            ->where('end_date', '>', $today)
                            ->orWhere('start_date', '=', null)
                            ->orWhere('end_date', '=', null);
                    })->whereNotNull('product_property_id')
                        ->doesntHave('roles')
                        ->doesntHave('users')->get()->toArray();
                }
            },
            'properties.prices.users',
            'properties.prices.roles',
            'properties.propertyValue.property',
            'featureValue.feature.featureGroup',
            'images',
            'relatedProducts',
            'brand'])
            ->where('published', 1)
            ->firstOrFail();

    }
}
