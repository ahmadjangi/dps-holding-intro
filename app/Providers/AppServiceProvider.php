<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $siteConfig = getSiteConfigs();
        View::share('siteConfigs', $siteConfig);

//        $categories = Category::where('published',1)->where('name','<>','Root')->orderBy('arrangement')->get()->toArray();
//        View::share('navTreeCategories', buildTree($categories,1));

        Blade::directive('productLargImageUrl', function ($url) {
            return "<?php echo env('APP_URL') .'/storage/productImage/' . $url ?>";
        });

        Blade::directive('productMediumImageUrl', function ($url) {
            return "<?php echo env('APP_URL') . '/storage/productImage/thumbnail/600/' . $url ?>";
        });

        Blade::directive('productSmallImageUrl', function ($url) {
            return "<?php echo env('APP_URL') . '/storage/productImage/thumbnail/300/' . $url ?>";
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
    }
}
