<?php


namespace App\Classes;


use Illuminate\Support\Facades\Http;

class SendSMS
{

    protected static string $url = 'https://api.kavenegar.com/v1/636757695A443055426C7A4F386E554F696E4E576D696372736356596C76544866775461736A4235586E513D/verify/lookup.json';

    public static function sendVerifySMS($message,$number){

        $response = Http::get(static::$url, [
            'receptor' => $number,
            'token' => $message,
            'template' => 'verify',
            'type' => 'sms',
        ]);
        $result = $response->json();

        if($response->successful() && $result['entries'][0]['status'] === 10)
            return true;

        return false;
    }

    public static function sendContact($token1,$token2,$token3,$number,$template){

        $response = Http::get(static::$url, [
            'receptor' => $number,
            'token' => preg_replace("/\s+/",'', $token1),
            'token2' => preg_replace("/\s+/",'', $token2),
            'token3' => preg_replace("/\s+/",'', $token3),
            'template' => $template,
            'type' => 'sms',
        ]);

        $result = $response->json();
        if($response->successful() && $result['entries'][0]['status'] === 10)
            return true;

        return false;
    }
}
