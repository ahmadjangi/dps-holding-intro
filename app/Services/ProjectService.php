<?php

namespace App\Services;

use App\Filters\Admin\ProjectFilter;
use App\Http\Requests\Project\Projects;
use App\Http\Requests\Project\PublishProject;
use App\Http\Requests\Project\StoreProject;
use App\Http\Requests\Project\UpdateProject;
use App\Models\Project;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProjectService
{
    public static function index(Projects $request): LengthAwarePaginator
    {

        return ProjectFilter::filter($request);
    }

    public static function store(StoreProject $request){

        try{
            $project = new Project();
            $project->slug = $request->slug;
            $project->date = $request->date;
            $project->published = $request->published;
            $project->save();
            /* [title,description,short_description,meta_keyword,meta_description,status,partner_title] */
            $project->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $project->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($project): array
    {
        $columns = ['title','description','short_description','meta_keyword','meta_description','status','partner_title'];
        return getMultiLanguageRecords(Project::whereId($project)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdateProject $request, $project){

        try{
            $project = Project::find($project);
            $project->slug = $request->slug;
            $project->date = $request->date;
            $project->published = $request->published;
            $project->save();
            /* [title,description,short_description,meta_keyword,meta_description,status,partner_title] */
            $project->langs()->delete();
            $project->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $project->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($projects): int
    {

        $projects = explode(',',$projects);
        $count = 0;
        foreach ($projects as $project){
            Project::destroy($project);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishProject $request){
        $project = Project::find($request->id);
        $project->published = $request->published;
        $project->save();
    }

}
