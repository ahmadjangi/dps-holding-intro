<?php

namespace App\Services;

use App\Http\Requests\Config\Configs;
use App\Http\Requests\Config\StoreConfig;
use App\Http\Requests\Config\UpdateConfig;
use App\Models\SiteConfig;

class ConfigService
{
    public static function index(){
        return getSiteConfigs();
    }

    public static function store(StoreConfig $request){
        foreach ($request->all() as $key => $value){
            if (is_array($value) && key_exists('fa',$value)){
                foreach ($value as $langIdKey => $langIdValue){
                    $config = SiteConfig::firstOrNew(
                        ['name' => (string)$key,'lang_id'=>(string)$langIdKey]
                    );
                    $config->lang_id = $langIdKey;
                    $config->value = $value[$langIdKey];
                    $config->save();
                }
            }else{

                $config = SiteConfig::firstOrNew(
                    ['name' => (string)$key]
                );
                $config->value = $value;
                $config->save();
            }
        }
    }

    public static function update(UpdateConfig $request){

        foreach ($request->all() as $key => $value){
            SiteConfig::where('name',(string)$key)->update(['value' => $value]);
        }
    }

}
