<?php

namespace App\Services;

use App\Filters\Admin\PageFilter;
use App\Http\Requests\Page\Pages;
use App\Http\Requests\Page\PublishPage;
use App\Http\Requests\Page\StorePage;
use App\Http\Requests\Page\UpdatePage;
use App\Models\Banner;
use App\Models\Page;


class PageService
{
    public static function index(Pages $request){

        return PageFilter::filter($request);
    }

    public static function store(StorePage $request){
        try{
            $page = new Page();
            $page->slug = $request->slug;
            $page->published = $request->published;

            $page->save();
            /* [title,content,meta_keyword,meta_description] */
            $page->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $page->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($page){

        $columns = ['title','content','meta_keyword','meta_description'];
        return getMultiLanguageRecords(Page::whereId($page)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdatePage $request, $page){
        try{
            $page = Page::find($page);
            $page->slug = $request->slug;
            $page->published = $request->published;

            $page->save();
            /*[title,content,meta_keyword,meta_description]*/
            $page->langs()->delete();
            $page->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $page->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($banners){

        $banners = explode(',',$banners);
        $count = 0;
        foreach ($banners as $banner){
            Banner::destroy($banner);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishPage $request){
        $banner = Banner::find($request->id);
        $banner->published = $request->published;
        $banner->save();
    }
}
