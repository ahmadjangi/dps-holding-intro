<?php

namespace App\Services;


use App\Filters\Admin\BlogCategoryFilter;
use App\Http\Requests\BlogCategory\Categories;
use App\Http\Requests\BlogCategory\PublishCategory;
use App\Http\Requests\BlogCategory\StoreCategory;
use App\Http\Requests\BlogCategory\UpdateCategory;
use App\Models\BlogCategory;
use Illuminate\Support\Str;

class BlogCategoryService
{
    public static function index(Categories $request){

        return BlogCategoryFilter::filter($request);
    }

    public static function store(StoreCategory $request){

        try{
            $category = new BlogCategory();
            $category->parent_id = 1;
            $category->published = $request->published;
            $category->save();

            /* title,description */
            $category->langs()->createMany(SetMultiLanguageRecords($request->langs));
            return $category->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($category): array
    {
        return getMultiLanguageRecords(
            BlogCategory::whereId($category)->with('langs')->first()->toArray(),
            ['title','description']
        );
    }

    public static function update(UpdateCategory $request, $category){
        try{
            $category = BlogCategory::find($category);
            $category->parent_id = 1;
            $category->slug = $request->slug;
            $category->published = $request->published;
            $category->save();

            /* title,description */
            $category->langs()->delete();
            $category->langs()->createMany(SetMultiLanguageRecords($request->langs));
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($categories){

        $categories = explode(',',$categories);

        foreach ($categories as $category){

            $exist = BlogCategory::find($category);
            $product = $exist->posts()->first();
            if($product === null){
                BlogCategory::destroy($category);
                return true;
            }else{
                return false;
            }
        }
    }

    public static function publish(PublishCategory $request){

        $brand = BlogCategory::find($request->id);
        $brand->published = $request->published;
        $brand->save();
    }

}
