<?php

namespace App\Services;

use App\Filters\Admin\ProductFilter;
use App\Http\Requests\Product\ArrangeProduct;
use App\Http\Requests\Product\Products;
use App\Http\Requests\Product\PublishProduct;
use App\Http\Requests\Product\StorePrice;
use App\Http\Requests\Product\StoreProduct;
use App\Http\Requests\Product\UpdateProduct;
use App\Http\Requests\Product\UpdateProductQuntity;
use App\Models\Badge;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Category;
use App\Models\Feature;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\PriceProduct;
use App\Models\ImageProduct;
use App\Models\ProductProperty;
use App\Models\RelatedProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductService
{
    public static function index(Products $request)
    {
        return ProductFilter::filter($request);
    }

    public static function store(StoreProduct $request)
    {
        DB::beginTransaction();
        try{

            $lastArrangement = Product::max('arrangement');
            // Save new product
            $product = new Product;
            $product->category_id = $request->categories;
            $product->slug = $request->slug;
            $product->published = $request->published;
            $product->special = $request->special;
            $product->arrangement = $lastArrangement + 1 ;

            $product->save();

            /* [title,description,short_description,meta_keyword,meta_description,status,partner_title] */
            $product->langs()->createMany(SetMultiLanguageRecords($request->langs));

            // Insert Images
            if(count($request->images) > 0) {
                foreach ($request->images as $image) {
                    $imageProduct = new ImageProduct([
                        "url" => $image['url'],
                        "is_main" => $image['is_main'],
                    ]);

                    $product->images()->save($imageProduct);
                }
            }

            DB::commit();

            return array('status' => true,'data'=>$product->id);
        }catch (\Exception $e){

            DB::rollBack();

            return array('status'=>false,'data'=>$e->getMessage());
        }
    }

    public static function show($product)
    {
        $columns = ['title','description','short_description','meta_keyword','meta_description'];
        return getMultiLanguageRecords(Product::whereId($product)->with('langs','images')->first()->toArray(),$columns);

    }

    public static function update(UpdateProduct $request, $product)
    {
        DB::beginTransaction();
        try{

            $product = Product::find($product);
            $product->category_id = $request->categories;
            $product->slug = $request->slug;
            $product->published = $request->published;
            $product->special = $request->special;

            $product->save();

            /* [title,description,short_description,meta_keyword,meta_description,status,partner_title] */
            $product->langs()->delete();
            $product->langs()->createMany(SetMultiLanguageRecords($request->langs));


            DB::commit();

            return array('status' => true,'data'=>$product->id);
        }catch (\Exception $e){

            DB::rollBack();

            return array('status'=>false,'data'=>$e->getMessage());
        }
    }

    public static function delete($products)
    {
        $products = explode(',', $products);
        $count = 0;
        foreach ($products as $item) {
            $item->images ()->delete();
            $item->delete();
            $count++;
        }
        return $count;
    }

    public static function publish(PublishProduct $request)
    {
        $product = Product::find($request->id);

        $product->published = $request->published;

        $product->save();
    }

    public static function deleteImage($imageId)
    {
        DB::beginTransaction();
        try {

            $productImage = ImageProduct::find($imageId);

            $imageOriginalPath = 'productImage/' . $productImage->url;
            $image600Path =  'productImage/thumbnail/600/' . $productImage->url;
            $image300Path =  'productImage/thumbnail/300/' . $productImage->url;

            Storage::disk('public')->delete($imageOriginalPath);
            Storage::disk('public')->delete($image600Path);
            Storage::disk('public')->delete($image300Path);

            $productImage->delete();
            DB::commit();
            return array('status' => true,'data'=>null);

        }catch (\Exception $e){

            DB::rollBack();
            return array('status' => false,'data'=>null);
        }
    }

    public static function deleteFeature($productId,$featureId)
    {
        DB::beginTransaction();
        try{
            $product = Product::findOrFail($productId);
            $product->features()->detach($featureId);

            DB::commit();
            return true;
        }catch (\Exception $e){

            DB::rollBack();
            return false;
        }

    }

    public static function arrange(ArrangeProduct $request)
    {
        foreach ($request->arrangeList as $item){
            Product::whereId($item['id'])->update(['arrangement'=>$item['position']]);
        }
    }

    public static function duplicate($id)
    {
        DB::beginTransaction();
        try {
            $lastArrangement = Product::max('arrangement');

            $product = Product::find($id);

            $cloneProduct = $product->replicate();
            $cloneProduct->name = $product->name . ' کپی';
            $cloneProduct->sku = $product->sku .'-'. Carbon::now();
            $cloneProduct->slug = $product->slug .'-'. Carbon::now();
            $cloneProduct->hits = 1;
            $cloneProduct->published = 0;
            $cloneProduct->arrangement = $lastArrangement +1;
            $cloneProduct->created_at = Carbon::now();
            $cloneProduct->updated_at = Carbon::now();
            $cloneProduct->save();

            $productPrices = $product->prices->where('product_property_id',null);
            if ($productPrices) {
                foreach ($productPrices as $price){
                    $clonedPrice = $price->replicate();
                    $clonedPrice->product_id = $cloneProduct->id;
                    $clonedPrice->save();

                    $clonedPrice->roles()->attach($price->roles);
                    $clonedPrice->users()->attach($price->users);
                }
            }

            if ($product->images) {
                foreach ($product->images as $image){
                    $clonedImage = $image->replicate();
                    $clonedImage->product_id = $cloneProduct->id;
                    $clonedImage->save();
                }
            }

            if ($product->categories){
                foreach ($product->categories as $category){
                    $cloneProduct->categories()->attach($category->id,['main_category' => $category->id === 0 ? 0 :1]);
                }
            }

            if($product->featureValue){
                foreach ($product->featureValue as $featureValue){
                    $cloneProduct->features()->attach($featureValue->feature_id,['feature_value_id' => $featureValue->id]);
                }
            }

            if ($product->relatedProducts){
                foreach ($product->relatedProducts as $relatedProduct) {
                    $relatedProduct = new RelatedProduct(["related_product_id" => $relatedProduct->related_product_id]);
                    $cloneProduct->relatedProducts()->save($relatedProduct);
                }
            }

            if ($product->properties){

                foreach ($product->properties as $property) {

                    $clonedProperty = $property->replicate();
                    $clonedProperty->product_id = $cloneProduct->id;
                    $clonedProperty->name = $property->name . ' کپی';
                    $clonedProperty->save();

                    $clonedProperty->propertyValue()->attach($property->propertyValue);
//                    dd($property->prices);

                    $propertyPrices = $property->prices->where('product_property_id','<>',null);
                    foreach ($propertyPrices as $propertyPrice){
//                        dd($property->prices);
                        $clonedPropertyPrice = $propertyPrice->replicate();
                        $clonedPropertyPrice->product_id = $cloneProduct->id;
                        $clonedPropertyPrice->product_property_id = $clonedProperty->id;
                        $clonedPropertyPrice->save();

                        $clonedPropertyPrice->roles()->attach($propertyPrice->roles);
                        $clonedPropertyPrice->users()->attach($propertyPrice->users);
                    }
                }
            }

            if ($cloneProduct->badges){
                $cloneProduct->badges()->attach($product->badges);
            }

            DB::commit();
            return array('status' => true,'data'=>null);
        }catch (\Exception $e){
            DB::rollBack();
            return array('status'=>false,'data'=>$e->getMessage());

        }

    }

}
