<?php

namespace App\Services;

use App\Filters\Admin\SubsetCompanyFilter;
use App\Http\Requests\SubsetCompany\PublishSubsetCompany;
use App\Http\Requests\SubsetCompany\StoreSubsetCompany;
use App\Http\Requests\SubsetCompany\SubsetCompanys;
use App\Http\Requests\SubsetCompany\UpdateSubsetCompany;
use App\Models\SubsetCompanies;

class SubsetCompanyService
{
    public static function index(SubsetCompanys $request){

        return SubsetCompanyFilter::filter($request);
    }

    public static function store(StoreSubsetCompany $request){

        try{
            $subsetCompany = new SubsetCompanies();
            $subsetCompany->established_year = $request->established_year;
            $subsetCompany->slug = $request->slug;
            $subsetCompany->published = $request->published;
            $subsetCompany->email = $request->email;
            $subsetCompany->tel1 = $request->tel1;
            $subsetCompany->tel2 = $request->tel2;
            $subsetCompany->postalCode = $request->postalCode;
            $subsetCompany->logo_url = $request->logo_url;
            $subsetCompany->image_url = $request->image_url;
            $subsetCompany->save();
            /* [title,about] */
            $subsetCompany->langs()->createMany(SetMultiLanguageRecords($request->langs));
            $subsetCompany->products()->attach($request->products);
            $subsetCompany->services()->attach($request->services);

            return $subsetCompany->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($subsetCompany){
        $columns = ['title','about','address','slogan'];
        return getMultiLanguageRecords(SubsetCompanies::whereId($subsetCompany)->with(['langs','services.langs','products.langs'])->first()->toArray(),$columns);
    }

    public static function update(UpdateSubsetCompany $request, $subsetCompany){

        try{
            $subsetCompany = SubsetCompanies::find($subsetCompany);
            $subsetCompany->established_year = $request->established_year;
            $subsetCompany->slug = $request->slug;
            $subsetCompany->published = $request->published;
            $subsetCompany->email = $request->email;
            $subsetCompany->tel1 = $request->tel1;
            $subsetCompany->tel2 = $request->tel2;
            $subsetCompany->postalCode = $request->postalCode;
            $subsetCompany->logo_url = $request->logo_url;
            $subsetCompany->image_url = $request->image_url;
            $subsetCompany->save();
            /* [title,about] */
            $subsetCompany->langs()->delete();
            $subsetCompany->products()->detach();
            $subsetCompany->services()->detach();
            $subsetCompany->langs()->createMany(SetMultiLanguageRecords($request->langs));
            $subsetCompany->products()->attach($request->products);
            $subsetCompany->services()->attach($request->services);

            return $subsetCompany->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($subsetCompanys): int
    {
        $subsetCompanys = explode(',',$subsetCompanys);
        $count = 0;
        foreach ($subsetCompanys as $subsetCompany){
            SubsetCompanies::destroy($subsetCompany);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishSubsetCompany $request){
        $subsetCompany = SubsetCompanies::find($request->id);
        $subsetCompany->published = $request->published;
        $subsetCompany->save();
    }

}
