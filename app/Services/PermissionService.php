<?php

namespace App\Services;


use App\Filters\Admin\PermissionFilter;
use App\Http\Requests\Permission\Permissions;
use App\Http\Requests\Permission\ShowPermission;
use App\Http\Requests\Permission\UpdatePermission;
use App\Models\Permission;

class PermissionService
{
    public static function index(Permissions $request){
        return PermissionFilter::filter($request);
    }

    public static function show($id){
        return Permission::where('id',$id)->with('roles')->first();
    }

    public static function update(UpdatePermission $request,$id){
        $permission = Permission::find($id);
        $permission->name = $request->input('name');
        $permission->save();

        $permission->roles()->detach();
        foreach ($request->input('roles') as $role){
            $permission->roles()->attach($role['id']);
        }
    }
}
