<?php

namespace App\Services;

use App\Filters\Admin\RoleFilter;
use App\Http\Requests\Role\PublishRole;
use App\Http\Requests\Role\Roles;
use App\Http\Requests\Role\StoreRole;
use App\Http\Requests\Role\UpdateRole;
use App\Models\Role;

class RoleService
{
    public static function index(Roles $request){

        return RoleFilter::filter($request);
    }
    public static function store(StoreRole $request){

        $role = new Role();
        $role->name         = $request->name;
        $role->label        = $request->label;
        $role->description  = $request->description;
        $role->published    = $request->published;
        $role->save();
        return $role->id;
    }

    public static function show($role){

        return $role = Role::find($role);
    }

    public static function update(UpdateRole $request, $country){

        $role = Role::find($country);
        $role->name         = $request->name;
        $role->label        = $request->label;
        $role->description  = $request->description;
        $role->published    = $request->published;
        $role->save();
    }

    public static function delete($roles){

//        Todo Check Role Used in Product ***

        $roles = explode(',',$roles);

        foreach ($roles as $role){
            Role::destroy($role);
        }
    }


    public static function publish(PublishRole $request)
    {
        $role = Role::find($request->id);
        $role->published = $request->input('published');
        $role->save();
    }


}
