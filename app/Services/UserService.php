<?php

namespace App\Services;


use App\Filters\Admin\UserFilter;
use App\Http\Requests\User\DeleteUser;
use App\Http\Requests\User\PublishUser;
use App\Http\Requests\User\ShowUser;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateUser;
use App\Http\Requests\User\Users;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public static function index(Users $request){

         return UserFilter::filter($request);
    }

    public static function store(StoreUser $request){

        $user = new User;
        $user->email        = $request->email;
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->national_id  = $request->national_id;
        $user->mobile       = $request->mobile;
        $user->card_number  = $request->card_number;
        $user->published    = $request->published;
        $user->role_id      = $request->role_id;
        $user->password     = Hash::make($request->password);
        $user->save();
        return $user->id;
    }

    public static function show($user){
        return $user = User::where('id',$user)->with('role')->first();
    }

    public static function update(UpdateUser $request, $user){

        $user = User::find($user);

        $user->email        = $request->email;
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->national_id  = $request->national_id;
        $user->mobile       = $request->mobile;
        $user->card_number  = $request->card_number;
        $user->published    = $request->published;
        $user->role_id      = $request->role_id;
        if(!is_null($request->password)){
                    $user->password     = Hash::make($request->password);
        }

        $user->save();
    }

    public static function delete($users){

        $users = explode(',',$users);

        $count = 0;
        foreach ($users as $user){
            $user = User::find($user);
            $userCart = $user->cart()->get();
            $userPost = $user->posts()->get();
            $userOrder = $user->orders()->get();
            $userProductComments = $user->productComments()->get();

            if ($userCart || $userPost || $userOrder || $userProductComments){
                return $count;
            }else{
                $user->delete();
                $user->address()->delete();
                $user->prices()->delete();
                $user->discounts()->delete();
                $user->discountUsedCount()->delete();
                $count++;
            }
        }

        return $count;
    }

    public static function publish(PublishUser $request){

        $user = User::find($request->id);
        $user->published = $request->published;
        $user->save();
    }
}
