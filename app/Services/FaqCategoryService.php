<?php

namespace App\Services;


use App\Filters\Admin\FaqCategoryFilter;
use App\Http\Requests\FaqCategory\FaqCategories;
use App\Http\Requests\FaqCategory\PublishFaqCategory;
use App\Http\Requests\FaqCategory\StoreFaqCategory;
use App\Http\Requests\FaqCategory\UpdateFaqCategory;
use App\Models\Faq;
use App\Models\FaqCategory;

class FaqCategoryService
{
    public static function index(FaqCategories $request){
        return FaqCategoryFilter::filter($request);
    }

    public static function store(StoreFaqCategory $request){

        try{
            $faqCategory = new FaqCategory();
            $faqCategory->published = $request->published;
            $faqCategory->save();
            /* [title] */
            $faqCategory->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $faqCategory->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($faqCategory){
        $columns = ['title'];
        return getMultiLanguageRecords(FaqCategory::whereId($faqCategory)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdateFaqCategory $request, $faqCategory){

        try{
            $faqCategory = FaqCategory::find($faqCategory);
            $faqCategory->published = $request->published;
            $faqCategory->save();
            /* [title] */
            $faqCategory->langs()->delete();
            $faqCategory->langs()->createMany(SetMultiLanguageRecords($request->langs));

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($faqs){

        $faqs = explode(',',$faqs);
        $count = 0;
        foreach ($faqs as $faq){
            $faqCategory = FaqCategory::find($faq);
            if($faqCategory->faqs()->count() === 0){
                $faqCategory->delete();
                $count++;
            }
        }

        return $count;
    }

    public static function publish(PublishFaqCategory $request){

        $faq = FaqCategory::find($request->id);
        $faq->published = $request->published;
        $faq->save();
    }

}
