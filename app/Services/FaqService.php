<?php

namespace App\Services;


use App\Filters\Admin\FaqFilter;
use App\Http\Requests\Faq\Faqs;
use App\Http\Requests\Faq\PublishFaq;
use App\Http\Requests\Faq\StoreFaq;
use App\Http\Requests\Faq\UpdateFaq;
use App\Models\Faq;

class FaqService
{
    public static function index(Faqs $request){
        return FaqFilter::filter($request);
    }

    public static function store(StoreFaq $request){

        try{
            $faq = new Faq();
            $faq->faq_category_id = $request->faq_category_id;
            $faq->published = $request->published;
            $faq->save();
            /* title,description */
            $faq->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $faq->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($faq): array
    {
        return getMultiLanguageRecords(Faq::whereId($faq)->with('langs')->first()->toArray(),['title']);
    }

    public static function update(UpdateFaq $request, $faq){

        try{
            $faq = Faq::find($faq);
            $faq->title = $request->title;
            $faq->description = $request->description;
            $faq->faq_category_id = $request->faq_category_id;
            $faq->published = $request->published;
            $faq->save();
            /* title,description */
            $faq->langs()->delete();
            $faq->langs()->createMany(SetMultiLanguageRecords($request->langs));

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($faqs){

        $faqs = explode(',',$faqs);
        $count = 0;
        foreach ($faqs as $faq){
            Faq::destroy($faq);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishFaq $request){

        $faq = Faq::find($request->id);
        $faq->published = $request->published;
        $faq->save();
    }

}
