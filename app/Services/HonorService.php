<?php

namespace App\Services;

use App\Filters\Admin\HonorFilter;
use App\Http\Requests\Honor\Honors;
use App\Http\Requests\Honor\PublishHonor;
use App\Http\Requests\Honor\StoreHonor;
use App\Http\Requests\Honor\UpdateHonor;
use App\Models\Honor;


class HonorService
{
    public static function index(Honors $request){

        return HonorFilter::filter($request);
    }

    public static function store(StoreHonor $request){

        try{
            $honor = new Honor();
            $honor->img_url = $request->img_url;
            $honor->published = $request->published;

            $honor->save();
            /* [title] */
            $honor->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $honor->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($honor){
        $columns = ['title'];
        return getMultiLanguageRecords(Honor::whereId($honor)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdateHonor $request, $honor){

        try{
            $honor = new Honor();
            $honor->img_url = $request->img_url;
            $honor->published = $request->published;
            $honor->save();
            /* [title] */
            $honor->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $honor->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($honors){

        $honors = explode(',',$honors);
        $count = 0;
        foreach ($honors as $honor){
            Honor::destroy($honor);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishHonor $request){
        $honor = Honor::find($request->id);
        $honor->published = $request->published;
        $honor->save();
    }

}
