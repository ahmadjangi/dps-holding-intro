<?php

namespace App\Services;


use App\Filters\Admin\BlogPostFilter;
use App\Http\Requests\BlogPost\Posts;
use App\Http\Requests\BlogPost\PublishPost;
use App\Http\Requests\BlogPost\StorePost;
use App\Http\Requests\BlogPost\UpdatePost;
use App\Http\Requests\Upload\BlogPostImage;
use App\Models\BlogPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BlogPostService
{
    public static function index(Posts $request){

        return BlogPostFilter::filter($request);
    }

    public static function store(StorePost $request){
        try{

            $userId = Auth::id();
            $post = new BlogPost();
            $post->slug = Str::slug($request->slug);
            $post->user_id = $userId;
            $post->image_url = $request->image_url;
            $post->published = $request->published;
            $post->save();

            $post->categories()->attach($request->category_id);
            $post->langs()->createMany(SetMultiLanguageRecords($request->langs));
            return $post->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($post){
        $columns = ['title','description','description_short','meta_description','meta_keyword'];
        return getMultiLanguageRecords(BlogPost::whereId($post)->with(['langs','categories'])->first()->toArray(),$columns);
    }

    public static function update(UpdatePost $request, $post){

        try {

            $userId = Auth::id();
            $post = BlogPost::find($post);
            $post->user_id = $userId;
            $post->slug = Str::slug($request->slug);
            $post->image_url = $request->image_url;
            $post->published = $request->published;
            $post->categories()->detach();
            $post->categories()->attach($request->category_id);

            $post->save();
            $post->langs()->delete();
            $post->langs()->createMany(SetMultiLanguageRecords($request->langs));
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($posts){

        $posts = explode(',',$posts);
        $count=0;
        foreach ($posts as $post){
            $post = BlogPost::find($post);
            $post->categories()->detach();
            if($post->delete())
                $count++;
        }
        return $count;

    }

    public static function publish(PublishPost $request){
        $brand = BlogPost::find($request->id);
        $brand->published = $request->published;
        $brand->save();
    }


    public function uploadImage(BlogPostImage $request)
    {

        $widths = array(444 ,825);
        $heights = array(246,344);

        $fileName = uniqid().'.jpg';

        for ($i=0; $i< count($widths); $i++){

            $canvas = Image::canvas($widths[$i], $heights[$i]);

            $image  = Image::make($request->file('file')->getRealPath())->fit($widths[$i], $heights[$i]);

            $fileUrl = 'img/blog/thumbnails/'.$widths[$i].'-'.$heights[$i].'/image_'.$fileName;

            $canvas->insert($image, 'center');
            $canvas->save($fileUrl);
        }

        return $fileName;
    }

}
