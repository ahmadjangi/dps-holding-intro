<?php

namespace App\Services;

use App\Filters\Admin\ClientFilter;
use App\Http\Requests\Client\Clients;
use App\Http\Requests\Client\PublishClient;
use App\Http\Requests\Client\StoreClient;
use App\Http\Requests\Client\UpdateClient;
use App\Models\Banner;
use App\Models\Client;

class ClientService
{
    public static function index(Clients $request){

        return ClientFilter::filter($request);
    }

    public static function store(StoreClient $request){

        try{
            $client = new Client();
            $client->img_url = $request->img_url;
            $client->published = $request->published;
            $client->save();

            $client->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $client->id;

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($client){
        $columns = ['title'];
        return getMultiLanguageRecords(Client::whereId($client)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdateClient $request, $client){

        try{
            $client = Client::find($client);

            $client->img_url = $request->img_url;
            $client->published = $request->published;
            $client->save();

            $client->langs()->delete();
            $client->langs()->createMany(SetMultiLanguageRecords($request->langs));

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($clients){

        $clients = explode(',',$clients);
        $count = 0;
        foreach ($clients as $client){
            Client::destroy($client);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishClient $request){
        $client = Client::find($request->id);
        $client->published = $request->published;
        $client->save();
    }

}
