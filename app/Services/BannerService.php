<?php

namespace App\Services;

use App\Filters\Admin\BannerFilter;
use App\Http\Requests\Banner\Banners;
use App\Http\Requests\Banner\PublishBanner;
use App\Http\Requests\Banner\StoreBanner;
use App\Http\Requests\Banner\UpdateBanner;
use App\Models\Banner;

class BannerService
{
    public static function index(Banners $request){

        return BannerFilter::filter($request);
    }

    public static function store(StoreBanner $request){

        try{
            $banner = new Banner();
            $banner->url = $request->url;
            $banner->image_url = $request->image_url;
            $banner->position = $request->position;
            $banner->published = $request->published;

            $banner->save();
            /* [title] */
            $banner->langs()->createMany(SetMultiLanguageRecords($request->langs));

            return $banner->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function show($banner){

        $columns = ['title'];
        return getMultiLanguageRecords(Banner::whereId($banner)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdateBanner $request, $banner){

        try{
            $banner = Banner::find($banner);

            $banner->url = $request->url;
            $banner->image_url = $request->image_url;
            $banner->position = $request->position;
            $banner->published = $request->published;
            $banner->save();

            $banner->langs()->delete();
            $banner->langs()->createMany(SetMultiLanguageRecords($request->langs));

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($banners){

        $banners = explode(',',$banners);
        $count = 0;
        foreach ($banners as $banner){
            Banner::destroy($banner);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishBanner $request){
        $banner = Banner::find($request->id);
        $banner->published = $request->published;
        $banner->save();
    }

}
