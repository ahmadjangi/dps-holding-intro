<?php

namespace App\Services;

use App\Filters\Admin\ServiceFilter;
use App\Http\Requests\Service\Services;
use App\Http\Requests\Service\PublishService;
use App\Http\Requests\Service\StoreService;
use App\Http\Requests\Service\UpdateService;
use App\Models\Service;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class ServiceService
{
    public static function index(Services $request)
    {

        return ServiceFilter::filter($request);
    }

    public static function store(StoreService $request){

        DB::beginTransaction();
        try{
            $service = new Service();
            $service->slug = $request->slug;
            $service->catalog_url = $request->catalog_url;
            $service->img_url = $request->img_url;
            $service->published = $request->published;
            $service->special = $request->special;

            $service->save();
            /* [title,description,short_description,meta_keyword,meta_description] */
            $service->langs()->createMany(SetMultiLanguageRecords($request->langs));
            $service->faqs()->attach($request->faq);
            DB::commit();
            return $service->id;

        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public static function show($service){
        $columns = ['lang_id','title','description','short_description','meta_keyword','meta_description'];
        return getMultiLanguageRecords(Service::whereId($service)->with('langs','faqs')->first()->toArray(),$columns);
    }

    public static function update(UpdateService $request, $service): string
    {
        try{
            $service = Service::find($service);
            $service->slug = $request->slug;
            $service->img_url = $request->img_url;
            $service->catalog_url = $request->catalog_url;
            $service->published = $request->published;
            $service->special = $request->special;

            $service->save();
            /* [title,description,short_description,meta_keyword,meta_description] */
            $service->langs()->delete();
            $service->faqs()->detach();
            $service->langs()->createMany(SetMultiLanguageRecords($request->langs));
            $service->faqs()->attach($request->faq);
            return $service->id;
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public static function delete($services): int
    {

        $services = explode(',',$services);
        $count = 0;
        foreach ($services as $service){
            Service::destroy($service);
            $count++;
        }
        return $count;
    }

    public static function publish(PublishService $request){
        $service = Service::find($request->id);
        $service->published = $request->published;
        $service->save();
    }

}
