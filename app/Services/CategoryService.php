<?php

namespace App\Services;

use App\Filters\Admin\CategoryFilter;
use App\Http\Requests\Category\ArrangeCategory;
use App\Http\Requests\Category\Categories;
use App\Http\Requests\Category\PublishCategory;
use App\Http\Requests\Category\StoreCategory;
use App\Http\Requests\Category\UpdateCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoryService
{
    public static function index(Categories $request){

        return CategoryFilter::filter($request);
    }

    public static function store(StoreCategory $request){

        DB::beginTransaction();
        try{

            $category = new Category;

            $category->slug = $request->slug;
            $category->published = $request->published;

            $category->save();

            /* [title,meta_keyword,meta_description,status,partner_title] */
            $category->langs()->createMany(SetMultiLanguageRecords($request->langs));

            DB::commit();

            return array('status' => true,'data'=>$category->id);
        }catch (\Exception $e){

            DB::rollBack();

            return array('status'=>false,'data'=>$e->getMessage());
        }
    }

    public static function show($category){

        $columns = ['title','meta_keyword','meta_description'];
        return getMultiLanguageRecords(Category::whereId($category)->with('langs')->first()->toArray(),$columns);
    }

    public static function update(UpdateCategory $request, $category){

        DB::beginTransaction();
        try{

            $category = Category::find($category);

            $category->slug = $request->slug;
            $category->published = $request->published;

            $category->save();

            /* [title,meta_keyword,meta_description,status,partner_title] */
            $category->langs()->delete();
            $category->langs()->createMany(SetMultiLanguageRecords($request->langs));


            DB::commit();

            return array('status' => true,'data'=>$category->id);
        }catch (\Exception $e){

            DB::rollBack();

            return array('status'=>false,'data'=>$e->getMessage());
        }
    }

    public static function delete($categories){

//        Todo Check Category Used in Product ***

        $categories = explode(',',$categories);

        foreach ($categories as $category){
            Category::destroy($category);
        }
    }

    public static function publish(PublishCategory $request){

        $brand = Category::find($request->id);
        $brand->published = $request->published;
        $brand->save();
    }

}
