<?php

namespace App\Services;

use App\Http\Requests\Slide\ArrangeSlide;
use App\Http\Requests\Upload\BannerImage;
use App\Http\Requests\Upload\CatalogFile;
use App\Http\Requests\Upload\ClientImage;
use App\Http\Requests\Upload\HonorImage;
use App\Http\Requests\Upload\ProjectImage;
use App\Http\Requests\Upload\ServiceImage;
use App\Models\CommonImage;
use App\Http\Requests\Upload\BadgeImage;
use App\Http\Requests\Upload\BlogPostImage;
use App\Http\Requests\Upload\BrandLogo;
use App\Http\Requests\Upload\CategoryImage;
use App\Http\Requests\Upload\GeneralImage;
use App\Http\Requests\Upload\ProductImage;
use App\Models\SiteConfig;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UploadService
{

    public static function subsetCompanyImage(ClientImage $request): string
    {
        $path = $request->file('file')->store('subsetCompanyImage','public');
        return Storage::url($path);
    }

    public static function clientImage(ClientImage $request): string
    {

        $path = $request->file('file')->store('clientImage','public');
        return Storage::url($path);
    }

    public static function serviceImage(ServiceImage $request){

        $image = $request->file('file');
        $input['imageName'] = time().'.'.$image->extension();

        $filePath = [
            ['width' => 1920,'height' => 605,'path'  => '/storage/serviceImage'],
            ['width' => 950,'height' => 545,'path'   => '/storage/serviceImage/thumbnail/950'],
            ['width' => 551,'height' => 581,'path'   => '/storage/serviceImage/thumbnail/551'],
        ];

        foreach ($filePath as  $item){
            $img = Image::make($image->path());
            $width = $item['width'];
            $height = $item['height'];
            if ($width > $height){
                $img->resize($width,null, function ($const) {
                    $const->aspectRatio();
                })->crop($width, $height,0,intval($img->height()/2 - ($height- $img->height()/2)))->save(env('App_OFFLINE') ? public_path($item['path']).'/'.$input['imageName'] : env('APP_ROOT_FOLDER').$item['path'].'/'.$input['imageName']);
            }else{
                $img->resize(null, $height, function ($const) {
                    $const->aspectRatio();
                })->crop($width, $height,intval($img->width()/2 - ($width - $img->width()/2)),0)->save(env('App_OFFLINE') ? public_path($item['path']).'/'.$input['imageName'] : env('APP_ROOT_FOLDER').$item['path'].'/'.$input['imageName']);
            }
        }

        return $input['imageName'] ;
    }

    public static function projectImage(ProjectImage $request): string
    {
        $path = $request->file('file')->store('projectImage','public');
        return Storage::url($path);
    }
    public static function honorImage(HonorImage $request): string
    {
        $path = $request->file('file')->store('honorImage','public');
        return Storage::url($path);
    }

    public static function productImage(ProductImage $request){

        $image = $request->file('file');
        $input['imageName'] = time().'.'.$image->extension();

        $filePath = [
            ['size' => 1000,'path'  => '/storage/productImage'],
            ['size' => 600,'path'   => '/storage/productImage/thumbnail/600'],
            ['size' => 300,'path'   => '/storage/productImage/thumbnail/300']
        ];

        $img = Image::make($image->path());

        foreach ($filePath as  $item){

            $img->resize($item['size'], $item['size'], function ($const) {
                $const->aspectRatio();
            })->save(env('App_OFFLINE') ? public_path($item['path']).'/'.$input['imageName'] : env('APP_ROOT_FOLDER').$item['path'].'/'.$input['imageName']);
        }

        return $input['imageName'] ;
    }

    public static function generalImage(GeneralImage $request){

        $path = $request->file('file')->store('generalImage','public');
        $path = Storage::url($path);

        $config = SiteConfig::firstOrNew(
            ['name' => 'siteLogoUrl']
        );

        $config->value = $path;

        $config->save();

        return $path;
    }

    public static function alternativeLogo(GeneralImage $request){

        $path = $request->file('file')->store('generalImage','public');
        $path = Storage::url($path);

        $config = SiteConfig::firstOrNew(
            ['name' => 'siteLogoUrlAlternative']
        );

        $config->value = $path;

        $config->save();

        return $path;
    }

    public static function aboutUsImage(GeneralImage $request){

        $path = $request->file('file')->store('generalImage','public');
        $path = Storage::url($path);

        $config = SiteConfig::firstOrNew(
            ['name' => 'aboutUsImage']
        );

        $config->value = $path;

        $config->save();

        return $path;
    }

    public static function BlogPostImage(BlogPostImage $request){

        $path = $request->file('file')->store('blogPostImage','public');
        return Storage::url($path);
    }

    public static function slideImage(GeneralImage $request){

        $path = $request->file('file')->store('slideImage','public');
        $path = Storage::url($path);

        $lastArrangement = CommonImage::max('arrangement');
        $slideImage = new CommonImage();

        $slideImage->image_url      = $path;
        $slideImage->type           = 'homeSlide';
        $slideImage->arrangement    = $lastArrangement + 1 ;
        $slideImage->save();

        return array('id' => $slideImage->id,'image_url' => $path, 'type' => 'homeSlide');
    }

    public static function uploadImage(GeneralImage $request){

        $path = $request->file('file')->store('generalImage','public');
        $path = Storage::url($path);

        return env('APP_URL') . $path;
    }

    public function slides(){
        return CommonImage::where('type','homeSlide')->get();
    }

    public function deleteSlides($id){
        return CommonImage::find($id)->delete();
    }

    public static function bannerImage(BannerImage $request){

        $path = $request->file('file')->store('bannerImage','public');
        return Storage::url($path);
    }

    public static function catalogFile(CatalogFile $request){

        $path = $request->file('file')->store('catalogFile','public');
        return Storage::url($path);
    }


    public static function arrange(ArrangeSlide $request)
    {
        foreach ($request->arrangeList as $item){
            CommonImage::whereId($item['id'])->update(['arrangement'=>$item['position']]);
        }
    }


}
