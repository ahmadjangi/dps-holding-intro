<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psy\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\ValidationException;

use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Throwable $exception
     * @return void
     * @throws Throwable
     */
    public function report(Throwable  $exception)
    {
        parent::report($exception);
    }

    public function register()
    {
//        $this->renderable(function (NotFoundHttpException $e, $request) {
//            if ($request->is('api/*')) {
//                $error = $e->getMessage();
//                return response()->json([
//                    'data' => $error,
//                    'status' => [
//                        'message'=>__('general.NotFoundHttpException'),
//                        'code'=>Config::get('constants.apiResponseCodes.NotFoundHttpException')],
//                ],404);
//            }
//        });
//
//        $this->renderable(function (AuthorizationException $e, $request) {
//            if ($request->is('api/*')) {
//                return response()->json([
//                    'data' => '',
//                    'status' => [
//                        'message'=>__('general.AuthorizationException'),
//                        'code'=>Config::get('constants.apiResponseCodes.AuthorizationException')],
//                ],404);
//            }
//        });
//
//        $this->renderable(function (ValidationException $e, $request) {
//            if ($request->is('api/*')) {
//                $error = $e->validator->errors()->getMessages();
//                return response()->json([
//                    'data' => $error,
//                    'status' => [
//                        'message' => __('general.ValidationException'),
//                        'code' => Config::get('constants.apiResponseCodes.ValidationException')],
//                ], 402);
//            }
//        });
//
//        $this->renderable(function (QueryException $e, $request) {
//            if ($request->is('api/*')) {
//                $error = $e->validator->errors()->getMessages();
//                return response()->json([
//                    'data' => $error,
//                    'status' => [
//                        'message'=>__('general.ValidationException'),
//                        'code'=>Config::get('constants.apiResponseCodes.ValidationException')],
//                ],402);
//            }
//
//        });
//
//        $this->renderable(function(FatalErrorException $e, $request){
//            if ($request->is('api/*')) {
//                $error = $e->getMessage();
//                return response()->json([
//                    'data' => $error,
//                    'status' => [
//                        'message' => __('general.FatalErrorException'),
//                        'code' => Config::get('constants.apiResponseCodes.FatalErrorException')],
//                ], 405);
//            }
//        });

    }

}
