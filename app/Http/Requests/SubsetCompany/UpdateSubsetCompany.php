<?php

namespace App\Http\Requests\SubsetCompany;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubsetCompany extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'bail|required|array',
            'langs.title' => 'bail|required|array',
            'langs.title.*' => 'bail|required|max:255|min:3|string',
            'langs.address' => 'bail|required|array',
            'langs.address.*' => 'bail|required|max:255|min:3|string',
            'langs.slogan' => 'bail|required|array',
            'langs.slogan.*' => 'bail|required|max:255|min:3|string',
            'langs.about' => 'bail|required|array',
            'langs.about.*' => 'bail|required|max:255|min:3|string',
            'slug' => 'bail|required|string',
            'logo_url' => 'bail|required|string',
            'image_url' => 'bail|required|string',
            'published' => 'bail|required|boolean',
        ];
    }
}
