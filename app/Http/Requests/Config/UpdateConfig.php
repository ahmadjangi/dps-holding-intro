<?php

namespace App\Http\Requests\Config;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateConfig extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'defaultCurrency'               => 'bail|required|integer',
            'defaultCountry'                => 'bail|required|integer',
            'priceMode'                     => 'bail|required',Rule::in(['high', 'low']),
            'title'                         => 'bail|required|string',
            'metaKeyword'                   => 'bail|required|string',
            'metaDescription'               => 'bail|required|string',
            'siteLogoUrl'                   => 'bail|required|string',
            'shortDescription'              => 'bail|required|string',
            'blogAboutText'                 => 'bail|required|string',
            'shippingPriceByWeight'         => 'bail|required|numeric',
            'shippingPriceByFixedPrice'     => 'bail|required|numeric',
            'shippingPriceByCampaignPrice'  => 'bail|required|numeric',
            'shippingPriceMethod'           => 'bail|required|string',
            'address'                       => 'bail|string',
        ];
    }
}
