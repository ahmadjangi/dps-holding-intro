<?php

namespace App\Http\Requests\Honor;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHonor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'bail|required|integer',
            'langs' => 'bail|required|array',
            'langs.title' => 'bail|required|array',
            'langs.title.*' => 'bail|required|max:255|min:3|string',
            'img_url' => 'bail|required|string',
            'published' => 'bail|required|boolean',
        ];
    }
}
