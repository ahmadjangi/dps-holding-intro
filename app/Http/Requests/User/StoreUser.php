<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'bail|required|max:255|min:3|string',
            'last_name'     => 'bail|required|max:255|min:3|string',
            'email'         => 'bail|unique:users|required|email',
            'national_id'   => 'unique:users|digits:10',
            'mobile'        => 'unique:users|bail|required|digits:11',
            'card_number'   => 'digits:16',
            'published'     => 'bail|required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'نام اجباری است.',
            'last_name.required' => 'نام خانوادگی اجباری است.',
            'email.required' => 'ایمیل اجباری است.',
            'mobile.required' => 'موبایل اجباری است.',
        ];
    }
}
