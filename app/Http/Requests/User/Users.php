<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Users extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'bail|max:255|min:3|string|nullable',
            'last_name'     => 'bail|max:255|min:3|string|nullable',
            'email'         => 'bail|email|nullable',
            'national_id'   => 'digits:10|nullable',
            'mobile'        => 'digits:11|nullable',
            'card_number'   => 'digits:16|nullable',
            'published' => 'bail|boolean',
            'page'          => 'integer|nullable',
            'recordCount'   => 'integer|nullable',
            'sortAsc'      => 'string|nullable',
            'sortBy'       => 'string|nullable',
        ];
    }
}
