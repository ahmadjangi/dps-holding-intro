<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'bail|required|integer',
            'langs' => 'bail|required|array',
            'langs.title' => 'bail|required|array',
            'langs.title.*' => 'bail|required|max:255|min:3|string',
            'langs.description' => 'bail|required|array',
            'langs.description.*' => 'bail|string',
            'langs.short_description' => 'bail|required|array',
            'langs.short_description.*' => 'bail|string',
            'langs.meta_keyword' => 'bail|required|array',
            'langs.meta_keyword.*' => 'bail|string',
            'langs.meta_description' => 'bail|required|array',
            'langs.meta_description.*' => 'bail|string',
            'langs.status' => 'bail|required|array',
            'langs.status.*' => 'bail|string',
            'langs.partner_title' => 'bail|required|array',
            'langs.partner_title.*' => 'bail|string',
            'img_url' => 'bail|required|string',
            'slug' => 'bail|required|string',
            'published' => 'bail|required|boolean',
        ];
    }
}
