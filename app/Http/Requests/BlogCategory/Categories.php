<?php

namespace App\Http\Requests\BlogCategory;

use Illuminate\Foundation\Http\FormRequest;

class Categories extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'          => 'max:255|min:3|string|nullable',
            'page'          => 'integer|nullable',
            'recordCount'   => 'integer|nullable',
            'parentId'      => 'integer|nullable',
            'sortAsc'       => 'string|nullable',
            'sortBy'        => 'string|nullable',
        ];
    }
}
