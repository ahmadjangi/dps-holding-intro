<?php

namespace App\Http\Requests\BlogCategory;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'           => 'bail|required|integer',
            'parent_id'      => 'required|integer',
            'langs' => 'bail|required|array',
            'langs.title'  => 'bail|required|array',
            'langs.title.*'  => 'bail|required|max:255|min:2|string',
            'langs.description'  => 'bail|array',
            'langs.description.*'  => 'max:500|string',
        ];
    }
}
