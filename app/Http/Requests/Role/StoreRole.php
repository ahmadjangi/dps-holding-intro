<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class StoreRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'bail|required|max:255|min:3|string',
            'label'         => 'bail|max:255|min:3|string|nullable',
            'description'   => 'bail|max:255|min:3|string|nullable',
            'published'     => 'bail|required|boolean',
        ];
    }
}
