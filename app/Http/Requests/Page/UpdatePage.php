<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'bail|required|integer',
            'langs' => 'bail|required|array',
            'langs.title' => 'bail|required|array',
            'langs.title.*' => 'bail|required|max:255|min:3|string',
            'langs.content' => 'bail|required|array',
            'langs.content.*' => 'bail|required|string',
            'langs.meta_keyword' => 'bail|required|array',
            'langs.meta_keyword.*' => 'bail|required|string',
            'langs.meta_description' => 'bail|required|array',
            'langs.meta_description.*' => 'bail|required|string',
//            'image_url' => 'bail|required|string',
            'slug' => 'bail|required|string',
            'published' => 'bail|required|boolean',
        ];
    }
}
