<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('category');

        return [
            'langs' => 'bail|required|array',
            'langs.title' => 'bail|required|array',
            'langs.meta_description' => 'bail|required|array',
            'langs.meta_keyword' => 'bail|required|array',
            'langs.title.*'                  =>'bail|required|max:255|min:3|string',
            'langs.meta_description.*'       =>'max:255|min:3|string|nullable',
            'langs.meta_keyword.*'           =>'max:255|min:3|string|nullable',
            'published'             =>'bail|boolean|nullable',
            'slug'                  =>'bail|max:255|string|nullable|unique:products,slug,'.$id,
        ];
    }
}
