<?php

namespace App\Http\Requests\BlogPost;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id'       => 'required|integer',
            'langs' => 'bail|required|array',
            'langs.title'  => 'bail|required|array',
            'langs.title.*'  => 'bail|required|max:255|min:2|string',
            'langs.description_short'  => 'bail|required|array',
            'langs.description_short.*' => 'bail|required|max:1000|string',
            'langs.description'  => 'bail|required|array',
            'langs.description.*'       => 'nullable|string',
            'image_url'         => 'nullable|string',
            'langs.meta_keyword'  => 'bail|required|array',
            'langs.meta_keyword.*'      => 'bail|nullable|max:255|string',
            'langs.meta_description'  => 'bail|required|array',
            'langs.meta_description.*'  => 'bail|nullable|max:255|string',
        ];
    }
}
