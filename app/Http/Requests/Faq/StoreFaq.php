<?php

namespace App\Http\Requests\Faq;

use Illuminate\Foundation\Http\FormRequest;

class StoreFaq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'bail|required|array',
            'langs.title'  => 'bail|required|array',
            'langs.title.*'  => 'bail|required|max:255|min:2|string',
            'langs.description'  => 'bail|required|array',
            'langs.description.*'  => 'bail|required|string',
            'faq_category_id'  => 'bail|required|integer',
            'published' => 'bail|required|boolean'
        ];
    }
}
