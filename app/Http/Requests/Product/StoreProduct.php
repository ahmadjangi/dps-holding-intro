<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'bail|required|array',
            'langs.title' => 'bail|required|array',
//            'langs.description' => 'bail|required|array',
            'langs.short_description' => 'bail|required|array',
            'langs.meta_description' => 'bail|required|array',
            'langs.meta_keyword' => 'bail|required|array',
            'langs.title.*'                  =>'bail|required|max:255|min:3|string',
//            'langs.description.*'           =>'min:3|string|nullable',
            'langs.short_description.*'      =>'min:3|string|nullable',
            'langs.meta_description.*'       =>'max:255|min:3|string|nullable',
            'langs.meta_keyword.*'           =>'max:255|min:3|string|nullable',
            'slug'                   =>'bail|max:255|string|unique:products|nullable',
            'published'             =>'bail|boolean|nullable',
            'features'              =>'bail|Array',
            'relatedProduct'            =>'bail|Array',
        ];
    }
}
