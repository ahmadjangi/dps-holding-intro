<?php

namespace App\Http\Requests\FaqCategory;

use Illuminate\Foundation\Http\FormRequest;

class StoreFaqCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'bail|required|array',
            'langs.title'  => 'bail|required|array',
            'langs.title.*'  => 'bail|required|max:255|min:2|string',
            'published' => 'bail|required|boolean'
        ];
    }
}
