<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Illuminate\Http\Request;
use Closure;

class RolesAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth('api')->user()){

            $role = Role::findOrFail(auth('api')->user()->role_id);
            $permissions = $role->permissions;
            // get requested action
            $actionName = class_basename($request->route()->getActionname());
            // check if requested action is in permissions list
            foreach ($permissions as $permission)
            {
                $_namespaces_chunks = explode('\\', $permission->controller);
                $controller = end($_namespaces_chunks);

                if ($actionName === $controller . '@' . $permission->method)
                {
                    // authorized request
                    return $next($request);
                }
            }

            // none authorized request
            return apiResponse(null,['auth.Unauthorized',[]],
                'constants.apiResponseCodes.AuthorizationException',403);

        }else{
            return $next($request);
        }
    }
}
