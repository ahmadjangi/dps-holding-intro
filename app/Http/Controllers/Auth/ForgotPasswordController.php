<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserVerify;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        $passwordResetType = 'mobile';

        if($passwordResetType === 'email'){
            return view('auth.passwords.email');
        }else{
            return view('auth.passwords.mobile');
        }
    }

    public function sendVerifyCode(Request $request)
    {
        $request->validate([
            'mobile' => ['required', 'numeric', 'min:11','exists:users,mobile']
        ],[
            'mobile.exists' => 'این کاربر وجود ندارد، لطفا ثبت نام کنید.'
        ]);

        $mobile = $request->input('mobile');

        $verifyCode = generateUserVerifyCode(UserVerify::class, 'code');

        UserVerify::updateOrCreate(
            ['mobile'       => $mobile],
            ['ip'           => request()->ip(),
                'agent'     => request()->userAgent(),
                'code'      => $verifyCode]
        );

        return view('auth.passwords.mobileVerify',compact('mobile'));
    }

    public function showNewPassword(Request $request)
    {

        $validatedData =  $request->validate([
            'verifyCode' => ['required', 'numeric','exists:user_verifies,code'],
        ],[
            'verifyCode.required' => 'کد تایید را وارد نمایید.',
            'verifyCode.exists' => 'کد تایید نامعتبر است.'
        ]);

        $mobile = $request->input('mobile');
        $codeVerify = UserVerify::where([
            ['mobile',$mobile],
            ['code',$request->input('verifyCode')]
        ])->first();

        if($codeVerify){
            return view('auth.passwords.mobileReset',compact('mobile'));
        }
    }

    public function newPassword(Request $request)
    {
        DB::beginTransaction();

        $request->validate([
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        try {
            $mobile = $request->input('mobile');

            $user = User::where('mobile',$mobile)->first();
            $user->password = Hash::make($request->input('password'));
            $user->save();

            return view('auth.passwords.mobileConfirm',['message'=>'رمز عبور تغییر یافت.']);

        }catch (\Exception $e){
            return redirect(route('password.request'))->with('message','تغییر رمز عبور با خطا مواجه شد.');
        }
    }
}
