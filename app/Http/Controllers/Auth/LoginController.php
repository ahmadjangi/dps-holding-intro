<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\UserVerify;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     *
     * @throws ValidationException
     */
    public function login(Request $request)
    {

        $loginWith = session('loginWith');
        $verifyCode = session('verifyCode');

        $request->request->add([$loginWith => session($loginWith)]);


        if($loginWith === 'email'){
            $request->validate([
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string'],
            ]);
        }elseif ($loginWith === 'mobile' && !is_null($verifyCode)){
            $request->validate([
                'mobile' => ['required', 'numeric', 'min:11'],
                'verifyCode' => ['required', 'numeric','exists:user_verifies,code'],
            ],[
                'verifyCode.required' => 'کد تایید را وارد نمایید.',
                'verifyCode.exists' => 'کد تایید نامعتبر است.'

            ]);
        }elseif ($loginWith === 'mobile' && is_null($verifyCode)){

            $request->validate([
                'mobile' => ['required', 'numeric', 'min:11'],
                'password' => ['required', 'string'],
            ]);
        }


        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function attemptLogin(Request $request)
    {

        if(is_null($request->verifyCode)){

            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        }else{
            $codeVerify = UserVerify::where([
                [$this->username(),$request->input($this->username())],
                ['code',$request->input('verifyCode')]
            ])->first();

            if($codeVerify){
                $user = User::where('mobile',$codeVerify->mobile)->first();
                Auth::loginUsingId($user->id);
                session()->forget('mobile');
                session()->forget('verifyCode');
                return true;
            }else{
                return false;
            }
        }
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password','verifyCode');
    }

    public function username()
    {
        return session('loginWith');
    }

    protected function sendLoginResponse(Request $request)
    {

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        self::applyCartToLoginedUser($request, $this->guard()->user());

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }
        // session clear in step because username require
        session()->forget('loginWith');
        $data = array('redirectUrl'=>   session()->get('url.intended'));

        return apiResponse($data,['',[]],
            'constants.apiResponseCodes.success');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return apiResponse(null,['auth.usernameOrPasswordNotCorrect',[]],
            'constants.apiResponseCodes.success',403);
    }

    protected function sendBlockedUserResponse(Request $request)
    {
        return apiResponse(null,['auth.yourAccountIsBlockedPleaseContactWithAdmin',[]],
            'constants.apiResponseCodes.success',403);
    }

    public function applyCartToLoginedUser(Request $request, $user){

        $newCartCount = 0;
        $newCartTotalAmount = 0;

        DB::beginTransaction();

        try {

            $cart = Cart::where('user_id',$user->id)->first();
            $cartId = $request->cookie('cart_id');

            if(is_null($cart)){

                $cart = Cart::find($cartId);
                if ($cart){

                    $cart->user_id  = $user->id;
                    $cart->save();

                    Cookie::queue(Cookie::forget('cart_id'));
                }

            }else{
                $notAssignedUserCart = Cart::find($cartId);
                if(!is_null($notAssignedUserCart)){

                    $cartProducts = $notAssignedUserCart->cartProducts()->get();

                    foreach ($cartProducts as $cartProduct){

                        $cartProductExist = CartProduct::where([
                            ['product_property_id','=', $cartProduct->product_property_id],
                            ['cart_id','=', $cart->id]
                        ])->first();

                        $newCartCount = $newCartCount + $cartProduct->quantity;
                        $newCartTotalAmount = $newCartTotalAmount + ($cartProduct->quantity * $cartProduct->price);

                        if(is_null($cartProductExist)){
                            $cartProduct->cart_id = $cart->id;
                            $cartProduct->save();
                        }else{
                            $cartProductExist->cart_id = $cart->id;
                            $cartProductExist->quantity = $cartProductExist->quantity + $cartProduct->quantity;
                            $cartProductExist->price = $cartProduct->price;
                            $cartProductExist->save();
                            $cartProduct->delete();
                        }
                    }

                    $cart->count = $cart->count + $newCartCount;
                    $cart->total_amount = $cart->total_amount + $newCartTotalAmount;
                    $cart->save();

                    $notAssignedUserCart->delete();
                    Cookie::queue(Cookie::forget('cart_id'));
                }
            }

            DB::commit();

            return true;

        }catch (\Exception $e){
            DB::rollBack();

            return array('status' => false, 'error' => $e->getMessage());
        }
    }
}

