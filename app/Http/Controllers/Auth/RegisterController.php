<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\UserVerify;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\isNull;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'mobile' => ['required', 'numeric', 'min:11', 'unique:users'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'verifyCode' => ['required', 'numeric','exists:user_verifies,code'],
        ],[
            'verifyCode.exists' => 'کد تایید نامعتبر است.'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'role_id' => $data['role_id'],
            'mobile' => $data['mobile'],
        ]);

    }

    public function register(Request $request)
    {
//        $request->session()->regenerate();

        $data = array('mobile' => session('mobile'), 'verifyCode' => $request->input('verifyCode'),'role_id' => 2);

        $this->validator($data)->validate();

        $codeVerify = UserVerify::where([
            ['mobile',$data['mobile']],
            ['code',$data['verifyCode']]
        ])->count();

        if($codeVerify > 0){

            event(new Registered($user = $this->create($data)));

            $this->guard()->login($user);

            if ($response = $this->registered($request, $user)) {
                return $response;
            }
            self::applyCartToRegisteredUser($request, $this->guard()->user());

            session()->forget('mobile');
            session()->forget('verifyCode');

//            $data = array('redirectUrl'=>'/');
            $data = array('redirectUrl'=>   session()->get('url.intended'));

            return apiResponse($data,['',[]],
                'constants.apiResponseCodes.success');
        }else{
            return apiResponse(null,['auth.EnteredCodeNoteCorrect',[]],
                'constants.apiResponseCodes.error',422);
        }

    }

    public function applyCartToRegisteredUser(Request $request, $user){

        DB::beginTransaction();

        try {

            $cart = Cart::where('user_id',$user->id)->first();
            $cartId = $request->cookie('cart_id');

            if(is_null($cart) && !is_null($cartId)){

                $cart = Cart::find($cartId);
                $cart->user_id  = $user->id;
                $cart->save();

                Cookie::queue(Cookie::forget('cart_id'));
            }

            DB::commit();

            return true;

        }catch (\Exception $e){
            DB::rollBack();

            return array('status' => false, 'error' => $e->getMessage());
        }
    }
}
