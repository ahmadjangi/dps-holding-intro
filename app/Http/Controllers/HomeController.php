<?php

namespace App\Http\Controllers;

use App\Classes\PriceFilter;
use App\Classes\SendSMS;
use App\Filters\Front\ProductsFilter;
use App\Models\Banner;
use App\Models\BlogPost;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\CommonImage;
use App\Models\Honor;
use App\Models\PriceProduct;
use App\Models\Product;
use App\Models\ProductProperty;
use App\Models\Service;
use App\Models\SubsetCompanies;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;
use Illuminate\Pagination\LengthAwarePaginator;

use phpDocumentor\Reflection\Types\Self_;
use stdClass;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $lang = app()->getLocale();
        $slides = Banner::with(['langs'=>function ($query)use ($lang){$query->where('lang_id',$lang);}])->where('position', 'slide')->where('published', 1)->get();
        $services = Service::with(['langs'=>function ($query)use ($lang){$query->where('lang_id',$lang);}])->where('published', 1)->get();
        $specialServices = $services->where('special', 1)->take(3);
        $specialProducts = Product::with(['langs'=>function ($query)use ($lang){$query->where('lang_id',$lang);},'images'])->where('published', 1)->where('special', 1)->take(4)->get();
        $banners = Banner::where('position', 'like', '%home%')->where('published', 1)->get();
        $blog = BlogPost::with('user')->currentLang($lang)->orderBy('created_at')->get();

        return view('home', compact(
            'blog',
            'slides',
            'specialProducts',
            'services',
            'specialServices',
            'banners'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param $languageCode
     * @return RedirectResponse
     */
    public function changeLang($languageCode): RedirectResponse
    {
        App::setLocale($languageCode);
        session()->put("lang_code",$languageCode);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Factory|View
     */
    public function contact()
    {
        return view('general.contact');
    }

    public function sendMessage(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'bail|required|string|max:255',
            'subject' => 'bail|string',
            'phone' => 'bail|digits:11',
            'email' => 'bail|required|email',
            'message' => 'bail|required|max:9000',
        ]);

        try {

            $configs = getSiteConfigs();
            $to_name = 'مدیرسایت';
            $to_email = $configs->email;
            $data = array(
                'name' => $request->fullName,
                'subject' => $request->subject,
                'phone' => $request->phone,
                'email' => $request->email,
                'messages' => $request->message);

            Mail::send('email.email', $data, function ($message) use ($to_name, $to_email, $request) {
                $message->to($to_email, $to_name)->subject('ایمیل از فروشگاه');
                $message->from($request->email, 'پیام از سایت');
            });

            SendSMS::sendContact('رامین',$request->subject, $request->email, '09143012118', 'getMessage');

            return Response::json(array('status' => true), 200);
        } catch (\Exception $e) {
            return Response::json($e->getMessage(), 500);
        }
    }

    public function sendGetAdvice(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'bail|required|string|max:255',
            'phone' => 'bail|digits:11',
            'email' => 'bail|required|email',
        ]);

        try {

            SendSMS::sendContact('رامین',$request->name, $request->phone, $request->email, 'getAdvice');

            return Response::json(array('status' => true), 200);
        } catch (\Exception $e) {
            return Response::json($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Factory|View
     */
    public function faq()
    {
        $faqs = FaqCategory::with('faqs')->where('published', 1)->get();
        return view('general.faq', compact('faqs'));
    }

    public function about(){
        return view('general.about');
    }

    public function products(){

        $categories = Category::where('published', 1)->with(['langs'])->get();
        $products = Product::with([
            'langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
            'images'=>function ($query){$query->where('is_main',1);}])->where('published', 1)->paginate(3);
        return view('product.products',compact('products','categories'));
    }

    public function showProduct($slug){
        $products = Product::with([
            'langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
            'images'=>function ($query){$query->where('is_main',1);}])->where('published', 1)->take(4)->get();
        $product = Product::with([
            'langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
            'images'=>function ($query){$query->where('is_main',1);}
        ])->where('published', 1)->where('slug',$slug)->first();
        return view('product.product',compact('product','products'));
    }

    public function services(){
        $services = Service::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());}])->where('published', 1)->take(3)->get();
        return view('service.services',compact('services'));
    }

    public function showService($slug){
        $services = Service::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());}])->where('published', 1)->get();
        $service = Service::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'faqs'])->where('published', 1)->where('slug',$slug)->first();
        return view('service.service',compact('services','service'));
    }

    public function honors(){
        $honors = Honor::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());}])->where('published', 1)->get();
        return view('general.honor',compact('honors'));
    }

    public function subsetCompany($slug){
        $subsetCompany = SubsetCompanies::with([
            'langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
            'services.langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
            'products.langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
        ])->where('slug',$slug)->first();

        $services = $subsetCompany->services->count() > 0 ? $subsetCompany->services : null;
        $products = $subsetCompany->products->count() > 0 ? $subsetCompany->products : null;
        return view('subsetCompany.subsetCompany',compact('subsetCompany','services','products'));
    }

}
