<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function loginRegister(){

        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }
        return view('auth.loginRegister');
    }

    public function doLoginRegister(Request $request){

        $siteConfig =  getSiteConfigs();
        if (!$siteConfig->catalogMode){
            $emailMobile = $request->input('emailMobile');

            session()->forget('verifyCode');
            session()->forget('mobile');

            if(validateIranMobile($emailMobile)){
                return $this->loginWithMobile($emailMobile);
            }elseif(filter_var($emailMobile, FILTER_VALIDATE_EMAIL)){
                return $this->loginWithEmail($emailMobile);
            }else{
                return apiResponse(false,['auth.PleaseEnterValidMobile',[]],
                    'constants.apiResponseCodes.error',422);
            }
        }
    }

    public function loginWithMobile($mobile){

        $user = User::where('mobile',$mobile)->first();
        session(['mobile' => $mobile]);

        if($user){
            $this->getUserBlocked($user);

            if(is_null($user->password)){
                return $this->loginWithVerifyCode();
            }else{
                return $this->loginWithPassword();
            }

        }else{

            if(assignVerifyCode($mobile)){
                return apiResponse(array('redirectUrl'=>'/register'),['auth.ThisMobileNotRegister',[]],
                    'constants.apiResponseCodes.success');
            }else{
                return apiResponse('',['auth.SendSMSError',[]],
                    'constants.apiResponseCodes.error',500);
            }

        }
    }

    public function loginWithEmail($email){

        $user = User::where('email',$email)->first();

        if($user){
            $this->getUserBlocked($user);

            session(['email'=>$email]);
            $this->setLoginMethodSession('email');

            return apiResponse(array('redirectUrl'=>'/login'),['',[]],
                'constants.apiResponseCodes.success');


        }else{
            return apiResponse(false,['auth.ThisEmailNotRegisterPleaseEnterValidMobileForRegister',[]],
                'constants.apiResponseCodes.error',422);
        }
    }

    public function loginWithVerifyCode(){

        $this->setLoginMethodSession('mobile');

        if(assignVerifyCode(session('mobile'))){

            return apiResponse(array('redirectUrl' => '/login'),['',[]],
                'constants.apiResponseCodes.success');
        }else{
            return apiResponse('',['auth.SendSMSError',[]],
                'constants.apiResponseCodes.error',500);
        }


    }

    public function loginWithPassword(){

        $this->setLoginMethodSession('mobile');

        return apiResponse(array('redirectUrl' => '/login'),['',[]],
            'constants.apiResponseCodes.success');

    }

    public function setLoginMethodSession($method){
        session(['loginWith'=>'mobile']);
    }

    public function getUserBlocked($user){
        if($user->published === 0){
            return apiResponse(false,['auth.YourAccountSuspendedPleaseCallWithAdmin',[]],
                'constants.apiResponseCodes.error',403);
        }
    }

}
