<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    public function categoryPost($id)
    {
        $categories = BlogCategory::all();
        $category = BlogCategory::find($id);
        $posts = $category->posts()->with('categories')->orderBy('created_at','desc')->paginate(16);
        return view('blog.blog',compact('posts','categories'));
    }
}
