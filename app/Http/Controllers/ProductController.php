<?php

namespace App\Http\Controllers;

use App\Classes\PriceFilter;
use App\Filters\Front\ProductFilter;
use App\Http\Requests\ProductComment\StoreProductComment;
use App\Models\Category;
use App\Models\PriceProduct;
use App\Models\Product;
use App\Models\ProductComment;
use App\Models\ProductProperty;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProductController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param $slug
     * @return Factory|View|array
     */
    public function show($slug)
    {

        $siteConfig = getSiteConfigs();
        $priceMode = $siteConfig->priceMode;
        $categories = Category::where('published', 1)->get();

        $filters = new \stdClass();
        $filters->Slug = $slug;
        $filters->today = Carbon::now()->format('Y-m-d h:m:s');

        $product = ProductFilter::filter($filters);
        $rating = $commentRateAvg = $product->comments->avg('rating') + 0;
        $product = $product->toArray();

        $product['related_products'] = productRelatedFrontend($product['related_products'], $priceMode);
        $product['features'] = productFeaturesRebuildOnFrontEnd($product['feature_value']);
        $product['highlightFeatures'] = productHighlightFeatures($product['features']);
        Product::where('slug', $slug)->increment('hits');

        if ($product['type'] === 'simple') {

            $product = PriceFilter::applyProductDiscountInCategoryList($product, $priceMode);
            return view('product', compact('product', 'rating'));
        } else {

            $properties = productPropertiesRebuildOnFrontEnd([$product['properties']], false);
            $product = PriceFilter::applyPropertyDiscountInSingleProduct($product, $properties['defaultProductProperty'], $priceMode);
            return view('product', compact('product', 'categories', 'properties', 'rating'));
        }
    }

    public function showCategory($slug)
    {
        $categories = Category::where('published', 1)->with(['langs'])->get();
        $category = Category::where('slug', $slug)->first();
        $products = Product::with([
            'langs'=>function ($query){$query->where('lang_id',app()->getLocale());},
            'images'=>function ($query){$query->where('is_main',1);}])->where('category_id', $category->id)->where('published', 1)->paginate(3);
        return view('product.products',compact('products','categories'));
    }

}
