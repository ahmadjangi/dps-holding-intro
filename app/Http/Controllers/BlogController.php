<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use App\Models\BlogPostComment;

use App\Models\Category;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function blog()
    {
        $posts = BlogPost::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'categories'])->orderBy('created_at','desc')->where('published',1)->paginate(10);;
        $categories = BlogCategory::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());}])->where('published',1)->get();
        $recentPosts = BlogPost::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'categories'])->orderBy('created_at','desc')->where('published',1)->take(6)->get();
        $category = null;
        return view('blog.blog',compact('posts','category','categories','recentPosts'));
    }

    public function blogCategory($slug)
    {
        $categories = BlogCategory::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());}])->where('published',1)->get();
        $recentPosts = BlogPost::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'categories'])->orderBy('created_at','desc')->where('published',1)->take(6)->get();
        $category = BlogCategory::where('slug',$slug)->first();
        $posts = $category->posts(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'categories'])->orderBy('created_at','desc')->where('published',1)->paginate(8);
        return view('blog.blog',compact('posts','category','categories','recentPosts'));
    }

    public function show($id)
    {
        $categories = BlogCategory::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());}])->where('published',1)->get();
        $recentPosts = BlogPost::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'categories'])->orderBy('created_at','desc')->where('published',1)->take(6)->get();
        $post = BlogPost::with(['langs'=>function ($query){$query->where('lang_id',app()->getLocale());},'categories'])->whereSlug($id)->first();
        $category = $post->categories[0];
        return view('blog.singlePost',compact('post','categories','recentPosts','category'));
    }

    public function blogSearch(Request $request)
    {

        $categories = BlogCategory::where('published',1);
        $recentPosts = BlogPost::with('categories')->orderBy('created_at','desc')->where('published',1)->take(6)->get();
        $posts = BlogPost::where('title','like','%' . $request->get('term') . '%')->with('categories')->orderBy('created_at','desc')->where('published',1)->paginate(10);
        $category = null;
        return view('blog.blog',compact('posts','categories','recentPosts','category'));

    }

}
