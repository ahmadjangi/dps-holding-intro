<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubsetCompany\SubsetCompanys;
use App\Http\Requests\SubsetCompany\PublishSubsetCompany;
use App\Http\Requests\SubsetCompany\StoreSubsetCompany;
use App\Http\Requests\SubsetCompany\UpdateSubsetCompany;
use App\Services\SubsetCompanyService;
use Illuminate\Http\JsonResponse;

class SubsetCompanyController extends Controller
{

    protected SubsetCompanyService $subsetCompany;

    public function __construct(SubsetCompanyService $subsetCompany){
        $this->subsetCompany = $subsetCompany;
    }

    /**
     * Display a listing of the resource.
     *
     * @param SubsetCompanys $request
     * @return JsonResponse
     */
    public function index(SubsetCompanys $request): JsonResponse
    {
        $all = $this->subsetCompany->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSubsetCompany $request
     * @return JsonResponse
     */
    public function store(StoreSubsetCompany $request): JsonResponse
    {
        $all = $this->subsetCompany->store($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $subsetCompany = $this->subsetCompany->show($id);
        return apiResponse($subsetCompany,['',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSubsetCompany $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateSubsetCompany $request, $id): JsonResponse
    {
        $this->subsetCompany->update($request, $id);
        return apiResponse(null,['banner.BannerEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $this->subsetCompany->delete($id);
        return apiResponse(null,['banner.BannerDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishSubsetCompany $request
     * @return JsonResponse
     */
    public function publish(PublishSubsetCompany $request): JsonResponse
    {
        $this->subsetCompany->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
