<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FaqCategory\FaqCategories;
use App\Http\Requests\FaqCategory\PublishFaqCategory;
use App\Http\Requests\FaqCategory\StoreFaqCategory;
use App\Http\Requests\FaqCategory\UpdateFaqCategory;
use App\Models\FaqCategory;
use App\Services\FaqCategoryService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class FaqsCategoriesController extends Controller
{
    protected FaqCategoryService $faqCategory;
    public function __construct(FaqCategoryService $faqCategory)
    {
        $this->faqCategory = $faqCategory;
    }

    /**
     * Display a listing of the resource.
     *
     * @param FaqCategories $request
     * @return JsonResponse
     */
    public function index(FaqCategories $request): JsonResponse
    {
        $all = $this->faqCategory->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFaqCategory $request
     * @return JsonResponse
     */
    public function store(StoreFaqCategory $request): JsonResponse
    {
        $id = $this->faqCategory->store($request);
        return apiResponse($id,['faq.FaqCategoryCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $faqCategory = $this->faqCategory->show($id);
        return apiResponse($faqCategory,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFaqCategory $request
     * @param $faq
     * @return JsonResponse
     */
    public function update(UpdateFaqCategory $request, $faq)
    {
        $this->faqCategory->update($request, $faq);
        return apiResponse(null,['faq.FAQCategoryEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $faqs
     * @return JsonResponse
     */
    public function destroy($faqs)
    {
        $count = $this->faqCategory->delete($faqs);

        return apiResponse(null,$count > 0 ? ['faq.FaqCategoryDeletedSuccessful',['count'=>$count]] : ['faq.ThisFaqCategoryHasTransaction',[]],
            'constants.apiResponseCodes.success',$count > 0 ? 200 : 409);
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishFaqCategory $request
     * @return JsonResponse
     */
    public function publish(PublishFaqCategory $request)
    {
        $this->faqCategory->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
