<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Client\Clients;
use App\Http\Requests\Client\PublishClient;
use App\Http\Requests\Client\StoreClient;
use App\Http\Requests\Client\UpdateClient;
use App\Services\ClientService;
use Illuminate\Http\JsonResponse;

class ClientController extends Controller
{

    protected ClientService $client;

    public function __construct(ClientService $client){
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Clients $request
     * @return JsonResponse
     */
    public function index(Clients $request): JsonResponse
    {
        $all = $this->client->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreClient $request
     * @return JsonResponse
     */
    public function store(StoreClient $request)
    {
        $all = $this->client->store($request);
        return apiResponse($all,['client.ClientStoredSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $client = $this->client->show($id);
        return apiResponse($client,['',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateClient $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateClient $request, $id)
    {
        $this->client->update($request, $id);
        return apiResponse(null,['client.ClientEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->client->delete($id);
        return apiResponse(null,['client.ClientDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishClient $request
     * @return JsonResponse
     */
    public function publish(PublishClient $request)
    {
        $this->client->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
