<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Config\StoreConfig;
use App\Http\Requests\Config\UpdateConfig;
use App\Services\ConfigService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    protected ConfigService $config;

    public function __construct(ConfigService $config)
    {
        $this->config = $config;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $all = $this->config->index();
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreConfig  $request
     * @return JsonResponse
     */
    public function store(StoreConfig $request)
    {
        $this->config->store($request);
        return apiResponse(null,['config.ConfigCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UpdateConfig $request
     * @return JsonResponse
     */
    public function update(UpdateConfig $request)
    {
        $this->config->update($request);
        return apiResponse(null,['config.ConfigUpdatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }
}
