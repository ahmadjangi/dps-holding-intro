<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Role\PublishRole;
use App\Http\Requests\Role\Roles;
use App\Http\Requests\Role\StoreRole;
use App\Http\Requests\Role\UpdateRole;
use App\Services\RoleService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private RoleService $role;

    public function __construct(RoleService $role)
    {
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Roles $request
     * @return JsonResponse
     */
    public function index(Roles $request)
    {
        $all = $this->role->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRole $request
     * @return JsonResponse
     */
    public function store(StoreRole $request)
    {
        $id = $this->role->store($request);
        return apiResponse($id,['role.RoleCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $role = $this->role->show($id);
        return apiResponse($role,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRole $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateRole $request, $id)
    {
        $this->role->update($request, $id);
        return apiResponse(null,['role.RoleEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->role->delete($id);
        return apiResponse(null,['role.RoleDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Publish the specified resource.
     *
     * @param PublishRole $request
     * @return JsonResponse
     */
    public function publish(PublishRole $request)
    {
        $this->role->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
