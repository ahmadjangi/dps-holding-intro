<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Slide\ArrangeSlide;
use App\Http\Requests\Upload\BannerImage;
use App\Http\Requests\Upload\BlogPostImage;
use App\Http\Requests\Upload\CatalogFile;
use App\Http\Requests\Upload\ClientImage;
use App\Http\Requests\Upload\GeneralImage;
use App\Http\Requests\Upload\HonorImage;
use App\Http\Requests\Upload\ProductImage;
use App\Http\Requests\Upload\CategoryImage;
use App\Http\Requests\Upload\ProjectImage;
use App\Http\Requests\Upload\ServiceImage;
use App\Services\UploadService;
use App\Http\Controllers\Controller;


class UploadController extends Controller
{
    protected UploadService $upload;

    public function __construct(UploadService $upload)
    {
        $this->upload = $upload;
    }

    public function subsetCompanyImage(ClientImage $request): string
    {

        return $this->upload->subsetCompanyImage($request);
    }
    public function clientImage(ClientImage $request): string
    {

        return $this->upload->clientImage($request);
    }

    public function serviceImage(ServiceImage $request): string
    {

        return $this->upload->serviceImage($request);
    }

    public function projectImage(ProjectImage $request): string
    {

        return $this->upload->projectImage($request);
    }
    public function honorImage(HonorImage $request): string
    {
        return $this->upload->honorImage($request);
    }

    public function productImage(ProductImage $request){

        return $this->upload->productImage($request);
    }

    public function generalImage(GeneralImage $request){

        $result = $this->upload->generalImage($request);
        return apiResponse($result,
            $result !== null ? ['general.ImageUploadedSuccessful',[]] : ['general.ImageUploadError',[]],
            $result !== null? 'constant.apiResponseCode.success' : 'constant.apiResponseCode.error',
            $result !== null ? 200 : 500);
    }

    public function alternativeLogo(GeneralImage $request){

        $result = $this->upload->alternativeLogo($request);
        return apiResponse($result,
            $result !== null ? ['general.ImageUploadedSuccessful',[]] : ['general.ImageUploadError',[]],
            $result !== null? 'constant.apiResponseCode.success' : 'constant.apiResponseCode.error',
            $result !== null ? 200 : 500);
    }

    public function aboutUsImage(GeneralImage $request){

        $result = $this->upload->aboutUsImage($request);
        return apiResponse($result,
            $result !== null ? ['general.ImageUploadedSuccessful',[]] : ['general.ImageUploadError',[]],
            $result !== null? 'constant.apiResponseCode.success' : 'constant.apiResponseCode.error',
            $result !== null ? 200 : 500);
    }

    public function slideImage(GeneralImage $request){

        return $result = $this->upload->slideImage($request);
    }

    public function blogPostImage(BlogPostImage $request){

        return $this->upload->BlogPostImage($request);
    }

    public function uploadImage(GeneralImage $request){

        return $this->upload->uploadImage($request);
    }

    public function bannerImage(BannerImage $request){

        return $this->upload->bannerImage($request);
    }

    public function catalogFile(CatalogFile $request){

        return $this->upload->catalogFile($request);
    }

    public function slides(){
        return $this->upload->Slides();
    }

    public function deleteSlides($id){
        return $this->upload->deleteSlides($id);
    }

    /**
     * Publish the specified resource.
     *
     * @param ArrangeSlide $request
     * @return JsonResponse
     */
    public function arrange(ArrangeSlide $request)
    {
        $result = $this->upload->arrange($request);
        return apiResponse($result,['category.CategorySortedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

}
