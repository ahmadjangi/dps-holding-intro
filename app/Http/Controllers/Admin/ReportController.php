<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    public function salesReport()
    {
        return array('daily'=>self::dailySales(),'weekly'=>self::weeklySales(),'monthly'=>self::monthlySales());
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function dailySales()
    {
        $today = verta();
        $firstDayOfWeek = $today->startWeek()->formatGregorian('Y-m-d');
        $endDayOfWeek = $today->endWeek()->formatGregorian('Y-m-d');;

        return Order::where('order_state_id',3)
            ->whereBetween('created_at',[$firstDayOfWeek,$endDayOfWeek])
            ->orderBy('daily')
            ->groupBy('daily')
            ->get([DB::raw('DATE(created_at) as daily'), DB::raw('SUM(total_amount) AS sale_amount')]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function weeklySales()
    {
        $today = verta();
        $firstDayOfMonth = $today->startMonth()->formatGregorian('Y-m-d');
        $endDayOfMonth = $today->endMonth()->formatGregorian('Y-m-d');;

        return Order::where('order_state_id',3)
            ->whereBetween('created_at',[$firstDayOfMonth,$endDayOfMonth])
            ->orderBy('weekly')
            ->groupBy('weekly')
            ->get([DB::raw('WEEK(created_at) AS weekly'), DB::raw('SUM(total_amount) AS sale_amount')]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function monthlySales()
    {
        $today = verta();
        $firstDayOfYear = $today->startYear()->formatGregorian('Y-m-d');
        $endDayOfYear = $today->endYear()->formatGregorian('Y-m-d');;

        return Order::where('order_state_id',3)
            ->whereBetween('created_at',[$firstDayOfYear,$endDayOfYear])
            ->orderBy('monthly')
            ->groupBy('monthly')
            ->get([DB::raw('MONTH(created_at) AS monthly'), DB::raw('SUM(total_amount) AS sale_amount')]);
    }

}
