<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Honor\Honors;
use App\Http\Requests\Honor\PublishHonor;
use App\Http\Requests\Honor\StoreHonor;
use App\Http\Requests\Honor\UpdateHonor;
use App\Services\HonorService;
use Illuminate\Http\JsonResponse;

class HonorController extends Controller
{
    protected HonorService $honor;

    public function __construct(HonorService $honor){
        $this->honor = $honor;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Honors $request
     * @return JsonResponse
     */
    public function index(Honors $request): JsonResponse
    {
        $all = $this->honor->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreHonor $request
     * @return JsonResponse
     */
    public function store(StoreHonor $request): JsonResponse
    {
        $all = $this->honor->store($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $honor = $this->honor->show($id);
        return apiResponse($honor,['',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateHonor $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateHonor $request, $id): JsonResponse
    {
        $this->honor->update($request, $id);
        return apiResponse(null,['banner.BannerEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $this->honor->delete($id);
        return apiResponse(null,['banner.BannerDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishHonor $request
     * @return JsonResponse
     */
    public function publish(PublishHonor $request): JsonResponse
    {
        $this->honor->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
