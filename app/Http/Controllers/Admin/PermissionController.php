<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Permission\Permissions;
use App\Http\Requests\Permission\UpdatePermission;
use App\Models\Permission;
use App\Models\Role;
use App\Services\PermissionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;

class PermissionController extends Controller
{
    private PermissionService $permission;

    public function __construct(PermissionService $permission)
    {
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Permissions $request
     * @return JsonResponse
     */
    public function index(Permissions $request)
    {
        $permission_ids = [];

        foreach (Route::getRoutes()->getRoutes() as $key => $route)
        {
            // get route action
            $action = $route->getActionname();
            // separating controller and method
            $_action = explode('@',$action);

            $controller = $_action[0];
            $method = end($_action);

            // check if this permission is already exists
            $permission_check = Permission::where(
             ['controller'=>$controller,'method'=>$method]
            )->first();

         if(!$permission_check){
             $permission = new Permission();
             $permission->controller = $controller;
             $permission->name = $this->methodName($method);
             $permission->method = $method;
             $permission->save();
             // add stored permission id in array
             $permission_ids[] = $permission->id;
         }
        }

            // find admin role.
            $admin_role = Role::where('name','admin')->first();
            // atache all permissions to admin role
            $admin_role->permissions()->attach($permission_ids);

        $all = $this->permission->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $permission = $this->permission->show($id);
        return apiResponse($permission,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePermission $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdatePermission $request, $id)
    {
        $this->permission->update($request,$id);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }

    public function methodName($method){
        switch ($method){
            case "index":
                return 'مشاهده لیست';
                break;
            case "create":
                return 'ایجاد';
                break;
            case "show":
                return 'نمایش';
                break;
            case "store":
                return 'ذخیره';
                break;
            case "edit":
                return 'نمایش برای ویرایش';
                break;
            case "update":
                return 'ویرایش';
                break;
            case "destroy":
                return 'حذف';
                break;
            case "publish":
                return 'انتشار';
                break;
        }
    }

}
