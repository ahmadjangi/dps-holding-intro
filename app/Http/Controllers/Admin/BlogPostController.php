<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlogPost\Posts;
use App\Http\Requests\BlogPost\PublishPost;
use App\Http\Requests\BlogPost\StorePost;
use App\Http\Requests\BlogPost\UpdatePost;
use App\Http\Requests\Upload\BlogPostImage;
use App\Services\BlogPostService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BlogPostController extends Controller
{
    protected BlogPostService $post;

    public function __construct(BlogPostService $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.1
     *
     * @param Posts $request
     * @return JsonResponse
     */
    public function index(Posts $request)
    {
        $all = $this->post->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePost  $request
     * @return JsonResponse
     */
    public function store(StorePost $request)
    {
        $id = $this->post->store($request);
        return apiResponse($id,['post.PostCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param $post
     * @return JsonResponse
     */
    public function show($post)
    {
        $post = $this->post->show($post);
        return apiResponse($post,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePost $request
     * @param $post
     * @return JsonResponse
     */
    public function update(UpdatePost $request, $post)
    {
        $this->post->update($request, $post);
        return apiResponse(null,['post.PostEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $posts
     * @return JsonResponse
     */
    public function destroy($posts)
    {
        $result = $this->post->delete($posts);
        return apiResponse($result,$result == true ? ['post.PostDeletedSuccessful',[]] : ['post.ThisPostHasTransaction',[]],
            'constants.apiResponseCodes.success',$result == true ? 200 : 409);
    }


    /**
     * Publish the specified resource.
     *
     * @param PublishPost $request
     * @return JsonResponse
     */
    public function publish(PublishPost $request)
    {
        $this->post->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }

    public function uploadImage(BlogPostImage $request)
    {
        $ImageUrl = $this->post->uploadImage($request);
        return apiResponse($ImageUrl,['post.PostImageUploadedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }
}
