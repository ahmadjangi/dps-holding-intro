<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\Projects;
use App\Http\Requests\Project\PublishProject;
use App\Http\Requests\Project\StoreProject;
use App\Http\Requests\Project\UpdateProject;
use App\Services\ProjectService;
use Illuminate\Http\JsonResponse;

class ProjectController extends Controller
{

    protected ProjectService $project;

    public function __construct(ProjectService $project){
        $this->project = $project;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Projects $request
     * @return JsonResponse
     */
    public function index(Projects $request): JsonResponse
    {
        $all = $this->project->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProject $request
     * @return JsonResponse
     */
    public function store(StoreProject $request)
    {
        $all = $this->project->store($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $project = $this->project->show($id);
        return apiResponse($project,['',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProject $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateProject $request, $id)
    {
        $this->project->update($request, $id);
        return apiResponse(null,['banner.BannerEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->project->delete($id);
        return apiResponse(null,['banner.BannerDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishProject $request
     * @return JsonResponse
     */
    public function publish(PublishProject $request)
    {
        $this->project->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
