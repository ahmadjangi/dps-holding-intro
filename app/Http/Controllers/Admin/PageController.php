<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Page\Pages;
use App\Http\Requests\Page\PublishPage;
use App\Http\Requests\Page\StorePage;
use App\Http\Requests\Page\UpdatePage;
use App\Services\PageService;
use Illuminate\Http\JsonResponse;

class PageController extends Controller
{

    protected PageService $page;

    public function __construct(PageService $page){
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Pages $request
     * @return JsonResponse
     */
    public function index(Pages $request): JsonResponse
    {
        $all = $this->page->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePage $request
     * @return JsonResponse
     */
    public function store(StorePage $request)
    {
        $all = $this->page->store($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $page = $this->page->show($id);
        return apiResponse($page,['',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePage $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdatePage $request, $id)
    {
        $this->page->update($request, $id);
        return apiResponse(null,['banner.BannerEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->page->delete($id);
        return apiResponse(null,['banner.BannerDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishPage $request
     * @return JsonResponse
     */
    public function publish(PublishPage $request)
    {
        $this->page->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
