<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlogCategory\Categories;
use App\Http\Requests\BlogCategory\PublishCategory;
use App\Http\Requests\BlogCategory\StoreCategory;
use App\Http\Requests\BlogCategory\UpdateCategory;
use App\Services\BlogCategoryService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BlogCategoryController extends Controller
{
    protected BlogCategoryService $blogCategory;

    public function __construct(BlogCategoryService $blogCategory)
    {
        $this->blogCategory = $blogCategory;
    }

    /**
     * Display a listing of the resource.1
     *
     * @param Categories $request
     * @return JsonResponse
     */
    public function index(Categories $request)
    {
        $all = $this->blogCategory->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategory $request
     * @return JsonResponse
     */
    public function store(StoreCategory $request)
    {
        $id = $this->blogCategory->store($request);
        return apiResponse($id,['blogCategory.BlogCategoryCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param $blogCategory
     * @return JsonResponse
     */
    public function show($blogCategory)
    {
        $blogCategory = $this->blogCategory->show($blogCategory);
        return apiResponse($blogCategory,['',[]],
            'constants.apiResponseCodes.success');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategory $request
     * @param $blogCategory
     * @return JsonResponse
     */
    public function update(UpdateCategory $request, $blogCategory)
    {
        $this->blogCategory->update($request, $blogCategory);
        return apiResponse(null,['blogCategory.BlogCategoryEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $categories
     * @return JsonResponse
     */
    public function destroy($categories)
    {
        $result = $this->blogCategory->delete($categories);
        return apiResponse($result,$result === true ? ['blogCategory.BlogCategoryDeletedSuccessful',[]] : ['blogCategory.ThisBlogCategoryHasTransaction',[]],
            'constants.apiResponseCodes.success',$result === true ? 200 : 409);
    }


    /**
     * Publish the specified resource.
     *
     * @param PublishCategory $request
     * @return JsonResponse
     */
    public function publish(PublishCategory $request)
    {
        $this->blogCategory->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }

}
