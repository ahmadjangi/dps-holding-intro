<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\User\PublishUser;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateUser;
use App\Http\Requests\User\Users;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;


class UserController extends Controller
{
    protected UserService $user;


    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Users $request
     * @return JsonResponse
     */
    public function index(Users $request)
    {
        $all = $this->user->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUser $request
     * @return JsonResponse
     */
    public function store(StoreUser $request)
    {
        $id = $this->user->store($request);
        return apiResponse($id,['user.UserCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $user = $this->user->show($id);
        return apiResponse($user,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUser $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateUser $request, $id)
    {
        $this->user->update($request, $id);
        return apiResponse(null,['user.UserEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $result = $this->user->delete($id);
        return apiResponse(null,['user.UserDeletedSuccessful',['count' =>$result]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param  PublishUser  $request
     * @return JsonResponse
     */
    public function publish(PublishUser $request)
    {
        $this->user->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
