<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Category\ArrangeCategory;
use App\Http\Requests\Category\Categories;
use App\Http\Requests\Category\PublishCategory;
use App\Http\Requests\Category\UpdateCategory;
use App\Http\Requests\Category\StoreCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ArrangeProduct;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected CategoryService $category;

    public function __construct(CategoryService $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Categories $request
     * @return JsonResponse
     */
    public function index(Categories $request)
    {
        $all = $this->category->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCategory  $request
     * @return JsonResponse
     */
    public function store(StoreCategory $request)
    {
        $result = $this->category->store($request);

        if($result['status'] === false){
            return apiResponse($result['data'],['CategoryCreatedError',[]],
                'constants.apiResponseCodes.error',500);
        }else{
            return apiResponse($result['data'],['CategoryCreatedSuccessful',[]],
                'constants.apiResponseCodes.success');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param $category
     * @return JsonResponse
     */
    public function show($category)
    {
        $category = $this->category->show($category);
        return apiResponse($category,['',[]],
            'constants.apiResponseCodes.success');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategory $request
     * @param $category
     * @return JsonResponse
     */
    public function update(UpdateCategory $request, $category)
    {
        $result = $this->category->update($request, $category);

        if($result['status'] === false){
            return apiResponse($result['data'],['category.CategoryEditedError',[]],
                'constants.apiResponseCodes.error',500);
        }else{
            return apiResponse($result['data'],['category.CategoryEditedSuccessful',[]],
                'constants.apiResponseCodes.success');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $categories
     * @return JsonResponse
     */
    public function destroy($categories)
    {
        $this->category->delete($categories);
        return apiResponse(null,['category.CategoryDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Publish the specified resource.
     *
     * @param PublishCategory $request
     * @return JsonResponse
     */
    public function publish(PublishCategory $request)
    {
        $this->category->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
