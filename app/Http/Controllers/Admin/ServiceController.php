<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Service\Services;
use App\Http\Requests\Service\PublishService;
use App\Http\Requests\Service\StoreService;
use App\Http\Requests\Service\UpdateService;
use App\Services\ServiceService;
use Illuminate\Http\JsonResponse;

class ServiceController extends Controller
{

    protected ServiceService $service;

    public function __construct(ServiceService $service){
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Services $request
     * @return JsonResponse
     */
    public function index(Services $request)
    {
        $all = $this->service->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreService $request
     * @return JsonResponse
     */
    public function store(StoreService $request)
    {

        $all = $this->service->store($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $service = $this->service->show($id);
        return apiResponse($service,['',[]],
            'constants.apiResponseCodes.success');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateService $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateService $request, $id)
    {
        $this->service->update($request, $id);
        return apiResponse(null,['banner.BannerEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->service->delete($id);
        return apiResponse(null,['banner.BannerDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishService $request
     * @return JsonResponse
     */
    public function publish(PublishService $request)
    {
        $this->service->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
