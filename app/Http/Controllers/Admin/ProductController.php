<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Product\ArrangeProduct;
use App\Http\Requests\Product\Products;
use App\Http\Requests\Product\PublishProduct;
use App\Http\Requests\Product\StorePrice;
use App\Http\Requests\Product\StoreProduct;
use App\Http\Requests\Product\UpdateProduct;
use App\Http\Requests\Product\UpdateProductQuntity;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;


class ProductController extends Controller
{
    protected ProductService $product;

    public function __construct(ProductService $product){
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Products $request
     * @return JsonResponse
     */
    public function index(Products $request)
    {
        $all = $this->product->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProduct $request
     * @return JsonResponse
     */
    public function store(StoreProduct $request)
    {
        $result = $this->product->store($request);

        if($result['status'] === false){
            return apiResponse($result['data'],['product.ProductCreatedError',[]],
                'constants.apiResponseCodes.error',500);
        }else{
            return apiResponse($result['data'],['product.ProductCreatedSuccessful',[]],
                'constants.apiResponseCodes.success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $product = $this->product->show($id);
        return apiResponse($product,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProduct $request
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateProduct $request, $id)
    {
        $result = $this->product->update($request, $id);

        if($result['status'] === false){
            return apiResponse($result['data'],['product.ProductEditedError',[]],
                'constants.apiResponseCodes.error',500);
        }else{
            return apiResponse($result['data'],['product.ProductEditedSuccessful',[]],
                'constants.apiResponseCodes.success');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $products
     * @return JsonResponse
     */
    public function destroy($products)
    {
        $result = $this->product->delete($products);
        return apiResponse($result,['product.ProductDeletedSuccessful',[]],
            'constants.apiResponseCodes.success',200);
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishProduct $request
     * @return JsonResponse
     */
    public function publish(PublishProduct $request)
    {
        $this->product->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Delete the product image in database.
     *
     * @param  int $imageId
     * @return JsonResponse
     */
    public function destroyImage($imageId){

        $result = $this->product->deleteImage($imageId);
        if($result['status'] === false){
            return apiResponse($result['data'],['product.ProductImageDeletedError',[]],
                'constants.apiResponseCodes.error',500);
        }else{
            return apiResponse($result['data'],['product.ProductImageDeletedSuccessful',[]],
                'constants.apiResponseCodes.success');
        }

    }


    /**
     * Delete the product feature in database.
     *
     * @param $productId
     * @param int $featureId
     * @return JsonResponse
     */
    public function destroyFeature($productId,$featureId){

        $this->product->deleteFeature($productId,$featureId);
        return apiResponse(null,['product.ProductFeatureDeletedSuccessful',[]],'constant.apiResponseCode.success');

    }

    /**
     * Publish the specified resource.
     *
     * @param ArrangeProduct $request
     * @return JsonResponse
     */
    public function arrange(ArrangeProduct $request)
    {
        $result = $this->product->arrange($request);
        return apiResponse($result,['category.CategorySortedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

}
