<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\Banners;
use App\Http\Requests\Banner\PublishBanner;
use App\Http\Requests\Banner\StoreBanner;
use App\Http\Requests\Banner\UpdateBanner;
use App\Services\BannerService;
use Illuminate\Http\JsonResponse;

class BannerController extends Controller
{
    protected BannerService $banner;

    public function __construct(BannerService $banner){
        $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Banners $request
     * @return JsonResponse
     */
    public function index(Banners $request)
    {
        $all = $this->banner->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBanner $request
     * @return JsonResponse
     */
    public function store(StoreBanner $request)
    {
        $id = $this->banner->store($request);
        return apiResponse($id,['banner.BannerCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $banner = $this->banner->show($id);
        return apiResponse($banner,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBanner $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateBanner $request, $id)
    {

        return apiResponse($this->banner->update($request, $id),['banner.BannerEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $banners
     * @return JsonResponse
     */
    public function destroy($banners)
    {
        $this->banner->delete($banners);
        return apiResponse(null,['banner.BannerDeletedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishBanner $request
     * @return JsonResponse
     */
    public function publish(PublishBanner $request)
    {
        $this->banner->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
