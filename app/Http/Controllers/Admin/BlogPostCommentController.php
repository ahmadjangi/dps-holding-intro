<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlogPostComment\PostComments;
use App\Http\Requests\BlogPostComment\PublishPostComment;
use App\Http\Requests\BlogPostComment\StorePostComment;
use App\Http\Requests\BlogPostComment\UpdatePostComment;
use App\Services\PostCommentService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BlogPostCommentController extends Controller
{
    protected PostCommentService $postComment;

    public function __construct(PostCommentService $postComment)
    {
        $this->postComment = $postComment;
    }

    /**
     * Display a listing of the resource.
     *
     * @param PostComments $request
     * @return JsonResponse
     */
    public function index(PostComments $request)
    {
        $all = $this->postComment->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePostComment $request
     * @return JsonResponse
     */
    public function store(StorePostComment $request)
    {
        $id = $this->postComment->store($request);
        return apiResponse($id,['blogPost.BlogPostCommentCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param $postComment
     * @return JsonResponse
     */
    public function show($postComment)
    {
        $category = $this->postComment->show($postComment);
        return apiResponse($category,'',
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePostComment $request
     * @param $postComment
     * @return JsonResponse
     */
    public function update(UpdatePostComment $request, $postComment)
    {
        $this->postComment->update($request, $postComment);
        return apiResponse(null,'category.CategoryEditedSuccessful',
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $postComment
     * @return JsonResponse
     */
    public function destroy($postComment)
    {
        $this->postComment->delete($postComment);
        return apiResponse(null,'category.CategoryDeletedSuccessful',
            'constants.apiResponseCodes.success');
    }


    /**
     * Publish the specified resource.
     *
     * @param PublishPostComment $request
     * @return JsonResponse
     */
    public function confirm(PublishPostComment $request)
    {
        $this->postComment->confirm($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
