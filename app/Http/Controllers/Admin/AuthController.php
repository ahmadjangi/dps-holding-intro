<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Login API
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request){

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => 1])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('LaraPassport')->accessToken;
            $success['access'] =  true;
            $success['user'] =  $user;
            return apiResponse($success,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
        } else {
            return apiResponse(null,['user.LoginFailed',[]],'constants.apiResponseCodes.ValidationException',401);
        }
    }

    /**
     * Register API
     *
     * @param Request $request
     * @return JsonResponse
     */
//    public function register(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'first_name' => 'required',
//            'last_name' => 'required',
//            'mobile' => 'required',
//            'published' => 'required',
//            'email' => 'required|email',
//            'password' => 'required',
//            'c_password' => 'required|same:password',
//        ]);
//        if ($validator->fails()) {
//            return response()->json(['error'=>$validator->errors()]);
//        }
//        $postArray = $request->all();
//        $postArray['password'] = bcrypt($postArray['password']);
//        $user = User::create($postArray);
//        $success['token'] =  $user->createToken('LaraPassport')->accessToken;
//        $success['name'] =  $user->name;
//        return response()->json([
//            'status' => 'success',
//            'data' => $success,
//        ]);
//    }

}
