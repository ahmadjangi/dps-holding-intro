<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Faq\Faqs;
use App\Http\Requests\Faq\PublishFaq;
use App\Http\Requests\Faq\StoreFaq;
use App\Http\Requests\Faq\UpdateFaq;
use App\Models\Faq;
use App\Services\FaqService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    protected FaqService $faq;
    public function __construct(FaqService $faq)
    {
        $this->faq = $faq;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Faq $request
     * @return JsonResponse
     */
    public function index(Faqs $request): JsonResponse
    {
        $all = $this->faq->index($request);
        return apiResponse($all,['general.OperationSuccessful',[]],'constants.apiResponseCodes.success');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFaq $request
     * @return JsonResponse
     */
    public function store(StoreFaq $request): JsonResponse
    {
        $id = $this->faq->store($request);
        return apiResponse($id,['faq.FaqCreatedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Display the specified resource.
     *
     * @param $faq
     * @return JsonResponse
     */
    public function show($faq)
    {
        $faq = $this->faq->show($faq);
        return apiResponse($faq,['',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFaq $request
     * @param $faq
     * @return JsonResponse
     */
    public function update(UpdateFaq $request, $faq)
    {
        $this->faq->update($request, $faq);
        return apiResponse(null,['faq.FAQEditedSuccessful',[]],
            'constants.apiResponseCodes.success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $faqs
     * @return JsonResponse
     */
    public function destroy($faqs)
    {
        $result = $this->faq->delete($faqs);

        return apiResponse($result,$result === true ? ['faq.FaqDeletedSuccessful',[]] : ['faq.ThisFaqHasTransaction',[]],
            'constants.apiResponseCodes.success',$result === true ? 200 : 409);
    }

    /**
     * Publish the specified resource.
     *
     * @param PublishFaq $request
     * @return JsonResponse
     */
    public function publish(PublishFaq $request)
    {
        $this->faq->publish($request);
        return apiResponse(null,['',[]],
            'constants.apiResponseCodes.success');
    }
}
