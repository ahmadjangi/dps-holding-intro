<?php

namespace App\Http\Controllers;

use App\Http\Requests\Front\User\UpdateUser;
use App\Http\Requests\Front\Order\ReturnOrderStore;
use App\Http\Requests\UserAddress\StoreUserAddress;
use App\Models\Order;
use App\Models\OrderReturn;
use App\Models\OrderState;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserVerify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function personalInfo(){

        $user = Auth::user();
        $userAddress = $user->address()->get();
        return view('profile.personalInfo',compact('user'));
    }

    public function update(UpdateUser $request){

        if($request->id == Auth::id()){

            $user = User::find($request->id);

            $user->first_name   = $request->first_name;
            $user->last_name    = $request->last_name;
            $user->national_id  = $request->national_id;
            $user->card_number  = $request->card_number;
            $user->birthday     = $request->birthday;
            $user->job          = $request->job;
            if(!is_null($request->password)){
                $user->password     = Hash::make($request->password);
            }

            $user->save();

            return apiResponse(true,['user.UserUpdatedSuccessful',[]],
                'constants.apiResponseCodes.success');

        }else{
            return apiResponse(false,['user.YouDoNotHaveAccessToUpdateYourProfile',[]],
                'constants.apiResponseCodes.success',403);
        }
    }

    public function verifyMobile(Request $request){

        $mobile = $request->input('mobile');
        return assignVerifyCode($mobile);
    }

    public function updateMobile(Request $request){

        $mobile = $request->input('mobile');
        $verifyCode = $request->input('verifyCode');

        $codeVerify = UserVerify::where([
            ['mobile',$mobile],
            ['code',$verifyCode]
        ])->first();

        if($codeVerify) {
            $user = Auth::user();
            $user->mobile = $mobile;
            $user->save();
            return apiResponse(true,['user.YourMobileChangedSuccessfully',[]],
                'constants.apiResponseCodes.success');
        }else{
            return apiResponse(false,['user.MobileNumberNoteChanged',[]],
                'constants.apiResponseCodes.success',403);
        }
    }

    public function user(){
        return Auth::user();
    }

    public function showProfile(){

        $user = Auth::user();
        return view('profile.profile',compact('user'));
    }

}
