<?php

use App\Classes\PriceFilter;
use App\Classes\SendSMS;
use App\Models\FeatureGroup;
use App\Models\Product;
use App\Models\Property;
use App\Models\SiteConfig;
use App\Models\UserVerify;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Models\Category;


if(!function_exists('apiResponse')){
    function apiResponse($data,$message,$code,$statusCode = 200){
        return response()->json([
            'data' => $data,
            'status' => [
                'message'=>__($message[0],$message[1]),
                'code'=>Config::get($code)],
        ],$statusCode);
    }
}

if(!function_exists('SetMultiLanguageRecords')){
    function SetMultiLanguageRecords($languageRecord): array
    {
        $records = [];
        $counter = 0;
        $languages = ['fa','en'];

        foreach ($languages as $language){
            $records[$counter] = ['lang_id' => $language];
            foreach ($languageRecord as $fieldKey => $fieldValue){
                if(key_exists($language,$fieldValue)){
                    $records[$counter] = Arr::add($records[$counter],$fieldKey , $fieldValue[$language]) ;
                }else{
                    $records[$counter] = Arr::add($records[$counter],$fieldKey , $fieldValue[$languages[0]]) ;
                }
            }
            $counter++;
        }

        return $records;
    }
}

if(!function_exists('getMultiLanguageRecords')){
    function getMultiLanguageRecords($languageRecord,$columns): array
    {
        $records =  [];
        foreach ($languageRecord['langs'] as $langItem){
            foreach ($columns as $column){
                $records[$column][$langItem['lang_id']] = $langItem[$column];
            }
        }
        $languageRecord['langs'] = $records;
        return $languageRecord;
    }
}

if(!function_exists('productPropertiesRebuild')) {

    function productPropertiesRebuild($property){

        $values = [];
        $propertyC = -1;
        $tmp = -1;
        $propertyValueC = 0;

        foreach ($property['property_value'] as $propertyValue){

            $propertyValueName = Property::find($propertyValue['property_id'])->name;

            if($propertyValue['property_id'] !== $tmp){

                $propertyC++;
                $values[$propertyC] = [
                    'id'=>$propertyValue['property_id'],
                    'name'=>$propertyValueName,
                    'values'=>[[
                        'id'=>$propertyValue['id'],
                        'value'=>$propertyValue['value']]
                    ]];
                $tmp = $propertyValue['property_id'];
                $propertyValueC = 0;

            }else{
                $propertyValueC++;
                $values[$propertyC]['values'][$propertyValueC] = [
                    'id'=>$propertyValue['id'],
                    'value'=>$propertyValue['value']];
            }
        }

        return $values;
    }//EOF ProductProperty
}// EOF ProductProperty Rebuild

if(!function_exists('productFeaturesRebuild')) {

    function productFeaturesRebuild($features){

        $values = [];
        $featureC = -1;
        $tmp = -1;
        $featureValueC = 0;
        $featureValueName = '';


        foreach ($features as $feature){

            foreach ($feature['values'] as $value){
                if($feature['pivot']['feature_value_id'] === $value['id'] ){
                    $featureValueName = $value['name'];
                }
            }

            if($feature['id'] !== $tmp){

                $featureC++;
                $values[$featureC] = [
                    'id'=>$feature['id'],
                    'name'=>$feature['name'],
                    'values'=>[[
                        'id'=>$feature['pivot']['feature_value_id'],
                        'name'=>$featureValueName]
                    ]];
                $tmp = $feature['id'];
                $featureValueC = 0;

            }else{
                $featureValueC++;
                $values[$featureC]['values'][$featureValueC] = [
                    'id'=>$feature['pivot']['feature_value_id'],
                    'name'=>$featureValueName];
            }
        }

        return $values;
    }//EOF ProductProperty
}// EOF ProductFeature Rebuild

if(!function_exists('productFeaturesRebuildOnFrontEndV1')) {

    function productFeaturesRebuildOnFrontEndV1($features){

        $rebuildFeatures = [];
        $index = -1;
        $featureIndex = -1;

        foreach ($features as $feature) {
            foreach ($rebuildFeatures as $key => $rebuildFeature){
                $index = -1;
                if($rebuildFeature['id'] === $feature['feature_group_id']){
                    $index = $key;
                    break;
                }
            }

            if ($index === -1) {
                $name = FeatureGroup::where('id', $feature['feature_group_id'])->first()->name;
                $rebuildFeatures [] = [
                    'id' => $feature['feature_group_id'],
                    'name' => $name,
                    'features' => [[
                        'id' => $feature['id'],
                        'name' => $feature['name'],
                        'highlight' => $feature['highlight'],
                        'values' => $feature['values']
                    ]]
                ];
            } else {

                $counter = 0;
                foreach ($rebuildFeatures[$index]['features'] as $item) {
                    $featureIndex = -1;
                    $counter++;

                    if ($item['id'] === $feature['id']) {

                        $featureIndex = $item;
                        break;
                    }
                }

                if ($featureIndex === -1) {
                    $rebuildFeatures[$index]['features'] [] = [
                        'id' => $feature['id'],
                        'name' => $feature['name'],
                        'highlight' => $feature['highlight'],
                        'values' => $feature['values']
                    ];
                }
            }

        }

        return $rebuildFeatures;
    }
}

if(!function_exists('productPropertiesRebuildOnFrontEnd')) {
    function productPropertiesRebuildOnFrontEnd($property,$forFilter){


        $values = [];
        $propertyC = -1;
        $propertyValueC = 0;
        $result = false;
        $defaultProductProperty = '';

        foreach ($property as $categoryProductsProperty) {
            foreach ($categoryProductsProperty as $properties) {
                if($properties['published'] === 1) {
                    foreach ($properties['property_value'] as $propertyValue) {

                        if (count($values) > 0) {
                            $result = false;
                            $index = null;
                            foreach ($values as $key => $val) {
                                if ($propertyValue['property_id'] === $val['id']) {
                                    $result = true;
                                    $index = $key;
                                    break;
                                }
                            }
                        }

                        if ($result === false || count($values) === 0) {

                            $propertyC++;
                            $values[$propertyC] = [
                                'id' => $propertyValue['property_id'],
                                'name' => $propertyValue['property']['name'],
                                'type' => $propertyValue['property']['type'],
                                'values' => [[
                                    'id' => $propertyValue['id'],
                                    'value' => $propertyValue['value'],
                                    'colorCode' => $propertyValue['color_code']]
                                ]];
                            $propertyValueC = 0;

                        } else {

                            if (count($values[$index]['values']) > 0) {
                                $result1 = false;
                                foreach ($values[$index]['values'] as $key1 => $val1) {
                                    if ($propertyValue['id'] === $val1['id']) {
                                        $result1 = true;
                                        break;
                                    }
                                }
                            }

                            if ($result1 === false) {
                                $propertyValueC++;
                                $values[$index]['values'][$propertyValueC] = [
                                    'id' => $propertyValue['id'],
                                    'value' => $propertyValue['value'],
                                    'colorCode' => $propertyValue['color_code']];
                            }
                        }

                        if ($properties['default'] === 1 && !$forFilter) {
                            $defaultProductProperty = $properties;
                            $defaultProductProperty['default_property'][] = $propertyValue['id'];

                        }
                    }
                }
            }
        }

        return array('properties' => $values, 'defaultProductProperty' => $defaultProductProperty);

    }//EOF ProductProperty
}

if(!function_exists('getSiteConfigs')) {

    function getSiteConfigs(){
        $getConfigs = SiteConfig::all()->toArray();

        $configs = new stdClass;
        foreach ($getConfigs as $config){
            $proprty = (string) $config['name'];
            if ($config['lang_id'] !== null){
                if (property_exists($configs,$proprty)){
//                    return $configs[$proprty];
                    $configs->$proprty = array_merge($configs->$proprty,[$config['lang_id']=> $config['value']]) ;
                }else{
                    $configs->$proprty = [$config['lang_id']=> $config['value']];
                }
            }else{
                $configs->$proprty = $config['value'];
            }
        }

        return $configs;
    }//EOF SiteConfig Rebuild
}

if(!function_exists('productRelatedRebuild')) {

    function productRelatedRebuild($relatedProducts){

        $rebuildedRelatedProducts = [];

        foreach ($relatedProducts as $relatedProduct){
            $rebuildedRelatedProducts [] = Product::find($relatedProduct['related_product_id']);
        }

        return $rebuildedRelatedProducts;

    }//EOF SiteConfig Rebuild
}

if(!function_exists('productPriceRebuildOnFrontEnd')) {

    function productPriceRebuildOnFrontEnd($prices,$priceMode,$onlyProduct = false){

        $today = Carbon::now()->format('Y-m-d h:m:s');
        $limitedPrices = [];
        $user = auth()->user();
        if($onlyProduct){
            $prices = array_filter($prices,function ($price) {
                return $price['product_property_id'] === null;
            });
        }

        $prices = $limitedPrices;
        $limitedPrices = [];

        if(count($prices)> 0 && $user){

            foreach ($prices as $price){
                if(count($price['users']) > 0){
                    foreach ($price['users'] as $item){
                        if($item['id'] === $user->id){
                            $limitedPrices [] = $price;
                        }
                    }
                }
            }
            if(count($limitedPrices) > 0){
                return priceOutput($limitedPrices,$priceMode);
            }
        }

        if(count($prices)> 0 && $user){
            foreach ($prices as $price){
                if(count($price['roles']) > 0){
                    foreach ($price['roles'] as $item){
                        if($item['id'] === $user->role_id){
                            $limitedPrices [] = $price;
                        }
                    }
                }
            }
            if(count($limitedPrices) > 0){
                return priceOutput($limitedPrices,$priceMode);
            }
        }

        return $price = count($prices) > 0 ? priceOutput($prices,$priceMode) : $prices;
    }
}

if(!function_exists('priceOutput')){
    function priceOutput($prices,$priceMode){
        $priceWhere = '';

        if(count($prices) > 0){

            $prices = collect($prices);

            if($priceMode === 'high'){
                $priceWhere = $prices->max('price');
            }else{
                $priceWhere = $prices->min('price');
            }
        }

        if ($priceWhere !== ''){

            foreach ($prices as $price){
                if ($price['price']=== $priceWhere){
                    return $price;
                }
            }
        }
    }
}

if(!function_exists('productHighlightFeatures')) {

    function productHighlightFeatures($features){
        $rebuildFeatures = [];

        foreach ($features as $feature) {
            foreach ($feature['features'] as $item) {
                if ($item['highlight'] === 1) {

                    $rebuildFeatures [] = [
                        'name' => $item['name'],
                        'values' => $item['values']
                    ];
                }
            }
        }

        return $rebuildFeatures;
    }
}

if(!function_exists('validateIranMobile')) {

    function validateIranMobile($value)
    {
        return preg_match("/^09[0-1-2-3-9]\d{8}$/", $value);
    }
}

if(!function_exists('generateUserVerifyCode')) {

    function generateUserVerifyCode($model, $col, $min = 1000, $max = 9999)
    {
        $unique = false;
        do {

            $random = rand($min, $max);

            // Check if it is unique in the database
            $count = $model::where($col, $random)->count();

            // digit appears to be unique
            if ($count == 0) {
                // Set unique to true to break the loop
                $unique = true;
            }

        } while (!$unique);

        return $random;
    }
}

if(!function_exists('generateOrderTrackCode')) {

    function generateOrderTrackCode($model, $col, $min = 100000, $max = 999999)
    {
        $unique = false;
        do {
            // Generate random
            $random = 'AJ-' . rand($min, $max);

            // Check if it is unique in the database
            $count = $model::where($col, $random)->count();

            // digit appears to be unique
            if ($count == 0) {
                // Set unique to true to break the loop
                $unique = true;
            }

        } while (!$unique);

        return $random;
    }
}

if(!function_exists('breadcrumbCategories')) {

    function breadcrumbCategories($categoryId){

//        Todo Reafcotr از دیتابیس نمی گیریم
        do{
            $category = Category::find($categoryId);
            $categories [] = ['id' => $category->id,
                'name' => $category->name,
                'slug' => $category->slug];

            if($category->parent_id !== 0)
                $categoryId = $category->parent_id;

        }while($category->parent_id !== 1);

        return array_reverse($categories);
    }//EOF Breadcrumb Categories
}

if(!function_exists('buildTree')) {

    function buildTree(array &$elements, $parent_id = 0) {

        $branch = array();
        $i=0;
        foreach ($elements as $element) {

            if ($element['parent_id'] == $parent_id) {

                $children = buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$i] = $element;

                unset($elements[$i]);
                $i++;
            }
        }
        return $branch;
    }
}

if(!function_exists('productFeaturesRebuildOnFrontEnd')) {

    function productFeaturesRebuildOnFrontEnd($features){

        $rebuildFeatures = [];
        $index = -1;
        $featureIndex = -1;

        foreach ($features as $feature) {
            foreach ($rebuildFeatures as $key => $rebuildFeature){
                $index = -1;
                if($rebuildFeature['id'] === $feature['feature']['feature_group']['id']){
                    $index = $key;
                    break;
                }
            }

            if ($index === -1) {
                $rebuildFeatures [] = [
                    'id' => $feature['feature']['feature_group']['id'],
                    'name' => $feature['feature']['feature_group']['name'],
                    'features' => [[
                        'id' => $feature['feature']['id'],
                        'name' => $feature['feature']['name'],
                        'highlight' => $feature['feature']['highlight'],
                        'values' => [[
                            'id' => $feature['id'],
                            'name' => $feature['name'],
                        ]]
                    ]]
                ];
            } else {

                $counter = 0;
                foreach ($rebuildFeatures[$index]['features'] as $jkey => $item) {
                    $featureIndex = -1;
                    $counter++;
//                    dd($rebuildFeatures[$index]['features']);
                    if ($item['id'] === $feature['feature']['id']) {

                        $featureIndex = $jkey;
                        break;
                    }
                }

                if ($featureIndex === -1) {
                    $rebuildFeatures[$index]['features'] [] = [
                        'id' => $feature['feature']['id'],
                        'name' => $feature['feature']['name'],
                        'highlight' => $feature['feature']['highlight'],
                        'values' => [[
                            'id' => $feature['id'],
                            'name' => $feature['name'],
                        ]]
                    ];
                }else{
                    $rebuildFeatures[$index]['features'][$featureIndex]['values'] []=[
                        'id' => $feature['id'],
                        'name' => $feature['name'],
                    ];
                }
            }

        }

        return $rebuildFeatures;
    }
}

if(!function_exists('fetchArrayElement')) {

    function fetchArrayElement($array){

        $outputArray  = [];
        if($array){
            foreach ($array as $arrItem){
                foreach ($arrItem as $item){
                    $outputArray [] = $item;
                }
            }
        }

        return $outputArray;
    }
}

if(!function_exists('treeCategorySlugs')) {

    function treeCategorySlugs($category){

        $categoriesFilter []= $category->slug;

        if($category['children']){
            foreach ($category['children'] as $item){
                $categoriesFilter []= $item->slug;
            }
        }

        return $categoriesFilter;
    }
}

if(!function_exists('productRelatedFrontend')) {

    function productRelatedFrontend($relatedProducts,$priceMode){

        $rebuildRelatedProducts = [];

        foreach ($relatedProducts as $relatedProduct){
            $rebuildRelatedProducts [] = PriceFilter::applyCategoryDiscount(Product::GetProductForCalcPriceRelatedProducts($relatedProduct['related_product_id'])->first()->toArray(), $priceMode);
        }

        return $rebuildRelatedProducts;

    }//EOF SiteConfig Rebuild
}

if(!function_exists('assignVerifyCode')) {

    function assignVerifyCode($mobile){

        $now = Carbon::now();
        $verifyCode = UserVerify::where('mobile',$mobile)->first();
        if($verifyCode){

            $waitTime = Carbon::createFromFormat('Y-m-d H:i:s',$verifyCode->updated_at)->addMinute(2);

            if($now->gte($waitTime)){

                $verifyCode = generateUserVerifyCode(UserVerify::class, 'code');
                session(['verifyCode' => $verifyCode]);

                UserVerify::updateOrCreate(
                    ['mobile' => $mobile],
                    ['ip' => request()->ip(),
                        'agent' => request()->userAgent(),
                        'code' => $verifyCode]
                );

                SendSMS::sendVerifySMS($verifyCode,$mobile);
                return true;

            }else{
                session(['verifyCode' => $verifyCode->code]);
                return true;
            }
        }else{
            $verifyCode = generateUserVerifyCode(UserVerify::class, 'code');
            session(['verifyCode' => $verifyCode]);

            UserVerify::updateOrCreate(
                ['mobile' => $mobile],
                ['ip' => request()->ip(),
                    'agent' => request()->userAgent(),
                    'code' => $verifyCode]
            );

            SendSMS::sendVerifySMS($verifyCode,$mobile);
            return true;
        }
    }
}

if(!function_exists('checkCartProductPriceChange')) {

    function checkCartProductPriceChange($cartProductOriginalPrice,$cartProductDiscount,$cartProductId,$cartProductPropertyId,$priceMode){

        $cartProductPrice = $cartProductDiscount ? $cartProductOriginalPrice - $cartProductDiscount : $cartProductOriginalPrice;

        $user = Auth::user();
        $today = Carbon::now()->format('Y-m-d h:m:s');

        $product = Product::getProductForCalcPrice($cartProductId,$cartProductPropertyId)->firstOrFail();

        if(count($product->properties)>0){

            $productProperty = $product->properties[0];

            if($productProperty->quantity > 0 && count($productProperty->prices) > 0){
                $price = PriceFilter::applyPropertyDiscount($product->toArray(),$productProperty->toArray() ,$priceMode);
            }else{
                return apiResponse(null,['product.ThisProductNotPriceOrOutOfStock',[]],
                    'constants.apiResponseCodes.error',422);
            }

        }else{
            if(count($product->prices) > 0 && $product->quantity > 0){
                $price = PriceFilter::applyProductDiscount($product->toArray(), $priceMode);
            }else{
                return apiResponse(null,['product.ThisProductNotPriceOrOutOfStock',[]],
                    'constants.apiResponseCodes.error',422);
            }
        }

        return $cartProductPrice == $price->price ? false : true;

    }
}

