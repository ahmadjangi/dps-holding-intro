<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageLang extends Model
{
    use HasFactory;

    protected $fillable = ['lang_id','title','content','meta_keyword','meta_description'];
    public $timestamps = false;
}
