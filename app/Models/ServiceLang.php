<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceLang extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['lang_id','title','description','short_description','meta_keyword','meta_description'];
}
