<?php

namespace App\Models;


use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Product extends Model
{
    use Sluggable;

    public function sluggable(): array
    {
        return ['slug' => ['source' => 'name']];
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function langs(){
        return $this->hasMany(ProductLang::class);
    }
//    public function relatedProducts()
//    {
//        return $this->hasMany(RelatedProduct::class);
//    }
//
//    public function categories()
//    {
//        return $this->belongsToMany(Category::class);
//    }
//
//    public function featureValue()
//    {
//        return $this->belongsToMany(FeatureValue::class, 'feature_product');
//    }
//
//    public function features()
//    {
//        return $this->belongsToMany(Feature::class)
//            ->withPivot('feature_value_id');
//    }

    public function images()
    {
        return $this->hasMany(ImageProduct::class);
    }

}
