<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubsetCompanies extends Model
{
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function langs(){
        return $this->hasMany(SubsetCompaniesLang::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function services(){
        return $this->belongsToMany(Service::class);
    }
}
