<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqService extends Model
{
    use HasFactory;

    public function langs(){
        return $this->hasMany(FaqServiceLang::class);
    }
}
