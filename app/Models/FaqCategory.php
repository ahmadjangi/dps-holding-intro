<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    public $timestamps = false;

    public function faqs(){
        return $this->hasMany(Faq::class);
    }

    public function langs(){
        return $this->hasMany(FaqCategoryLang::class);
    }

}
