<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLang extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $fillable = ['lang_id','title','meta_keyword','meta_description','description','short_description'];
}
