<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogPostLang extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['lang_id','title','description','description_short','meta_description','meta_keyword'];
}
