<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;
    protected $hidden = ['created_at','updated_at'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products(){
        return $this->belongsTo(Product::class);
    }

    public function langs(){
        return $this->hasMany(CategoryLang::class);
    }
}
