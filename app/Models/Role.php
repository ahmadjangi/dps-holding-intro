<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $hidden = ['created_at','updated_at'];

    public function permissions(){
        return $this->belongsToMany(Permission::class );
    }

    public function users(){
        return $this->hasMany(User::class );
    }

    public function prices(){
        return $this->belongsToMany(PriceProduct::class,'price_product_roles','price_product_role_id','price_product_id');
    }

    public function discount(){
        return $this->belongsToMany(Discount::class);
    }

    public function cartDiscount($today){
        return $this->discount()
            ->where('published', 1)
            ->where('for', 'cart')
            ->where('start_date','<',$today)
            ->where('end_date','>',$today)
            ->orWhere('start_date','=',null)
            ->orWhere('end_date','=',null);;
    }
}
