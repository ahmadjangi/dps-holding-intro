<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public function langs(){
        return $this->hasMany(FaqLang::class);
    }
}
