<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

//    use Sluggable;
//
//    public function sluggable(): array
//    {
//        return ['slug' => ['source' => 'name']];
//    }
//
//    public function getRouteKeyName()
//    {
//        return 'slug';
//    }

    public function langs(){
        return $this->hasMany(ServiceLang::class);
    }

    public function faqs(){
        return $this->belongsToMany(Faq::class);
    }
}
