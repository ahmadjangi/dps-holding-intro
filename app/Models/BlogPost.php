<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Hekmatinasser\Verta\Verta;

class BlogPost extends Model
{

    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function categories(){
        return $this->belongsToMany(BlogCategory::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function langs(){
        return $this->hasMany(BlogPostLang::class);
    }

    public function scopeCurrentLang($query,$lang){
        return $query->with(['langs'=>function($query) use($lang){
            $query->where('lang_id',$lang);
        }])->whereHas('langs');
    }

    public function getCreatedAtAttribute($value): string
    {
        if (app()->getLocale() === 'fa'){
            return Verta::instance($value)->format('l d F %Y');
        }else{
            return Carbon::create($value)->format('d M Y');
        }

    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
