<?php

namespace App\Models;

use App\Http\Requests\ProductComment\ProductComments;
use Hekmatinasser\Verta\Verta;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','national_id','mobile','card_number', 'email', 'password','role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getBirthdayAttribute($value){
        $birthday[0] = Verta::instance($value)->year;
        $birthday[1] = Verta::instance($value)->month;
        $birthday[2] = Verta::instance($value)->day;
        return $birthday;
    }

    public function setBirthdayAttribute($value)
    {
        $birthday = explode('-', $value);
        $birthday = Verta::getGregorian($birthday[0],$birthday[1],$birthday[2]);
        $this->attributes['birthday'] = $birthday[0].'-'.$birthday[1].'-'.$birthday[2];
    }
    public function getCreatedAtAttribute($value){
        return Verta::instance($value)->format('H:i:s Y/m/d ');
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function cart(){
        return $this->hasOne(Cart::class);
    }

    public function address(){
        return $this->hasMany(UserAddress::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function posts(){
        return $this->hasMany(BlogPost::class);
    }

    public function productComments(){
        return $this->hasMany(ProductComment::class);
    }

    public function prices(){
        return $this->belongsToMany(PriceProduct::class,'price_product_users','price_product_user_id','price_product_id');
    }

    public function discounts(){
        return $this->belongsToMany(Discount::class);
    }

    public function discountUsedCount(){
        return $this->hasMany(DiscountUserUsedCount::class);
    }




}
